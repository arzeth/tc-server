#!/bin/node --experimental-specifier-resolution=node
/*!
 * tc-server
 * @author Arzet Ro <arzeth0@gmail.com>, 2020-2021
 * @license MIT
 */

import http from 'http'
import url from 'url'
import 'colors'
import { Server as IoServer, Socket as _IoServerSocket } from 'socket.io'
//import { scheduleJob } from 'node-schedule'
import _mv from 'mv'
//import Cookies    from 'cookies'
//import axios from 'axios'
import { fork } from 'child_process'
import {
	setupConfigAndWatchIt,
	setupUpdatingServer,
	newHttpBackendServer,
	newIoBackendServer,
	post,
	Config,
	blake3File,
	setupServerGetters,
	ServersVar,
	fsExistsAsync,
	safeMkdirAsync,
	getCdnServer,
	fetchAll,
	engine,
} from '../common-server-code/index.js'
import { promisify } from 'util'

console.log('-----------------------------  tc-server.js')

const mv = promisify(_mv)


// @ts-ignore
export const config: Config = {}// = JSON.parse(fs.readFileSync('./config.json'))
// @ts-ignore
const servers: ServersVar = setupServerGetters(config, {})// = {}
//import engine from './engine'



await setupConfigAndWatchIt(config)
const mdtBackendServer = await setupUpdatingServer(servers, config)

/*const liveStreamsSchedule: {[timestamp: number]: Array<[DocId, Blake3]>} = {}
setInterval(async () => {
	for (const v of await fetchAll(
`SELECT
	id,
	liveStartedOn,
	mainVideo
FROM ${config.mysql_prefix}site_content
WHERE
	expectedToBecomeLiveOn>0
	AND expectedToBecomeLiveOn<=${Date.now()}
	AND liveStartedOn=0
	AND mainVideo IS NOT NULL
`
	))
	{
		{
			;
		}
	}
}, 5000)._onTimeout()*/

const ioClientToWorkerToU_D: Map<_IoServerSocket, U_d> = new Map
const ioClientToWorkerToShaSize: Map<_IoServerSocket, [Sha256Hash, number]> = new Map
// use sockets instead of ... because the daemon (this file) can be restarted
const blake3ToWorker: Map<Buffer, _IoServerSocket> = new Map
//const blake3HexToInitialU_d: {[key: string]: U_d} = {}


//mdtBackendServer и nginx об этом не знают
const localServerToWorkerIoServer = new IoServer(2232, {
	transports: ['websocket'],
	serveClient: false,
})
localServerToWorkerIoServer.use(async (client, next) => {
	if (client.handshake.query.blake3Hex)
	{
		blake3ToWorker.set(
			client.blake3 = Buffer.from(
				client.handshake.query.blake3Hex, 'hex'
			),
			client
		)
		next()
	}
})
localServerToWorkerIoServer.on('connect', client => {
	const blake3: Buffer = client.blake3
	mdtBackendServer.emit(
		'notifyLoad',
		localServerToWorkerIoServer.sockets.sockets.size,
		Date.now(),
	);
	//const blake3Hex = client.handshake.query.blake3Hex as Blake3
	client.on(
		'OPTIONS', (
			{u_d, sha256OfImportantChunks, origFileSize}:
			{
				u_d: U_d,
				sha256OfImportantChunks: string,
				origFileSize: number,
			},
			callback: {(): void}
		) =>
	{
		if (!ioClientToWorkerToU_D.has(client))
		{
			ioClientToWorkerToU_D.set(client, u_d)
		}
		if (!ioClientToWorkerToShaSize.has(client))
		{
			ioClientToWorkerToShaSize.set(
				client,
				[sha256OfImportantChunks, origFileSize]
			)
		}
		callback()
	})
	client.on('disconnect', () => {
		setTimeout(() => {
			ioClientToWorkerToU_D.delete(client)
			ioClientToWorkerToShaSize.delete(client)
		}, 200)
		blake3ToWorker.delete(blake3)
	})
	client.on('complete:meta', async (premeta, callback: {(isSuccess: boolean): void}) => {
		await engine.wait(100)
		mdtBackendServer.emit(
			'complete:meta',
			ioClientToWorkerToU_D.get(client),
			blake3,
			ioClientToWorkerToShaSize.get(client)![0],
			ioClientToWorkerToShaSize.get(client)![1],
			premeta,
			(isSuccess: boolean) => {
				callback(isSuccess)
			}
		)
	})
	client.on('progress', async (
		u_d: U_d,
		blake3: Buffer,
		sha256OfImportantChunks: string,
		origFileSize: number,
		result,
		tcwCallback,
	) => {
		mdtBackendServer.emit(
			'progress',
			u_d,
			blake3,
			sha256OfImportantChunks,
			origFileSize,
			result,
			(mdtBackendCallback: {(): void}) => {
				mdtBackendCallback()
				tcwCallback()
			}
		)
	})
	client.on('complete:error', async (msg, params, callback: {(): void}) => {
		await engine.wait(400)//wait for .on(OPTIONS) otherwise ioClientToWorkerToShaSize is empty
		console.log('tc-server.ts:: ioClientToWorkerToShaSize.get(client)=%O', ioClientToWorkerToShaSize.get(client)!)
		mdtBackendServer.emit('complete:error',
			ioClientToWorkerToU_D.get(client),
			blake3,
			ioClientToWorkerToShaSize.get(client)![0],
			ioClientToWorkerToShaSize.get(client)![1],
			msg,
			params,
			() => {
				callback()
			}
		)
		client.disconnect(true)
	})
	/*client.on('complete:success', (result, callback: {(): void}) => {
		client.disconnect(true)
		mdtBackendServer.emit('complete:success', blake3, result, () => {
			callback()
		})
	})*/
	client.on('workerWantsAnyNewU_D', (callback) => {
		callback(Date.now(), ioClientToWorkerToU_D.get(client) || null)
	})
})
/*mdtBackendServer.on(
	'pause', async (

	) =>
{
		;
}
)*/

mdtBackendServer.on('connect', () => {
	mdtBackendServer.emit(
		'notifyLoad',
		localServerToWorkerIoServer.sockets.sockets.size,
		Date.now(),
	);
})

mdtBackendServer.on(
	'queueUpTranscoding',
	async (
		u_d: U_d,//массив, а не один элемент, ибо вдруг в черновике одно видео несколько раз. А ид владельца черновика не знаю зачем.
		blake3: Buffer,
		sha256OfImportantChunks: string,
		origFileSize: number,
		callback?: {(ok: boolean): void}
	) =>
{
	const blake3Hex: string = blake3.toString('hex')
	console.log(
		'queueUpTranscoding(u_d=%O, blake3=%O, sha=%O, origSz=%O)'.bgMagenta.blue,
		u_d, blake3Hex, sha256OfImportantChunks, origFileSize
	)
	if (blake3ToWorker.has(blake3))
	{
		console.log(blake3ToWorker.get(blake3)!)
		console.log(ioClientToWorkerToU_D.get(blake3ToWorker.get(blake3)!))
		console.log(
			'tc-worker has been already started for blake3=%O origFileSize=%O u_d=%O; therefore sending&appending u_d %O',
			blake3Hex,
			origFileSize,
			ioClientToWorkerToU_D.get(blake3ToWorker.get(blake3)!),
			u_d
		)
		blake3ToWorker.get(blake3)!.emit('appendU_d', u_d, () => {
			const shallAddList: U_d = []
			for (const xSrc of u_d)
			{
				let shallAdd = true
				for (const yDst of ioClientToWorkerToU_D.get(blake3ToWorker.get(blake3)!)!)
				{
					if (
						xSrc[0] === yDst[0]
						&&
						xSrc[1] === yDst[1]
					)
					{
						shallAdd = false
						break
					}
				}
				if (shallAdd)
				{
					shallAddList.push(xSrc)
				}
			}
			for (const x of shallAddList) // IIRC, it could be empty
			{
				ioClientToWorkerToU_D.get(blake3ToWorker.get(blake3)!)!.push(x)
			}
		})
	}
	else
	{
		console.log('starting tc-worker for blake3=%O origFileSize=%O u_d=%O', blake3Hex, origFileSize, u_d)
		const fk = fork(
			'./dist/tc-worker',
			[JSON.stringify({
				u_d,
				blake3Hex,
				sha256OfImportantChunks,
				origFileSize,
			})],
			{
				detached: true,
				//silent: true,
				stdio: 'pipe',
				killSignal: 'SIGINT', //хотя смысл, если abortsignal'а нет...
			},
		)
		fk.stdout.on('data', x => console.log('[out]'+x))
		fk.stderr.on('data', x => console.log('[err]'+x))
	}
	callback?.(true)
})


/*const ioBackend = newIoBackendServer(IoServer, config)
ioBackend.on('connection', (client) => {
	client.on('queueUpTranscoding', )
})*




newHttpBackendServer(config, async (req, resp, q) => {
	if (!q.GET.qbqb) return
	switch (q.GET.cmd)
	{
		case 'del':
		{
			const blake3Hex = q.GET.hash
			if (typeof blake3Hex === 'string' && /^[a-z0-9]{64}$/.test(blake3Hex))
			{
				if (!statuses.hasOwnProperty(blake3Hex))
				{
					console.warn('.del got nonexistent hash=%O', blake3Hex)
					break
				}
				delete statuses[blake3Hex]
			}
			else
			{
				console.error('.del got badHash, q.GET = %O', q.GET)
			}
			break
		}
	}
	resp.end()
})*/
