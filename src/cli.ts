#!/bin/node --experimental-specifier-resolution=node
/*!
 * tc-server
 * @author Arzet Ro <arzeth0@gmail.com>, 2020-2021
 * @license MIT
 */

import fsAsync from 'fs/promises'
//import { cwd } from 'process'
import yargs, { demandOption } from 'yargs'
import { hideBin } from 'yargs/helpers'
import { safeMkdirAsync, engine } from '../common-server-code/index.js'
import processMedia from './processMedia.js'
import processMediaSand from './processMediaSand.js'


const argv = yargs(hideBin(process.argv))
.option('how-much-live', {
	//alias: 'v',
	type: 'number',
	description: 'If live, then quality of live: 0, 1, 2; lower is better image quality',
	demandOption: false,
})
.option('force-input-fps', {
	//alias: 'v',
	type: 'number',
	description: '/dev/video* always gives 30 FPS, so we need to fix it',
	demandOption: false,
})
.option('live-save', {
	description: 'Store whole live stream',
	demandOption: false,
	choices: [0, 1],
	default: 0,
})
.option('also-single-file', {
	description: 'Also one single file',
	demandOption: false,
	choices: [0, 1],
	default: 0,
})
.option('also-cover', {
	description: 'Also one cover',
	demandOption: false,
	choices: [0, 1],
	default: 0,
})
.option('an', {
	description: 'Disable audio',
	demandOption: false,
	type: 'boolean',
	default: void 0,
})
.option('sand', {
	description: 'Sand',
	demandOption: false,
	type: 'boolean',
	default: false,
})
.option('vn', {
	description: 'Disable video',
	demandOption: false,
	type: 'boolean',
	default: void 0,
})
.option('gpu', {
	//alias: 'v',
	description: 'If live, then quality of live: 0, 1, 2; lower is better',
	demandOption: false,
	choices: ['off', 'auto', 'require-any', 'nvenc'],
	default: 'auto',
})
.option('seg', {
	alias: 'segment-duration',
	type: 'string',
	description: 'Segment duration. Can be a math expression',
	demandOption: false,
})
.strict()
.demandCommand(2, 2)
/*.option('verbose', {
	alias: 'v',
	type: 'boolean',
	description: 'Run with verbose logging'
})*/
.argv

//console.log(argv)
//aoeuo()

/*const argv = yargs
.command('live-on'
	'live-on': {key: 'live-on', description: 'When become live (0=offline, 1=right now, >1=timestamp in ms)', args: 1, required: false, default: '0'},
	'live-q': {key: 'live-q', description: 'If live, then quality of live: 0, 1, 2; lower is better', default: '1'},
	_meta_: {minArgs: 2, maxArgs: 2, args: 2},
})*/

/*const willBeLiveOn = (
	args.filter(x=>/^--?live-on=\d+$/.test(x)).length
	? +args.filter(x=>/^--?live-on=\d+$/.test(x))[0].replace(/[^\d]/g, '')
	: (
		args.filter(x=>/^--?live-now/.test(x)).length
		? 1
		: 0
	)
	
)*/
/*const ROOT_DIR      = (
	/\//.test(/*process.argv[2]*argv._[0]!.toString())
	? /*process.argv[2]*argv._[0].toString().replace(/^(.+)\/(.+)/, '$1') || cwd()
	: cwd()
)
const inputFileName = /*process.argv[2]*argv._[0]!.toString().replace(/^.+\//, '')
*/
const outputDir = argv._[1]!.toString()//process.argv[3]



const alsoSingleFile = !!(+argv.alsoSingleFile || 0)

await safeMkdirAsync(outputDir)
for (let i = 0; i <= 9; i++)
{
	await safeMkdirAsync(outputDir + '/' + i)
}
if (!(argv.liveOn && !argv.liveSave))
{
	//await safeMkdirAsync(outputDir + '/cover')
	await safeMkdirAsync(outputDir + '/thumbs')
	if (alsoSingleFile)
	{
		await safeMkdirAsync(outputDir + '/raw')
		await safeMkdirAsync(outputDir + '/raw/h264')
		await safeMkdirAsync(outputDir + '/raw/aac')
	}
}



console.log(argv)

const qj: {
	[key: string]: [number, number]
} = {}
const kk = await (process.env.SAND || process.env.sand || argv.sand ? processMediaSand : processMedia)({
	inputFile: argv._[0]!,
	outputDir: outputDir,
	howMuchLive: +argv.howMuchLive || 0,
	inputFramerate: +argv.forceInputFps || void 0,
	venc: [['sw', Infinity]],
	//venc: [['nv', 0]],
	//vdec: [['nv', 0]],
	vdec: [['sw', Infinity]],
	deleteOnFail: false,
	noAudio: true,
	enableVideo: argv.vn !== true,
	enableAudio: argv.an !== true,
	alsoSingleFile,
	alsoCover: !!(+argv.alsoCover || 1),
	forceSegDuration: argv.seg || process.env.SEG || process.env.seg || void 0,
	sendMeta: (result={}) =>
	{
		return {
			status: 'ok',
			result
		}
	},
	jsonFail: (msg: any/*: string*/, params={}, errorCode=400) =>
	{
		console.error('jsonFail(%O)', msg)
		return {
			status: 'error',
			message: msg + '',
			params
		}
	},
	_onTranscodeProgress: async (
		type,
		//format,
		//videoHeightOrAudioQualityOrNothingIfCover,
		//resultFilePath,
		//resultFileSize,
		howMuchLocalTcPercentShouldBe,//subTaskProgress // formatProgress
		whenFinishedHowMuchTcPercentWasAdded,//subTaskFinalOutput // formatGrantsToOverallProgress
	) => {
		console.log(type.bgGreen, whenFinishedHowMuchTcPercentWasAdded.toString().red)
		const qjT = type// + '-' + format + '-' + videoHeightOrAudioQualityOrNothingIfCover
		if (!qj.hasOwnProperty(qjT)) qj[qjT] = [howMuchLocalTcPercentShouldBe, whenFinishedHowMuchTcPercentWasAdded]
		else if (qj[qjT][0] >= 100) return
		else if (qj[qjT][0] < 0) return
		else qj[qjT][0] = howMuchLocalTcPercentShouldBe
		console.log('qj %O'.blue, qj)
		const ddd = {
			status: 'ok',
			result: {
				tcTotalPercent:
					Object.values(qj)
					.map(([loc, fin]) => fin * (loc/100))
					.reduce((a, b) => a + b, 0),
				curFormat: {
					type,// format, videoHeightOrAudioQualityOrNothingIfCover,
					tcPercent: howMuchLocalTcPercentShouldBe,
					tcToUpPercent: 0,
				},
			}
		}
		if(0)console.log('progress:',
			//Date.now(),
			/*u_d,
			blake3,
			sha256OfImportantChunks,
			origFileSize,*/
			ddd,
		)

		console.log('Transcoding progress=%O%%', howMuchLocalTcPercentShouldBe)
		if (0&&howMuchLocalTcPercentShouldBe >= 100)
		{
			console.log('204'.green)
			if (resultFilePath)
			{
				console.log('204 %O'.green, resultFilePath)
				resultFileSize ||= (await fsAsync.stat(resultFilePath)).size
				console.log('206'.green)
			}
			//console.log('%O vs %O'.green, cdnServer.serverId, config.serverId)
			return
			if (cdnServer.serverId === config.serverId)
			{
				console.log('213 cdn=localhost'.green)
				console.log('tc->cdn'.green)
				const cdnVideoDir = pathJoin(
					cdnServer.cdnDir,
					blake3.toString('hex')
				)
				await fsAsync.mkdir(cdnVideoDir, {
					mode: 0o0755,
					recursive: true
				})
				await fsAsync.mkdir(cdnVideoDir + '/cover', {
					mode: 0o0755,
					recursive: true
				})
				await fsAsync.mkdir(cdnVideoDir + '/video', {
					mode: 0o0755,
					recursive: true
				})
				await fsAsync.mkdir(cdnVideoDir + '/video/h264', {
					mode: 0o0755,
					recursive: true
				})
				/*if (type === 'a')
				{
					await fsAsync.mkdir(cdnVideoDir + '/audio', {
						mode: 0o0755,
						recursive: true
					})
					if (format === 'opus')
					{
						await fsAsync.mkdir(cdnVideoDir + '/audio/opus', {
							mode: 0o0755,
							recursive: true
						})
						await fsAsync.copyFile(resultFilePath, cdnVideoDir + '/audio/opus/high.opus')
					}
				}*/
				if (type === 'v')
				{
					console.log('248 type=v'.green)
					//const fname = inputFileName + '.'   + format + '.' + videoHeightOrAudioQualityOrNothingIfCover + 'p.mp4'
					const dst = cdnVideoDir + '/video/' + format + '/' + videoHeightOrAudioQualityOrNothingIfCover + 'p.mp4'
					console.log('tc->cdn:: copy %O -> %O', resultFilePath, dst)
					await mv(resultFilePath, dst)

					const yyy = Immutable.fromJS(ddd).toJS() as typeof ddd
					yyy.result.curFormat.tcToUpPercent = 100
					mdtBackendServer.emit('progress',
						//Date.now(),
						u_d,
						blake3,
						sha256OfImportantChunks,
						origFileSize,
						yyy
					)
				}// сначал обложки обрабатываются
				else if (type === 'c')
				{
					//const fname = inputFileName + '.cover.' + format
					const dst = cdnVideoDir + '/cover/orig.'+format//' + format + '/orig.webp'
					console.log('tc->cdn:: copy %O -> %O', resultFilePath, dst)
					await mv(resultFilePath, dst)
				}
			}
			else
			{
				if (!await new Promise<boolean>(resolve => {
					const ioParams = {
						pingInterval: 2000,
						transports: ['websocket'/*, 'polling'*/],
						query: {
							sha256OfImportantChunks,
							origFileSize,
						},
					}
					let firstChunkIdx: number
					let chunkSize: number
					let version
					let lastChunkIdx: number
					//let _status
					let _progress
					let _onProgress

					const upSocket = ioClient('wss://perm.test70.123y.ru:4011', ioParams)
					upSocket.on('connect', () => {
						console.warn('connected to cdn')
					})
					upSocket.on('OPTIONS', async (r) => {
						if (r.loaded)
						{
							/*_progress.uploadedBytes = ioParams.query.origFileSize
							_progress.uploadedPercent = 100
							_status = 2
							if (_onProgress) _onProgress(
								ioParams.query.sha256OfImportantChunks,
								_progress,
							)*/
							upSocket?.disconnect(true)
							resolve(true)
							return
						}
						firstChunkIdx = r.firstChunkIdx
						chunkSize = r.chunkSize
						version = r.version
						lastChunkIdx = Math.ceil(file.size / chunkSize) - 1
						console.log('firstChunkIdx=%O, lastChunkIdx=%O', firstChunkIdx, lastChunkIdx)
						const localConnectionIdx = connectionIdx

						for (let chunkIdx = firstChunkIdx; chunkIdx <= lastChunkIdx; chunkIdx++)
						{
							//console.log('connectionIdx = %O', connectionIdx)
							//if (connectionIdx !== localConnectionIdx) break
							//console.log('a1')
							if (0 !== await new Promise(async (resolve, reject) => {
								//console.log('a2')
								//const xxhash = await window.createBlake3Hash()// new SparkMD5.ArrayBuffer()
								const fileReader = new FileReader()
								//const loadNext = () => {
									const start = chunkIdx * chunkSize
									const end = (
										start + chunkSize >= file.size
										? file.size
										: start + chunkSize
									)
									const newChunkSize = end - start
									fileReader.readAsArrayBuffer(
										blobSlice.call(file, start, end)
									)
								//}
								fileReader.onload = (e) => {
									console.log("read chunk nr %d of %d"/*:: %O"*/, chunkIdx, lastChunkIdx/*, e.target.result*/)
									const chunkData = e.target.result
									//xxhash.update(new Uint8Array(chunkData)) // Append array buffer
									//chunkIdx++
									/*if (chunkIdx < lastChunkIdx) {
										//loadNext()
									} else {*/
										//const hash = xxhash.digest().toString(16)
										upSocket?.emit(
											'chunk',
											chunkIdx,
											e.target.result,
											///*expectedChunkHash: */hash,
											/*callback: */(result) => {
												console.log('sent chunk, got result %O', result)
												_progress.uploadedBytes = chunkIdx * chunkSize + newChunkSize
												_progress.uploadedPercent = getPercentage(
													_progress.uploadedBytes,
													origFileSize
												)
												if (_onProgress) _onProgress(
													ioParams.query.sha256OfImportantChunks,
													_progress,
												)
												if (result === 0) {
													resolve(result)
													return
												}
												console.error(result)
												reject(result)
											}
										)
									//}
								}
								fileReader.onerror = () => {
									reject('Не удалось прочитать файл')
								}
								
								//loadNext()
							}).catch((err) => {
								console.log('e2')
								perror(err)
								return false
							})) break
							if (_status === 0) break
						}

						const ioParams = {
							pingInterval: 2000,
							transports: ['websocket'/*, 'polling'*/],
							query: {
								
							},
						}
						const tcSocket = ioClient('wss://test70.123y.ru:4006', ioParams)
						tcSocket.on('connect', () => {
							console.warn('connect')
						})
						tcSocket.on('OPTIONS', async (r) => {
						})

						if (_status !== 0)
						{
							console.log('s!=0')
							socket.disconnect(true)
							_status = 0
						}
					})
					upSocket.on('error', (error) => {
						upSocket?.disconnect(true)
						//_status = 0
						console.error(error.message)
						//perror(error.message.replace(/%/g, ''))//FIXME
					})
					upSocket.on('reconnect', () => {
						console.warn('reconnect')
					})
				})){}
			}
		}
	},
	// uploadFile
	// v=video, a=audio, c=cover
	/*async (
		type,
		format,
		videoHeightOrAudioQualityOrNothingIfCover,
		filePath,
		callback,
	) => {
		if (cdnServer.serverId === config.serverId)
		{
			
		}
	}*/
})
if (kk)
{
	let sigint = false
	process.on('SIGINT', async () => {
		if (sigint) return
		console.log('SIGINT')
		sigint = true
		kk.sigint()
		await engine.wait(1595)
		process.exit(0)
	})
}
console.log('cli ended')
