#!/bin/node
import fs from 'fs'
import fsAsync from 'fs/promises'
import { spawn, spawnSync } from 'child_process'
import { join as pathJoin } from 'path'
import { io as ioClient } from 'socket.io-client'
import * as process from 'process'
//import { cpus as osCpus } from 'os'
import 'colors'
import { stat as fsStatAsync } from 'fs/promises'
import * as commons from '../common-server-code/index.js'
import {
	engine,
	MediaPremeta,
} from '../common-server-code/index.js'
import processMedia from './processMedia.js'
import { setInterval } from 'node:timers'

if (process.argv.length !== 3)
{
	console.error('process.argv.length !== 3')
	process.exit(1)
}

//const CPU_THREADS = osCpus().length

// @ts-ignore
export const config: commons.Config = {}// = JSON.parse(fs.readFileSync('./config.json'))
// @ts-ignore
const servers: commons.ServersVar = commons.setupServerGetters(config, {})// = {}
//import engine from './engine'

//const blake3ToU_D: {[key: string]: U_d} = {}


let {
	u_d,
	blake3Hex,
	sha256OfImportantChunks,
	origFileSize,
}: {
	u_d: U_d,//массив, а не один элемент, ибо вдруг в черновике одно видео несколько раз. А ид владельца черновика не знаю зачем.
	blake3Hex: string,
	sha256OfImportantChunks: string,
	origFileSize: number,
} = JSON.parse(process.argv[2]!)
console.log(process.argv[2])


//let u_d: U_d = []

/*const statuses: {
	[key: string/*filename*]: 1|-1|-2|number|AsyncReturnType<typeof processVideo>
} = {}
*/

const forceSchedule = process.env.S === '1'

//function start(coreId)
//{
	//coreId = coreId || 0;
	//const IS_INDEXHTML_REGEXP = /^index(\.[a-z]{1,4})?/;

let lastT: DateNumber = 0

await commons.setupConfigAndWatchIt(config, void 0, 'tcw')
const mdtBackendServer = await commons.setupUpdatingServer(servers, config)

const tcdSocket = forceSchedule ? null : await new Promise<ReturnType<typeof ioClient>>(resolve => {
	const tcdSocketRet = ioClient('ws://127.0.0.1:2232', {
		reconnectionAttempts: Infinity,
		reconnectionDelay: 100,
		reconnectionDelayMax: 100,
		transports: ['websocket'/*, 'polling'*/],
		query: {
			blake3Hex
		},
	})
	let intrvl: null|ReturnType<typeof setInterval> = null
	let setOptions = () => {
		tcdSocketRet.emit('OPTIONS',
			{
				u_d,
				sha256OfImportantChunks,
				origFileSize,
			},
			() =>
		{
			if (!intrvl) intrvl = setInterval(() => {
				tcdSocketRet.emit(
					'workerWantsAnyNewU_D',
					(t: DateNumber, newU_d: null|U_d) => {
						if (t < lastT) return
						lastT = t
						if (newU_d) u_d = newU_d
					}
				)
			}, 100)
			resolve(tcdSocketRet)
		})
	}
	tcdSocketRet.on('connect', () => {
		console.warn('tcdSocketRet.onconnect')
		setOptions()
	})
	tcdSocketRet.on('reconnect', () => {
		console.warn('tcdSocketRet.onreconnect')
		setOptions()
	})
	tcdSocketRet.on('appendU_d', (appU_d: U_d, callback) => {
		const shallAddList = []
		for (const xSrc of appU_d)
		{
			let shallAdd = true
			for (const yDst of u_d)
			{
				if (
					xSrc[0] === yDst[0]
					&&
					xSrc[1] === yDst[1]
				)
				{
					shallAdd = false
					break
				}
			}
			if (shallAdd)
			{
				shallAddList.push(xSrc)
			}
		}
		for (const x of shallAddList)
		{
			u_d.push(x)
		}
		callback()
	})
	/*tcdSocketRet.on('workerWantsAnyNewU_D', (newU_d) => {
		
	})*/
	tcdSocketRet.on('disconnect', () => {
		clearInterval(intrvl)
		intrvl = null
	})
})


const ROOT_DIR = config.cdnDir as string




const blake3 = Buffer.from(blake3Hex, 'hex')
const q = {
	/*jsonOk: async (result: Object|number={}) =>
	{
		return new Promise<never>(resolve => {
			console.log('jsonOk(%O)', result)
			/*callback({
				status: 'ok',
				result
			})*
			tcdSocket.emit('complete:success', result, () => {
				process.exit()
			})
		})
	},*/
	jsonFail: async (msg: any/*: string*/, params={}, errorCode=400) =>
	{
		return new Promise<never>(resolve => {
			console.error('jsonFail(%O)', msg)
			/*callback({
				status: 'error',
				message: msg + '',
				params
			})*/
			tcdSocket?.emit('complete:error', msg, params, () => {
				process.exit()
			})
		})
	},
}
//if (!sha256OfImportantChunks || !origFileSize) return await q.jsonFail('add:: bad args')


const inputFileShaName = sha256OfImportantChunks + '-' + origFileSize
/*const inputFileShaPath = pathJoin(ROOT_DIR, inputFileShaName)
if (!await fsExistsAsync(inputFileShaPath))
{
	await q.jsonFail('%upv__remote__fileNotFound%')
}
/*{
	const t = statuses[blake3Hex]
	if (typeof t === 'number' && t > 100000)
	{
		await q.jsonOk(Date.now() - t)
	}
}
if (statuses[blake3Hex])
{
	await q.jsonOk(statuses[blake3Hex]!)
}
statuses[blake3Hex] = Date.now()
*
console.log('statuses[%O] = 1'.bgMagenta.blue, inputFileShaName)
{
	const actualSize = (await fsStatAsync(inputFileShaPath)).size
	if (actualSize !== origFileSize)
	{
		//statuses[blake3Hex] = -1
		console.error('processVideo won\'t start because of bad size:: `%O`:  %O < %O', inputFileShaName, actualSize, origFileSize)
		await q.jsonFail('actualSize !== origFileSize')
	}
}
*/
let cdnServer = commons.getCdnServer(config.serverId)
if (!cdnServer)//был while, но и с for тупит ts всё равно, надо исправить
{
	await engine.wait(1000)
	for (let i = 1; !(cdnServer = commons.getCdnServer(config.serverId)); i++)
	{
		console.log('cdn server unavailable, attempts done: %d, time=%O,', i, (new Date).toISOString())
		await engine.wait(1000)
	}
}
console.log('%O'.bgMagenta.blue, cdnServer)
if (!cdnServer) throw new Error('auae')//только для typescript строка


// status = 0 в
console.log(cdnServer)
const outputDir = commons.getVideoBasepath(
	cdnServer.cdnDir,
	blake3Hex
) // only used when if (cdnServer.serverId === config.serverId), i.e. localhost
const alsoSingleFile: boolean = false

console.log('176'.bgMagenta.blue, inputFileShaName)

const qj: {
	[key: string/*MediaType*/]: [number, number]
} = {}
const alreadySuccessfullyTranscoded: boolean = !!(await commons.getFileSize(outputDir + '/PREJOB_META.json'))
console.log('alreadySuccessfullyTranscoded=%O', alreadySuccessfullyTranscoded)
const videoResult = forceSchedule || alreadySuccessfullyTranscoded ? {
	premeta: JSON.parse(
		fs.readFileSync(outputDir + '/PREJOB_META.json', {encoding: 'utf-8'})
	),
	promise: Promise.resolve(),
} : await processMedia({
	inputFile: pathJoin(ROOT_DIR, inputFileShaName),
	outputDir: outputDir,
	howMuchLive: 0,
	deleteOnFail: true,
	alsoSingleFile,
	alsoCover: true,
	dontResizeIfDiffIsLessThan: ['px', 0],
	//venc: 'auto',
	venc: [['sw', Infinity]],
	//venc: [['nv', 12]],
	vdec: [['sw', Infinity]],
	jsonFail: q.jsonFail,
	_onTranscodeProgress: async (
		mediaType,
		//format,
		//videoHeightOrAudioQualityOrNothingIfCover,
		//resultFilePath,
		//resultFileSize,
		howMuchLocalTcPercentShouldBe,//subTaskProgress // formatProgress
		whenFinishedHowMuchTcPercentWasAdded,//subTaskFinalOutput // formatGrantsToOverallProgress
	) => {
		//console.log(mediaType, howMuchLocalTcPercentShouldBe, whenFinishedHowMuchTcPercentWasAdded)
		//console.log(qj)
		const qjT = mediaType// + '-' + format + '-' + videoHeightOrAudioQualityOrNothingIfCover
		if (!qj.hasOwnProperty(qjT)) qj[qjT] = [howMuchLocalTcPercentShouldBe, whenFinishedHowMuchTcPercentWasAdded]
		else if (qj[qjT][0] >= 100) return
		else if (qj[qjT][0] < 0) return
		else qj[qjT][0] = howMuchLocalTcPercentShouldBe
		//console.log('qj %O'.blue, qj)
		const ddd = {
			status: 'ok',
			result: {
				tcTotalPercent:
					Object.values(qj)
					.map(([loc, fin]) => fin * (loc/100))
					.reduce((a, b) => a + b, 0),
				curFormat: {
					type: mediaType,//format, videoHeightOrAudioQualityOrNothingIfCover,
					tcPercent: howMuchLocalTcPercentShouldBe,
					//tcToUpPercent: 0,
					tcToUpPercent: 100,//!!!!!!! захардкодено
				},
			}
		}
		tcdSocket?.emit('progress',
			//Date.now(),
			u_d,
			blake3,
			sha256OfImportantChunks,
			origFileSize,
			ddd,
			() => {// пока 100% не отправит, tc-worker пусть не process.exit()

			}
		)
		//console.log('199'.green)
		if (!cdnServer)return// typescript забывает, если вне await processVideo
		console.log('howMuchLocalTcPercentShouldBe=%O', howMuchLocalTcPercentShouldBe)
		//console.log('41111111111111')
	},
	// uploadFile
	// v=video, a=audio, c=cover
	/*async (
		type,
		format,
		videoHeightOrAudioQualityOrNothingIfCover,
		filePath,
		callback,
	) => {
		if (cdnServer.serverId === config.serverId)
		{
			
		}
	}*/
})
if (!videoResult)
{
	throw '!videoResult'
}
await videoResult.promise
await engine.wait(1000)
if (tcdSocket)
{
	while (!await new Promise(resolve => {
		commons.socketEmitVoid(
			tcdSocket,
			'complete:meta',
			videoResult.premeta,
			(isSuccess: boolean) => {
				console.log(
					'complete:meta %O %O %O',
					isSuccess ? 'success' : 'failure',
					new Date(),
					Date.now()
				)
				resolve(isSuccess)
			}
		)
	}));
}


console.log('!!!!!!!!!!!!')
console.log('videoResult=%O'.green, videoResult)

//statuses[blake3Hex] = videoResult

console.log('u_d: %O', u_d)
const draftIds = new Set(//Set тут, ибо надо аналог array_unique в php
	u_d.map(x => x[1])
	//Object.values(u_d)
	//.map(x => x[1])//x.map(x => x[1]))
	//.flat()
)
console.log(videoResult.premeta)
for (const draftId of draftIds)
{
	console.log('draftid 449 %O', draftId)


	const meta = videoResult.premeta
	const {allMetaFfprobe, h264Heights, av1Heights, inputHasAudio, durationInMs} = (() => {
		if ((meta as any).h264Heights) return meta as any
		const [h264Heights, av1Heights] =
		['h264', 'av1']
		.map(codec => 
			(meta as MediaPremeta)
			.flatOutputs.adaptive.v
			.map(x =>
				(x.list || [x])
				.filter(x=>x.codec === codec)
				.map(x=>Math.min(x.w, x.h))
			).flat()
		)
		return {
			allMetaFfprobe: (meta as MediaPremeta).inputs[0].allMetaFfprobe,
			h264Heights,
			av1Heights,
			inputHasAudio: (meta as MediaPremeta).inputs[0].hasAudio,
			durationInMs: (meta as MediaPremeta).inputs[0].durationInMs
		}
	})()

	const data = {
		draftId,
		iaoeuj: 'done',
		blake3: blake3Hex,
		sha256OfImportantChunks,
		origFileSize,
		videoResult: {
			origFileBlake3: blake3Hex,
			video: {
				//raw_h264: videoResult.filter(x => /raw_h264/.test(x)),
				abs_h264: h264Heights.map(h=>'abs_h264_' + h + 'p'),
				abs_av1:  av1Heights .map(h=>'abs_av1_'  + h + 'p'),
			},
			audio: {
				abs_aac: inputHasAudio ? ['abs_aac'] : [],
				//raw_aac: videoResult.meta.filter(x => /raw_aac/.test(x)),
				/*abs_aac: videoResult.meta.filter(x => /abs_aac/.test(x)),
				...(
					videoResult.meta.filter(x => /abs_opus/.test(x)).length
					? {abs_opus: videoResult.meta.filter(x => /abs_opus/.test(x))}
					: {}
				),*/
			},
			subtitles: {},
		},
		videoResultFields: [
			...h264Heights.map(h=>'abs_h264_' + h + 'p'),
			...av1Heights .map(h=>'abs_av1_' + h + 'p'),
			...(inputHasAudio ? ['abs_aac'] : [])
		],
	}
	console.log(data)
	const a = await commons.post(
		config.apiUrl + 'schedulePublishDraft',
		data
	)
	let d = a.data
	try {
		if (!d?.status) d = JSON.parse(d)
		if (d.status !== 'ok') console.error('server said BAD')
		else
		{
			console.log('schedulePublishDraft said OK, result=%O', d.result)
		}

	} catch (e) {
		console.error('schedulePublishDraft sent BAD json: %O', a.data)
	}
}
process.exit()
