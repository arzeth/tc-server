import { fraction } from 'mathjs'

export function numberToFractional (
	n: number,
): Urational
{
	// @ts-ignore
	if (Array.isArray(n)) return n
	const ret = fraction(+n)
	return [ret.n, ret.d]
}

export function getH264LevelNumber (
	outputActualW: Uint32,
	outputActualH: Uint32,
	outputFps: Uint16|Urational,
): Uint8
{
	if (Array.isArray(outputFps)) outputFps = outputFps.$fromRational()
	/*if (
		(outputActualW <= 352 && outputActualH <= 480 && outputFps <= 30.7)
		||
		// моя строка
		(outputActualW <= 416 && outputActualH <= 234 && outputFps <= 30)
		||
		(outputActualW <= 352 && outputActualH <= 576 && outputFps <= 25.6)
		||
		(outputActualW <= 720 && outputActualH <= 480 && outputFps <= 15.0)
		||
		(outputActualW <= 720 && outputActualH <= 576 && outputFps <= 12.5)
	)
	return 22
	if (
		(outputActualW <= 352 && outputActualH <= 480 && outputFps <= 61.4)
		||
		// моя строка
		(outputActualW <= 416 && outputActualH <= 234 && outputFps <= 60)
		||
		(outputActualW <= 352 && outputActualH <= 576 && outputFps <= 51.1)
		||
		(outputActualW <= 720 && outputActualH <= 480 && outputFps <= 30.0)
		||
		(outputActualW <= 720 && outputActualH <= 576 && outputFps <= 25.0)
		// моя строка, но не проверял. Впритык.
		//(outputActualW <= 864 && outputActualH <= 486 && outputFps <= 24.0)
	)
	return 30*/
	if (
		(outputActualW <= 720 && outputActualH <= 480 && outputFps <= 80.0)
		||
		(outputActualW <= 720 && outputActualH <= 576 && outputFps <= 66.7)
		||
		// моя строка
		(outputActualW <= 864 && outputActualH <= 486 && outputFps <= 61)//наверно до 65.0 так-то
		||
		(outputActualW <= 1280 && outputActualH <= 720 && outputFps <= 30.0)
	)
	return 31
	if (
		(outputActualW <= 1280 && outputActualH <= 720 && outputFps <= 60.0)
		||
		(outputActualW <= 1280 && outputActualH <= 1024 && outputFps <= 42.2)
	)
	return 32
	if (
		(outputActualW <= 1280 && outputActualH <= 720 && outputFps <= 68.3)
		||
		(outputActualW <= 1920 && outputActualH <= 1080 && outputFps <= 30.1)
		||
		(outputActualW <= 2048 && outputActualH <= 1024 && outputFps <= 30.0)
	)
	return 41//4.0 and 4.1 differ only in max possible bitrate and refframes
	if (
		(outputActualW <= 1280 && outputActualH <= 720 && outputFps <= 145.1)
		||
		(outputActualW <= 1920 && outputActualH <= 1080 && outputFps <= 64.0)
		||
		(outputActualW <= 2048 && outputActualH <= 1080 && outputFps <= 60.0)
	)
	return 42
	if (
		(outputActualW <= 1920 && outputActualH <= 1080 && outputFps <= 72.3)
		||
		(outputActualW <= 2048 && outputActualH <= 1024 && outputFps <= 72.0)
		||
		(outputActualW <= 2048 && outputActualH <= 1080 && outputFps <= 67.8)
		||
		(outputActualW <= 2560 && outputActualH <= 1920 && outputFps <= 30.7)
		||
		// моя строка
		(outputActualW <= 2560 && outputActualH <= 1440 && outputFps <= 40)
		||
		(outputActualW <= 3672 && outputActualH <= 1536 && outputFps <= 26.7)
	)
	return 50
	if (
		(outputActualW <= 1920 && outputActualH <= 1080 && outputFps <= 120.5)
		||
		(outputActualW <= 2560 && outputActualH <= 1920 && outputFps <= 51.2)
		||
		// моя строка
		(outputActualW <= 2560 && outputActualH <= 1440 && outputFps <= 65.0)
		||
		(outputActualW <= 3840 && outputActualH <= 2160 && outputFps <= 31.7)
		||
		(outputActualW <= 4096 && outputActualH <= 2048 && outputFps <= 30.0)
		||
		(outputActualW <= 4096 && outputActualH <= 2160 && outputFps <= 28.5)
		||
		(outputActualW <= 4096 && outputActualH <= 2304 && outputFps <= 26.7)
	)
	return 51
	// https://developer.apple.com/documentation/http_live_streaming/http_live_streaming_hls_authoring_specification_for_apple_devices#2969552
	// 1.3b. Profile and Level for H.264 MUST be less than or equal to High Profile, Level 5.1.
	if (
		(outputActualW <= 1920 && outputActualH <= 1080 && outputFps <= 172.0)
		||
		(outputActualW <= 2560 && outputActualH <= 1920 && outputFps <= 108.0)
		||
		(outputActualW <= 3840 && outputActualH <= 2160 && outputFps <= 66.8)
		||
		(outputActualW <= 4096 && outputActualH <= 2048 && outputFps <= 63.3)
		||
		(outputActualW <= 4096 && outputActualH <= 2160 && outputFps <= 60.0)
		||
		(outputActualW <= 4096 && outputActualH <= 2304 && outputFps <= 56.3)
	)
	return 52
	if (
		(outputActualW <= 3840 && outputActualH <= 2160 && outputFps <= 128.9)
		||
		(outputActualW <= 7680 && outputActualH <= 4320 && outputFps <= 32.2)
		||
		(outputActualW <= 8192 && outputActualH <= 4320 && outputFps <= 30.2)
	)
	return 60
	if (
		(outputActualW <= 3840 && outputActualH <= 2160 && outputFps <= 257.9)
		||
		(outputActualW <= 7680 && outputActualH <= 4320 && outputFps <= 64.5)
		||
		(outputActualW <= 8192 && outputActualH <= 4320 && outputFps <= 60.4)
	)
	return 61
	if (
		(outputActualW <= 3840 && outputActualH <= 2160 && outputFps <= 300.0)
		||
		(outputActualW <= 7680 && outputActualH <= 4320 && outputFps <= 128.9)
		||
		(outputActualW <= 8192 && outputActualH <= 4320 && outputFps <= 120.9)
	)
	return 62
	throw new Error(`impossible avc level. outputActualW="${outputActualW}", outputActualH="${outputActualH}", outputFps="${outputFps}"`)
}

// https://ffmpeg.org/download.html
// discontinued ones are from https://ffmpeg.org/olddownload.html
export const ffmpegReleaseData: DeepReadonly<Array<{
	discontinued?: true,
	numericalVersion: [Uint32, Uint32, Uint32],
	title?: string,
	description?: string,
	libavfilter: [Uint32, Uint32, Uint32],
}>> =
[
	{
		numericalVersion: [5, 1, 0],
		libavfilter: [8, 40, 100],
	},
	{
		numericalVersion: [5, 0, 1],
		title: 'FFmpeg 5.0.1 "Lorentz"',
		description: '5.0.1 was released on 2022-04-04. It is the latest stable FFmpeg release from the 5.0 release branch, which was cut from master on 2022-01-04.',
		libavfilter: [8, 24, 100],
	},

	{
		numericalVersion: [4, 4, 2],
		title: 'FFmpeg 4.4.2 "Rao"',
		description: '4.4.2 was released on 2022-04-14. It is the latest stable FFmpeg release from the 4.4 release branch, which was cut from master on 2021-04-08.',
		libavfilter: [7, 110, 100],
	},

	{
		numericalVersion: [4, 3, 4],
		title: 'FFmpeg 4.3.4 "4:3"',
		description: '4.3.4 was released on 2022-04-16. It is the latest stable FFmpeg release from the 4.3 release branch, which was cut from master on 2020-06-08.',
		libavfilter: [7, 85, 100],
	},

	{
		numericalVersion: [4, 2, 7],
		title: 'FFmpeg 4.2.7 "Ada"',
		description: '4.2.7 was released on 2022-05-14. It is the latest stable FFmpeg release from the 4.2 release branch, which was cut from master on 2019-07-21.',
		libavfilter: [7, 57, 100],
	},

	{
		numericalVersion: [4, 1, 9],
		title: 'FFmpeg 4.1.9 "al-Khwarizmi"',
		description: '4.1.9 was released on 2022-04-17. It is the latest stable FFmpeg release from the 4.1 release branch, which was cut from master on 2018-11-02.',
		libavfilter: [7, 40, 101],
	},

	{
		discontinued: true,
		numericalVersion: [4, 0, 6],
		title: 'FFmpeg 4.0.6 "Wu"',
		description: '4.0.6 was released on 2020-07-03. It is the latest stable FFmpeg release from the 4.0 release branch, which was cut from master on 2018-04-16.',
		libavfilter: [7, 16, 100],
	},

	{
		numericalVersion: [3, 4, 11],
		title: 'FFmpeg 3.4.11 "Cantor"',
		description: '3.4.11 was released on 2022-05-14. It is the latest stable FFmpeg release from the 3.4 release branch, which was cut from master on 2017-10-11.',
		libavfilter: [6, 107, 100],
	},

	{
		discontinued: true,
		numericalVersion: [3, 3, 9],
		title: 'FFmpeg 3.3.9 "Hilbert"',
		description: '3.3.9 was released on 2018-11-18. It is the latest stable FFmpeg release from the 3.3 release branch, which was cut from master on 2017-04-02.',
		libavfilter: [6, 82, 100],
	},

	{
		numericalVersion: [3, 2, 18],
		title: 'FFmpeg 3.2.18 "Hypatia"',
		description: '3.2.18 was released on 2022-05-15. It is the latest stable FFmpeg release from the 3.2 release branch, which was cut from master on 2016-10-26.',
		libavfilter: [6, 65, 100],
	},

	{
		discontinued: true,
		numericalVersion: [3, 1, 11],
		title: 'FFmpeg 3.1.11 "Laplace"',
		description: '3.1.11 was released on 2017-09-25. It is the latest stable FFmpeg release from the 3.1 release branch, which was cut from master on 2016-06-26.',
		libavfilter: [6, 47, 100],
	},

	{
		discontinued: true,
		numericalVersion: [3, 0, 12],
		title: 'FFmpeg 3.0.12 "Einstein"',
		description: '3.0.12 was released on 2018-10-28. It is the latest stable FFmpeg release from the 3.0 release branch, which was cut from master on 2016-02-14.',
		libavfilter: [6, 31, 100],
	},

	{
		numericalVersion: [2, 8, 20],
		title: 'FFmpeg 2.8.20 "Feynman"',
		description: '2.8.20 was released on 2022-05-15. It is the latest stable FFmpeg release from the 2.8 release branch, which was cut from master on 2015-09-05. Amongst lots of other changes, it includes all changes from ffmpeg-mt, libav master of 2015-08-28, libav 11 as of 2015-08-28.',
		libavfilter: [5, 40, 101],
	},

	{
		discontinued: true,
		numericalVersion: [2, 7, 7],
		title: 'FFmpeg 2.7.7 "Nash"',
		description: '2.7.7 was released on 2016-04-30. It is the latest stable FFmpeg release from the 2.7.7 release branch, which was cut from master on 2015-06-09. Amongst lots of other changes, it includes all changes from ffmpeg-mt, libav master of 2015-06-10, libav 11 as of 2015-06-11.',
		libavfilter: [5, 16, 101],
	},

	{
		discontinued: true,
		numericalVersion: [2, 6, 9],
		title: 'FFmpeg 2.6.9 "Grothendieck"',
		description: '2.6.9 was released on 2016-05-03. It is the latest stable FFmpeg release from the 2.6 release branch, which was cut from master on 2015-03-06. Amongst lots of other changes, it includes all changes from ffmpeg-mt, libav master of 2015-03-06, libav 11 as of 2015-03-06.',
		libavfilter: [5, 11, 102],
	},

	{
		discontinued: true,
		numericalVersion: [2, 5, 11],
		title: 'FFmpeg 2.5.11 "Bohr"',
		description: '2.5.11 was released on 2016-02-02. It is the latest stable FFmpeg release from the 2.5 release branch, which was cut from master on 2014-12-15. Amongst lots of other changes, it includes all changes from ffmpeg-mt, libav master of 2014-12-03, libav 11 as of 2014-12-03.',
		libavfilter: [5, 2, 103],
	},

	{
		discontinued: true,
		numericalVersion: [2, 4, 14],
		title: 'FFmpeg 2.4.14 "Fresnel"',
		description: '2.4.14 was released on 2017-12-31. It is the latest stable FFmpeg release from the 2.4 release branch, which was cut from master on 2014-09-14. Amongst lots of other changes, it includes all changes from ffmpeg-mt, libav master of 2014-09-14, libav 11.4 as of 2015-08-25.',
		libavfilter: [5, 1, 100],
	},

	{
		discontinued: true,
		numericalVersion: [2, 3, 6],
		title: 'FFmpeg 2.3.6 "Mandelbrot"',
		description: '2.3.6 was released on 2015-01-06. It is the latest stable FFmpeg release from the 2.3 release branch, which was cut from master on 2014-07-16. Amongst lots of other changes, it includes all changes from ffmpeg-mt, libav master of 2014-07-15, libav 10.2 as of 2014-07-15.',
		libavfilter: [4, 11, 100],
	},

	{
		discontinued: true,
		numericalVersion: [2, 2, 16],
		title: 'FFmpeg 2.2.16 "Muybridge"',
		description: '2.2.16 was released on 2015-06-18. It is the latest stable FFmpeg release from the 2.2 release branch, which was cut from master on 2014-03-01. Amongst lots of other changes, it includes all changes from ffmpeg-mt, libav master of 2014-03-01, libav 10.7 as of 2015-06-18.',
		libavfilter: [4, 2, 100],
	},

	{
		discontinued: true,
		numericalVersion: [2, 1, 8],
		title: 'FFmpeg 2.1.8 "Fourier"',
		description: '2.1.8 was released on 2015-04-30. It is the latest stable FFmpeg release from the 2.1 release branch, which was cut from master on 2013-10-28. Amongst lots of other changes, it includes all changes from ffmpeg-mt, libav master of 2013-10-27, libav 9 as of 2013-10-27.',
		libavfilter: [3, 90, 100],
	},

	{
		discontinued: true,
		numericalVersion: [2, 0, 7],
		title: 'FFmpeg 2.0.7 "Nameless"',
		description: '2.0.7 was released on 2015-06-10. It is the latest stable FFmpeg release from the 2.0 release branch, which was cut from master on 2013-07-10. Amongst lots of other changes, it includes all changes from ffmpeg-mt, libav master of 2013-07-09, libav 9 as of 2013-07-09.',
		libavfilter: [3, 79, 101],
	},

	{
		discontinued: true,
		numericalVersion: [1, 2, 12],
		title: 'FFmpeg 1.2.12 "Magic"',
		description: '1.2.12 was released on 2015-02-12. It is the latest stable FFmpeg release from the 1.2 release branch, which was cut from master on 2013-03-07. Amongst lots of other changes, it includes all changes from ffmpeg-mt, libav master of 2013-03-07, libav 9 as of 2013-03-07.',
		libavfilter: [3, 42, 103],
	},

	{
		discontinued: true,
		numericalVersion: [1, 1, 16],
		title: 'FFmpeg 1.1.16 "Fire Flower"',
		description: '1.1.16 was released on 2015-03-13. It is the latest stable FFmpeg release from the 1.1 release branch, which was cut from master on 2013-01-06. Amongst lots of other changes, it includes all changes from ffmpeg-mt, libav master of 2013-01-06, libav 9.18 as of 2015-03-13.',
		libavfilter: [3, 32, 100],
	},

	{
		discontinued: true,
		numericalVersion: [1, 0, 10],
		title: 'FFmpeg 1.0.10 "Angel"',
		description: '1.0.10 was released on 2014-07-20. It is the latest stable FFmpeg release from the 1.0 release branch, which was cut from master on 2012-09-28. Amongst lots of other changes, it includes all changes from ffmpeg-mt, libav master of 2012-09-27, libav 0.8.3 as of 2013-01-22.',
		libavfilter: [3, 17, 100],
	},

	{
		discontinued: true,
		numericalVersion: [0, 11, 5],
		title: 'FFmpeg 0.11.5 "Happiness"',
		description: '0.11.5 was released on 2014-03-10. It is the latest stable FFmpeg release from the 0.11 release branch, which was cut from master on 2012-05-25. Amongst lots of other changes, it includes all changes from ffmpeg-mt, libav master of 2012-05-24, libav 0.8.2 as of 2012-09-19.',
		libavfilter: [2, 77, 100],
	},

	{
		discontinued: true,
		numericalVersion: [0, 10, 16],
		title: 'FFmpeg 0.10.16 "Freedom"',
		description: '0.10.16 was released on 2015-03-12. It is the latest stable FFmpeg release from the 0.10 release branch, which was cut from master on 2012-01-26. Amongst lots of other changes, it includes all changes from ffmpeg-mt, libav master of 2012-01-26, libav 0.8.17 as of 2015-03-12.',
		libavfilter: [2, 61, 100],
	},
]

//export const isOutputVideo: 