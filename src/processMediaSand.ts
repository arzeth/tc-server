/*
 * Copyright © 2020-2022 Arzet Ro <arzeth0@gmail.com>
 *
 * This source code is licensed under the MIT No Attribution License:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
// https://developer.apple.com/documentation/http_live_streaming/http_live_streaming_hls_authoring_specification_for_apple_devices
// https://developer.apple.com/documentation/http_live_streaming/http_live_streaming_hls_authoring_specification_for_apple_devices/hls_authoring_specification_for_apple_devices_appendixes

import fsAsync, { FileHandle } from 'fs/promises'
//import { userInfo as osUserInfo } from 'os'
//import { unlinkSync as fsUnlinkSync } from 'fs'
import { open as fsPromisesOpen } from 'fs/promises'
//import { default as __PQueue_default } from 'p-queue'
import { default as PQueue } from 'p-queue'
import kill from 'tree-kill'
import { evaluate as mthEvaluate } from 'mathjs'
//import { div as DecimalDiv } from 'decimal.js'
import Decimal from 'decimal.js-light'
//import { join as pathJoin } from 'path'
import { cpus as osCpus } from 'os'
import * as commons from '../common-server-code/index.js'
import {
	spawnAsyncUTF8,
	shellSpawnAsyncUTF8,
	spawnAsyncVoid,
	escapeSpawnArg,
	safeUnlinkAsync,
	gracefullyReadFileUtf8,
	safeMkdirAsync,
	pathJoinSmart,

	Output_vActual,
	Output_vLandscape,
	Output_vActual2,
	Output_vLandscape2,
	Output_v,
	Output_a,
	Output_i,
	Output_multi,
	OutputSet,
	MediaPremeta,

	engine,
	AdaptationSetA,
	AdaptationSetV,
	ListStats,
	AbortController2,
} from '../common-server-code/index.js'
import gpuInfo, { GraphicsControllerDataExtended } from './gpuInfo.js'
import * as utils from './utils.js'
//import { string } from 'yargs'
//import { config } from './tc-server.js'


// https://cdn.1meditat.com/b36a4744feb3fd9d2014e9e30b8dfcdfe2c847bc49eabf20f2f7346ca421fd1f/abs/manifest.mpd
// https://cdn.1meditat.com/ia/abs/manifest.mpd

export let FFMPEG_PATH: string = (
	await commons.fsExistsAsync(process.env.FFMPEG_PATH!)
	? process.env.FFMPEG_PATH!
	: ''
)
if (!FFMPEG_PATH) FFMPEG_PATH = (
	await commons.fsExistsAsync('/opt/ffmpeg-sand2/bin/ffmpeg')
	? '/opt/ffmpeg-sand2/bin/ffmpeg'
	: 'ffmpeg'
)

export let FFPROBE_PATH: string = (
	await commons.fsExistsAsync(process.env.FFPROBE_PATH!)
	? process.env.FFPROBE_PATH!
	: ''
)
if (!FFPROBE_PATH) FFPROBE_PATH = (
	await commons.fsExistsAsync('/opt/ffmpeg-sand2/bin/ffprobe')
	? '/opt/ffmpeg-sand2/bin/ffprobe'
	: 'ffprobe'
)

const enum Protocol {
	FILE,
	HTTP_OR_DEV_OR_SOCK,
	SDP,
	RTMP,
	SRT,
}

const pk: 'ff'|'sh' = 'ff'/*'ff'||(
	outputMediaManifestFilePath.includes('/69/9/')
	? 'sh'
	: 'ff'
)*/

const IS_ARZET_PC: boolean = process.env.USER === 'arzeth'

const CPU_THREADS: number = osCpus().length

// usr

const SDP_TEST: boolean = false
// @ts-ignore
if (SDP_TEST === true) console.log('SDP_TEST=TRUE'.red)

// -vf 'select=not(mod(n\,6)),scale=160:90,tile=5x5' ${ffmpegVsyncParamName} passthrough -vcodec libwebp -qscale:v 53 -compression_level 6 x%03d.webp
//+ `  -i ${escapeSpawnArg(inputFile)} -c:v select=not(mod(n\\,6)),scale=160:90,tile=5x5 ${ffmpegVsyncParamName}:v passthrough -c:v libwebp -qscale:v 83 -compression_level:v 6 ${thumbTplPath}`

const hasNvidiaGpu: boolean = /NVIDIA\s+CUDA/i.test((await spawnAsyncUTF8('clinfo', []))[1])
const hasIntelGpu: boolean = /*process.env.QQ === 'Q' && */ /*!hasNvidiaGpu && */ /Intel\s*\(R\)|Intel\s*®/i.test((await spawnAsyncUTF8('vainfo', []))[1])


let ffmpegLibs: Record<string, [u32, u32, u32]> = {
	libavutil: [0, 0, 0],
	libavcodec: [0, 0, 0],
	libavformat: [0, 0, 0],
	libavdevice: [0, 0, 0],
	libavfilter: [0, 0, 0],
	libswscale: [0, 0, 0],
	libswresample: [0, 0, 0],
	libpostproc: [0, 0, 0],
}
const ffmpegVersion = await (async () => {
	const k = (await spawnAsyncUTF8(FFMPEG_PATH, ['-version']))[1]

	const vvv = k.match(
		// That's two or three numbers.
		/ffmpeg(?: v(?:\.|er(?:sion|\.)?)?)? [a-z \-\.]*(\d+)\.(\d+)(?:\.(\d+))?(?![.\-]git)/i
	) // there was "0.9.1.git"
	if (vvv)
	{
		return vvv.slice(1,4).map(x => +x || 0)
	}

	const ffmpegLibsMatch = Array.from(k.matchAll(/(lib[a-z_\-]+)\s+(\d+)\s*\.\s*(\d+)\s*\.\s*(\d+)/g))
	if (!ffmpegLibsMatch?.length) throw new Error('ffmpeg -version failed')
	ffmpegLibsMatch.forEach(([_, libname, v, vv, vvv]) => {
		ffmpegLibs[libname] = [+v, +vv, +vvv]
	})
	//?.slice(1).map(x=>+x)

	/*const yearCompiledOn = k.match(/\(c\) 2000-(\d+)/)
	if (yearCompiledOn)
	{
		// @ts-ignore
		return yearCompiledOn[1] >= 2021 ? [4,4,0] : [4,3,4]
	}*/
	return utils.ffmpegReleaseData.filter(({libavfilter}) => {
		return (
			ffmpegLibs.libavfilter[0] >= libavfilter[0]
			&&
			ffmpegLibs.libavfilter[1] >= libavfilter[1]
			//&&
			//ffmpegLibs.libavfilter[2] >= libavfilter[2]
		)
	})[0]?.numericalVersion || [0, 0, 0]
})() as [number, number, number]
if (ffmpegVersion[0] < 3) throw new Error(`Your FFmpeg version ${ffmpegVersion.join('.')} is earlier than 3.0.`)
export const isFfmpegMin51 = ffmpegVersion[0] > 5 || (ffmpegVersion[0] === 5 && ffmpegVersion[1] >= 1)
export const isFfmpegMin50 = ffmpegVersion[0] > 5 || (ffmpegVersion[0] === 5 && ffmpegVersion[1] >= 0)
export const isFfmpegMin44 = ffmpegVersion[0] > 4 || (ffmpegVersion[0] === 4 && ffmpegVersion[1] >= 4)
export const isFfmpegMin43 = ffmpegVersion[0] > 4 || (ffmpegVersion[0] === 4 && ffmpegVersion[1] >= 3)
export const isFfmpegMin42 = ffmpegVersion[0] > 4 || (ffmpegVersion[0] === 4 && ffmpegVersion[1] >= 2)
export const isFfmpegMin41 = ffmpegVersion[0] > 4 || (ffmpegVersion[0] === 4 && ffmpegVersion[1] >= 1)
export const isFfmpegMin40 = ffmpegVersion[0] > 4 || (ffmpegVersion[0] === 4 && ffmpegVersion[1] >= 0)

// -vsync is deprecated since 5.1 https://github.com/ffmpeg/FFmpeg/commit/09c53a04c5892baee88872fbce3df17a00472faa
//const ffmpegVsyncParamName = isFfmpegMin51 ? '-fps_mode' : '-vsync'

export const supports$hls_master_name = isFfmpegMin44
export const supports$scale_cuda__interp_algo__lanczos = isFfmpegMin44

export const ffmpegEncoders = (await spawnAsyncUTF8(FFMPEG_PATH, ['-encoders']))[1]
export const ffmpegDecoders = (await spawnAsyncUTF8(FFMPEG_PATH, ['-decoders']))[1]
export const ffmpegMuxers   = (await spawnAsyncUTF8(FFMPEG_PATH, ['-muxers']))[1]
export const ffmpegFilters  = (await spawnAsyncUTF8(FFMPEG_PATH, ['-filters']))[1]

const has$Bs2b = /\sbs2b/.test(ffmpegFilters)
if (has$Bs2b) console.log('ffmpeg is built with --enable-libbs2b, that\'s good.'.green)
else console.warn('ffmpeg ' + 'is not built'.red + ' with --enable-libbs2b, that\'s a little bit bad for headphone listeners.')

const has$Afftdn = /\safftdn/.test(ffmpegFilters)

const has$Nvenc = /[.D]\s(?:h264_)?nvenc/.test(ffmpegEncoders)
const has$Nvdec = /[.D]\sh264_cuvid/i.test(ffmpegDecoders)
const has$CudaLlvm = has$Nvenc// && /enable-cuda-llvm/i.test(ffmpegBuildConf)
if (hasNvidiaGpu)
{
	if (!has$Nvenc)
	{
		console.warn('You have an NVIDIA GPU, but ffmpeg is built without --enable-nvenc'.yellow)
	}
	if (!has$Nvdec)
	{
		console.warn('You have an NVIDIA GPU, but ffmpeg is built without --enable-nvdec'.yellow)
	}
	if (has$Nvdec && !has$CudaLlvm)
	{
		console.warn('If you are going to use an NVIDIA GPU for decoding, then it is recommended to rebuild your ffmpeg with also --enable-cuda-llvm (requires cuda package 3.4GB)'.yellow)
	}
}
const has$LibaomAv1 = /[.D]\slibaom-av1/.test(ffmpegEncoders) //--enable-libaom
const has$Libsvtav1 = /[.D]\slibsvtav1/.test(ffmpegEncoders) // SVT-AV1
const has$Librav1e = /[.D]\slibrav1e/.test(ffmpegEncoders)
const has$Libdav1d = /[.D]\slibdav1d/.test(ffmpegDecoders)
const has$QsvAv1Decoder = /[.D]\sav1_qsv/.test(ffmpegDecoders)
const has$Av1Enc = has$Libsvtav1 || has$LibaomAv1 || has$Librav1e
const has$Av1Dec = has$Libsvtav1 || has$LibaomAv1 || has$Libdav1d || has$QsvAv1Decoder
if (!has$Av1Dec)
{
	if (has$QsvAv1Decoder)
	{

	}
	else
	{
		console.warn('ffmpeg is built with neither --enable-libsvtav1 (SVT-AV1) nor enable-libaom nor enable-libdav1d , which means ' + 'input files with AV1 videocodec are not supported.'.red)
	}
}
const has$Qsv = /[.D]\sh264_qsv/i.test(ffmpegDecoders)
const has$Soxr = /[.ED]\ssox\s/i.test(ffmpegMuxers) // or demuxers
if (!has$Soxr) console.warn('ffmpeg ' + 'is not built'.red + ' with --enable-libsoxr, that\'s bad.')
const has$Libopus = /[.D]\slibopus/i.test(ffmpegDecoders)// ffmpegEncoders has it too but with "A....." instead of "A....D"
if (!has$Libopus) console.warn('ffmpeg ' + 'is not built'.red + ' with --enable-libopus, that\'s very bad.')
const has$Libfdk_aac = (
	process.env.ENABLE_FDKAAC === '0'
	||
	process.env.ENABLE_LIBFDKAAC === '0'
) ? false : /[.D]\slibfdk_aac/i.test(ffmpegEncoders)
if (has$Libfdk_aac) console.log('ffmpeg is built with --enable-libfdk-aac, that\'s good.'.green)
else console.warn('ffmpeg ' + 'is not built'.red + ' with --enable-libfdk-aac, that\'s very bad. libfdk_aac (that\'s an audio encoder) provides 30% better quality/bitrate ratio, and supports HE-AAC (so +40% quality than default `aac` encoder at <=64 kbit/s).')


// Nota Bene: Opus (codec itself, not its implementations) supports only 48000,24000,16000,12000,8000 by design.
const GOOD_AUDIO_SAMPLE_RATES = [44100, 48000, 22050]

//const PQueue = __PQueue_default.default as typeof __PQueue_default
const tcQueue     = new PQueue({concurrency: Math.max(Math.floor(CPU_THREADS / 2), 1)});
const imagesQueue = new PQueue({concurrency: Math.max(Math.floor(CPU_THREADS / 2), 1)});
//const tcJobMultiFileLogFilesByController: Map<KillMethods, FileHandle> = new Map()
//const tcJobMultiFileLogFileWriteQueueByController: Map<KillMethods, PQueue> = new Map()
//const tcToUpQueue: Map<ServerId, typeof __PQueue_default> = new Map

tcQueue.on('add', () => {
	console.log(`Transcode task is added.  Queue size: ${tcQueue.size}  Pending: ${tcQueue.pending}`);
});
tcQueue.on('next', () => {
	console.log(`Transcode task is completed.  Queue size: ${tcQueue.size}  Pending: ${tcQueue.pending}`);
});
/*tcToUpQueue.on('add', () => {
	console.log(`'Upload the transcoded file' task is added.  Queue size: ${tcToUpQueue.size}  Pending: ${tcToUpQueue.pending}`);
});
tcToUpQueue.on('next', () => {
	console.log(`'Upload the transcoded file' task is completed.  Queue size: ${tcToUpQueue.size}  Pending: ${tcToUpQueue.pending}`);
});*/






const TestSegmentsModeDebug = false
// WebM !== VP8/9. WebM is a container.
const WEBM_BANNED = !isFfmpegMin42
const IS_OPUS_BANNED = WEBM_BANNED || true//1.34x vs 1.4x speed, i.e. 4.3%
const IS_AAC_BANNED = false
const VERTICAL_VIDEOS_ALLOWED = true
//const IS_LIVE = true
//const IS_LIVE_MEGALL = true

//const OFFLINE_SEGMENT_DURATION = 2.0
//const OFFLINE_KEYFRAME_SIZE = 2


// 1.18. VOD content SHOULD use a natural frame rate for the content. Any of the following frame rates: 23.976, 24, 25, 29.97, 30, 50, 59.94, and 60 fps are supported.
// Вроде бы для разрешений ниже 720p можно только <=30 FPS, поэтому эта функция существует
/*
Таки 50 превращать в 25, а не 30, ибо (...хотя SHOULD это, а не MUST):
3.5. + Inserted media SHOULD be at a similar frame rate (x, 2x, x/2) as the other content in the Media Playlist.
8.24. + Frame rate changes at discontinuities SHOULD use a similar frame rate (x, 2x, x/2).
9.21. + You SHOULD provide a full range of variants for each codec type and frame rate. Similar frame rates (x, 2x, x/2) are compatible, nonsimilar frame rates may cause playback issues.
*/
// TODO?: if not integer (~23.976023976===24000/1001), then use 24000/1001 in ffmpeg
const lowerFps = (fps: number): OutputFramerate => /*fps === 50 ? 30 : */(fps > 30 ? engine.toPrecision(fps / 2, 4) : fps)

// https://streaminglearningcenter.com/articles/open-and-closed-gops-all-you-need-to-know.html
// 2. By default, x264 produces closed GOPs, so you don’t have to do anything special with x264 to produce closed GOPs. If you’re using a different H.264 codec, check its default setting.
// 3. By default, x265 produces open GOPs. To produce a closed GOP, add open-gop=0 to your x265 FFmpeg command string.

// iphone 7 supports max 160kbps aac
const MAX_AAC_BITRATE_KBPS = 160


type VAccelApi = 'sw'|'nv'|'vaapi'|'qsv'
// if sw, then number = threads, else number = which GPU
type GpuVariant = (
	['sw', number]//0 not allowed
	|
	[Exclude<VAccelApi, 'sw'>, number|'rand'|'load']//starting from 0 // todo: bus addresses
)
type GpuVariants = Array<GpuVariant>

type HowMuchLive = 0|1|2|3|4


const possibleOutputFramerates = [25,50, 30,60] as const
Object.freeze(possibleOutputFramerates)
type OutputFramerate = typeof possibleOutputFramerates[number]

const possibleOutputLandscapeHeightsForNow = [1080, 720, 486, 360, 234, 144,] as const
const minPossibleOutputLandscapeHeightForNow = Math.min(...possibleOutputLandscapeHeightsForNow) as Last<typeof possibleOutputLandscapeHeightsForNow>
const maxPossibleOutputLandscapeHeightForNow = Math.max(...possibleOutputLandscapeHeightsForNow) as typeof possibleOutputLandscapeHeightsForNow[0]

const possibleOutputLandscapeWidthsForNow = [1080/9*16, 720/9*16, 486/9*16, 360/9*16, 234/9*16, 144/9*16,] as const
const minPossibleOutputLandscapeWidthForNow = Math.min(...possibleOutputLandscapeWidthsForNow) as Last<typeof possibleOutputLandscapeWidthsForNow>
const maxPossibleOutputLandscapeWidthForNow = Math.max(...possibleOutputLandscapeWidthsForNow) as typeof possibleOutputLandscapeWidthsForNow[0]

if (process.env.PH) // for debug
{
	const newPh = process.env.PH.split(/[,:.\s]+/).map(x=>engine.atoi(x)).filter(x=>x>0)
	if (newPh.length)
	{
		// @ts-ignore
		while (possibleOutputLandscapeHeightsForNow.length) possibleOutputLandscapeHeightsForNow.pop()
		// @ts-ignore
		while (possibleOutputLandscapeWidthsForNow.length) possibleOutputLandscapeWidthsForNow.pop()
		// @ts-ignore
		for (const x of newPh)
		{
			// @ts-ignore
			possibleOutputLandscapeHeightsForNow.push(x)
			// @ts-ignore
			possibleOutputLandscapeWidthsForNow.push(engine.roundToEven(x/9*16))
		}
	}
}
//const possibleOutputHeightsForNow = [720, 144] as const
Object.freeze(possibleOutputLandscapeHeightsForNow)





const fixFinalOutputV = (
	_x: Output_vActual|Output_vLandscape|Output_vActual2|Output_vLandscape2,
	howMuchLive: HowMuchLive,
	origOrientation: Orientation,
	origFps: Urational,
	origLandscapeH: u32,
	origVideoCodec: string,
	origVideoBitrateInBits: Uint64,
	outputMinFramerate: Urational,
	outputMaxFramerate: Urational,
	hacks: NonNullable<Parameters<typeof processMedia>[0]['hacks']>,
): DeepRequired<Output_vActual2> => {
	const x = _x as Output_vActual2
	const origFpsAsNumber = origFps.$fromRational()
	const outputMinFramerateAsNumber = outputMinFramerate.$fromRational()
	const outputMaxFramerateAsNumber = outputMaxFramerate.$fromRational()
	if (
		engine.isSafePositiveInteger(+(_x as Output_vLandscape2).lw)
		&&
		engine.isSafePositiveInteger(+(_x as Output_vLandscape2).lh)
	)
	{
		// fixme: use outputOrientation?
		;(x as Output_vActual2).w = origOrientation === 'v' ? +(_x as Output_vLandscape2).lh : +(_x as Output_vLandscape2).lw
		;(x as Output_vActual2).h = origOrientation === 'v' ? +(_x as Output_vLandscape2).lw : +(_x as Output_vLandscape2).lh
		// @ts-ignore
		delete x.lw
		// @ts-ignore
		delete x.lh
	}
	else if (!x.w)
	{
		throw '!!'
	}


	const finalLandscapeHeight = Math.min(x.w, x.h)
	
	// fixme: min, max
	if (!x.fps?.$isPositiveRational()) // also checks whether it is a valid rational
	{
		/*if (origFpsAsNumber === 23.976)//23.9635
		{
		}
		else *//*if (origFpsAsNumber <= 24)
		{
			x.fps = [24,1]
		}
		else */if (outputMinFramerateAsNumber <= 25 && (outputMaxFramerateAsNumber < 30 || origFpsAsNumber <= 26/* && possibleOutputFramerates.includes(25)*/))
		{
			x.fps = [25,1]
		}
		// it is outputMaxFramerateAsNumber < (next if's x.fps=)[0]
		else
		{
			/*if (origFpsAsNumber > 25 && origFpsAsNumber < 29.97)
			{
				if (origFpsAsNumber > 26) x.fps = [30,1]
				else x.fps = [25,1]
			}
			else if (origFpsAsNumber === 29.9700315) x.fps = [origFpsAsNumber,1]
			else if (origFpsAsNumber === 59.940063) x.fps = [origFpsAsNumber,1]
			else if (origFpsAsNumber > 29.97 && origFpsAsNumber < 30) x.fps = [30,1]*/
			if (outputMinFramerateAsNumber <= 30 && (outputMaxFramerateAsNumber < 50 || origFpsAsNumber < 40/* && possibleOutputFramerates.includes(30)*/)) x.fps = [30,1]
			else if (outputMinFramerateAsNumber <= 50 && (outputMaxFramerateAsNumber < 60 || (origFpsAsNumber >= 40 && origFpsAsNumber <= 51/* && possibleOutputFramerates.includes(50)*/))) x.fps = [50,1]
			else x.fps = [60,1]
			//else if (origFpsAsNumber > 50 && origFpsAsNumber < 59.94) x.fps = [60,1]
			//else if (origFpsAsNumber > 59.94 && origFpsAsNumber !== 60) x.fps = [60,1]
		}
		if (x.fps.$fromRational() > 30)
		{
			if (howMuchLive || finalLandscapeHeight < 720) x.fps = x.fps.$fromRational() % 25 === 0 ? [25,1] : [30,1]// /= 2
		}
	}
	else if (typeof x.fps === 'number')
	{
		x.fps = utils.numberToFractional(x.fps)
	}
	/*if (outputMaxFramerate) x.fps = Math.min(
		newFPSForHighResolutions,
		outputMaxFramerate,
	)*/
	x.hdr = false

	//x.bitrateInBits = 1000 * Math.min(origBitrateKbps, Math.round(1900 * (h/720)))
	x.bitrateInBits = engine.getIdealBitrateInBits(
		howMuchLive, 'h264', finalLandscapeHeight, x.fps.$fromRational(),
		['h264','vp8','vp9','vp9_0','av1'].includes(origVideoCodec) ? {
			bitrateInBits: origVideoBitrateInBits,
			codec: origVideoCodec as 'h264'|'vp8'|'vp9'|'vp9_0'|'av1',
			landscapeHeight: origLandscapeH,
			fps: origFps,
		} : void 0
	)
	return x as any//DeepRequired<Output_vActual2>
}

const fixFinalOutputA = (
	x: Output_a,
	//origAudioBitrateInBits: u32
	hacks: NonNullable<Parameters<typeof processMedia>[0]['hacks']>,
	lowestOutputFps: Urational,
	SEGMENT_DURATION_float: Secs,
): DeepRequired<Output_a> => {
	if (x.codec === 'copy')
	{
		//x.bs2b = false
		x.cutoff = 0
		//x.noiseCancel = false
		x.bitrateInBits = 0//origAudioBitrateInBits
		return x as any
	}
	const lowestOutputFpsAsNumber = lowestOutputFps.$fromRational()
	const isLosslessCodec = false//x.codec === 'flac' || x.codec === 'alac'
	if (x.bitrateInBits < (isLosslessCodec ? 12_000 : 2300)) x.bitrateInBits *= 1000
	//if (x.bs2b === void 0 || x.bs2b === true) x.bs2b = {profile: 'jmeier'}
	//if (x.noiseCancel === void 0 || x.noiseCancel === 'afftdn') x.noiseCancel = {filter: 'afftdn', params: {}}
	if (x.codec === 'he-aac_v2')
	{
		if (!has$Libfdk_aac)
		{
			// console.warn('') todo
			x.codec = 'aac-lc'
		}
		else if (
			x.codec === 'he-aac_v2'
			&&
			!hacks.allowSegmentDurationNotBeExact
			&&
			!(
				(lowestOutputFpsAsNumber === 25 && !+(new Decimal(SEGMENT_DURATION_float)).mod(0.64).toFixed(11))
				||
				(lowestOutputFpsAsNumber === 30 && !+(new Decimal(SEGMENT_DURATION_float)).mod(1.066666666666666).toFixed(11))
			)
		)
		{
			// console.warn('') todo
			x.codec = 'aac-lc'
		}
		else if (!hacks.allowHeAacForMoreThan64kbps && x.bitrateInBits > 64e3)
		{
			// console.warn('') todo
			x.bitrateInBits = 64e3
		}
	}

	//if (x.cutoff === Infinity) throw new Error('Use 0 instead of Infinity for \`cutoff\`; because JSON doesn\'t support (-)Infinity/NaN, and you probably use JSON.')
	if (x.cutoff === Infinity) throw new Error('\`cutoff\` cannot be Infinity')
	if (x.cutoff === 'helperDefaults' || x.cutoff === 'encoderDefaults' || x.cutoff === 'disabled')
	{

	}
	else
	{
		const fixedCutoff = engine.fixAudioSampleRateNumber(x.cutoff)
		if (Number.isNaN(fixedCutoff)) throw new Error('Bad value for `cutoff`: `' + x.cutoff + '`')

		if (fixedCutoff === null || fixedCutoff === 0)
		{
			if (Object.hasOwn(x, 'cutoff')) console.warn('cutoff cannot be 0, falling back to cutoff="helperDefaults"')
			x.cutoff = 'helperDefaults'
		}
		else if (fixedCutoff < ((22_050+255) >> 8) || fixedCutoff > 20_000) // fixme: change 22_050 to the output sample rate
		{
			/*
			libavcodec/libfdk-aacenc.c:
					if (avctx->cutoff < (avctx->sample_rate + 255) >> 8 || avctx->cutoff > 20000) {
						av_log(avctx, AV_LOG_ERROR, "cutoff valid range is %d-20000\n",
								(avctx->sample_rate + 255) >> 8);
								avctx->sample_rate = 48000, etc.
								*/
			throw new Error(`\`cutoff\` valid range is ${(24_000+255) >> 8}-20000, supplied: ${x.cutoff}`)
		}
		// @ts-ignore
		else//if (fixedCutoff === 0 || fixedCutoff > 0) // null >= 0 is true
		{
			x.cutoff = fixedCutoff!
		}
	}
	return x as any//DeepRequired<Output_a>
}



const getVideoParams = (
	v: DeepRequired<Output_vActual2>,
	howMuchLive: HowMuchLive,
	hasInputClearEnd: boolean,
	finalOutputShouldBeRotated: boolean,
	// auto and number are for TS
	venc: /*number|'auto'*/|[GraphicsControllerDataExtended|number, VAccelApi]|GpuVariants,// number is number of CPU threads. >= 1. -1 is auto
	vdec: /*number|'auto'*/|[GraphicsControllerDataExtended|number, VAccelApi]|GpuVariants,
	origLandscapeW: u32,
	origLandscapeH: u32,
	origFps: Urational,
	origVideoCodec: string,
	origVideoBitrateInBits: u32,
	keyFrameEverySeconds: Floto,
	whichYUV: string,
	isInterlaced: boolean,
	pr: Protocol,
	idx?: void|null|number,
) =>
{
	console.log(
`getVideoParams(
	v=%O
	howMuchLive=%O
	hasInputClearEnd=%O
	finalOutputShouldBeRotated=%O
	venc=%O
	vdec=%O
	origLandscapeW=%O
	origLandscapeH=%O
	origFps=%O
	origVideoCodec=%O
	origVideoBitrateInBits=%O
	keyFrameEverySeconds=%O
	whichYUV=%O
	isInterlaced=%O
	pr=%O
	idx=%O
)`,
		v,
		howMuchLive,
		hasInputClearEnd,
		finalOutputShouldBeRotated,
		// auto and number are for TS
		venc,
		vdec,
		origLandscapeW,
		origLandscapeH,
		origFps,
		origVideoCodec,
		origVideoBitrateInBits,
		keyFrameEverySeconds,
		whichYUV,
		isInterlaced,
		pr,
		idx,
	)
	// @ts-ignore
	if (venc === 'auto') throw 'venc === auto'
	// @ts-ignore
	if (vdec === 'auto') throw 'vdec === auto'
	// @ts-ignore
	if (typeof venc === 'number') throw 'venc is number'
	// @ts-ignore
	if (typeof vdec === 'number') throw 'vdec is number'
	// https://www.theoplayer.com/blog/encoding-cost-efficient-streaming
	// We advise against the usage of B-frames, as this requires the entire chunk to be available before playback can start.

	const III = typeof idx === 'number' ? ':' + idx : ''
	/*const w = (
		Math.ceil( h * (1920 / 1080))
		% 2 === 0
		? Math.ceil( h * (1920 / 1080))
		: Math.floor(h * (1920 / 1080))
	)*/
	const pixelformat: string = whichYUV !== '420p' ? 'format=pix_fmts=yuv420p' : ''

	// -vf "scale=iw*min(405/iw\,320/ih):ih*min(405/iw\,320/ih),pad=405:320:(405-iw)/2:(320-ih)/2"
	//-vf "scale=(iw*sar)*min(TARGET_WIDTH/(iw*sar)\,TARGET_HEIGHT/ih):ih*min(TARGET_WIDTH/(iw*sar)\,TARGET_HEIGHT/ih),      pad=TARGET_WIDTH:TARGET_HEIGHT:(TARGET_WIDTH-iw)/2:(TARGET_HEIGHT-ih)/2"
	//-vf "scale=min(iw*TARGET_HEIGHT/ih\,TARGET_WIDTH):min(TARGET_HEIGHT\,ih*TARGET_WIDTH/iw),
	//     pad=TARGET_WIDTH:TARGET_HEIGHT:(TARGET_WIDTH-iw)/2:(TARGET_HEIGHT-ih)/2"




	const vFpsAsNumber: Floto = v.fps.$fromRational()
	const origFpsAsNumber: Floto = origFps.$fromRational()

	const landscapeWidth  = Math.max(v.w, v.h)
	const landscapeHeight = Math.min(v.w, v.h)
	//const finalW = finalOutputShouldBeRotated ? h : w
	//const finalH = finalOutputShouldBeRotated ? w : h
	//const newFPS: OutputFramerate = h >= 720 ? newFPSForHighResolutions : lowerFps(newFPSForHighResolutions)

	const _fn = (vvvendorShort: string, t: 'enc'|'dec', v: 'qsv'|'vaapi'|'nv') => {
		//;(t === 'dec' ? vdec as GpuVariants : venc as GpuVariants).filter
		const list = (t === 'dec' ? vdec as GpuVariants : venc as GpuVariants)
		/*.filter(x => x[0] === v)*/.map(
			([vaacel,which]) => {
				//gpuInfo.filter(y => y.)
				const ret = gpuInfo.filter(x=>x.vendorShort === vvvendorShort)[which as number] // TODO
				//console.log('o %O %O vendorShort`%O` %O', gpuInfo, ret, vendorShort, which)
				return ret
				/*
				if (reqV[1] === 'rand') WWenc.push(list[0])
				else if (typeof reqV[1] === 'number')
				{
					if (reqV[1] >= 0)
					{
						const p = list[reqV[1]]
						if (p) WWenc.push(p)
						else throw 'not found ' + p
					}
				}
				*/
			}
		).filter(x=> x?.[t][v].includes(origVideoCodec)) as Array<GraphicsControllerDataExtended>
		if (list.length)
		{
			return list[0]
		}
	}

	let finalVdec: VAccelApi = void 0
	let finalVdecThreadsOrGpuInfo: GraphicsControllerDataExtended|number = void 0
	if (typeof vdec === 'string')
	{
		throw 'vdec is string'
	}
	//console.log('vdec: %O', vdec)
	if (typeof vdec[1] === 'string')
	{
		;[finalVdecThreadsOrGpuInfo, finalVdec] = vdec as [GraphicsControllerDataExtended|number, VAccelApi]
	}
	else if (origLandscapeW / origLandscapeH === 16/9)
	{
		const x = _fn('nv', 'dec', 'nv')
		if (x)
		{
			finalVdec = 'nv'
			finalVdecThreadsOrGpuInfo = x
		}
	}
	if (!finalVdec)
	{
		if (!whichYUV.includes('p10'))
		{
			const x = _fn('intel', 'dec', 'qsv')
			if (x)
			{
				finalVdec = 'qsv'
				finalVdecThreadsOrGpuInfo = x
			}
		}
		if (!finalVdec)
		{
			const x = (vdec as GpuVariants).filter(x => x[0] === 'sw')[0]
			if (x)
			{
				finalVdec = 'sw'
				finalVdecThreadsOrGpuInfo = x[1] as number
				if (finalVdecThreadsOrGpuInfo === -1) finalVdecThreadsOrGpuInfo = howMuchLive ? 8 : 8
			}
		}
	}

	let finalVenc: VAccelApi = void 0
	let finalVencThreadsOrGpuInfo: GraphicsControllerDataExtended|number = void 0
	console.log('venc: %O', venc)
	if (typeof venc[1] === 'string')
	{
		;[finalVencThreadsOrGpuInfo, finalVenc] = venc as [GraphicsControllerDataExtended|number, VAccelApi]
	}
	else if (origLandscapeW / origLandscapeH === 16/9)
	{
		const x = _fn('nv', 'enc', 'nv')
		if (x)
		{
			finalVenc = 'nv'
			finalVencThreadsOrGpuInfo = x
		}
	}
	if (!finalVenc)
	{
		if (!whichYUV.includes('p10') && howMuchLive/* && possibleOutputHeightsForNow.includes(landscapeHeight as any)*/)
		{
			const x = _fn('intel', 'enc', 'qsv')
			//console.log(x)
			//consoleueouo()
			if (x)
			{
				finalVenc = 'qsv'
				finalVencThreadsOrGpuInfo = x
			}
		}
		if (!finalVenc)
		{
			const x = (venc as GpuVariants).filter(x => x[0] === 'sw')[0]
			if (x)
			{
				finalVenc = 'sw'
				finalVencThreadsOrGpuInfo = x[1] as number
				if (finalVencThreadsOrGpuInfo === -1) finalVencThreadsOrGpuInfo = Infinity//howMuchLive ? 8 : 8
			}
		}
	}
	if (finalVenc === 'sw' && typeof finalVencThreadsOrGpuInfo === 'number')
	{
		//if (finalVencThreadsOrGpuInfo === Infinity/* && h > 144*/)
		{
			//finalVencThreadsOrGpuInfo = CPU_THREADS
		}
		//else
		{
			/*if (finalVencThreadsOrGpuInfo !== CPU_THREADS)
			{
				console.log(finalVencThreadsOrGpuInfo)
				console.trace()
				finalVencThreadsOrGpuInfo = CPU_THREADS
			}*/
			// https://blog.otterbro.com/x264-threads-we-gotta-talk/
			// https://github.com/corecodec/x264/blob/5493be84cdccecee613236c31b1e3227681ce428/encoder/encoder.c#L568
			finalVencThreadsOrGpuInfo = Math.max(1, Math.min(finalVencThreadsOrGpuInfo, Math.round((landscapeHeight + 15) / 16 / 2 )))
		}
		if (!howMuchLive) finalVencThreadsOrGpuInfo = Math.max(1, Math.min(finalVencThreadsOrGpuInfo, Math.round((landscapeHeight + 15) / 16 / 2 )))
	}
	if (typeof finalVencThreadsOrGpuInfo === 'number') finalVencThreadsOrGpuInfo = Math.min(16, finalVencThreadsOrGpuInfo) // hack, fix me
	if (howMuchLive)
	{
		//finalVdec = 'sw'//aoei
	}
	//finalVenc = 'sw'//aoei
	if (landscapeHeight === 360)
	{
		console.log('finalVenc = %O', finalVenc)
		console.log('finalVdec = %O', finalVdec)
	}
	if (typeof finalVenc === 'undefined' || typeof finalVdec === 'undefined')
	{
		throw 'no anything'
	}

	const force_key_framesIsBugged = finalVenc === 'qsv'// && origLandscapeWidth / origLandscapeHeight === 16/9 && ['h264', 'hevc', 'vp9'].includes(origVideoCodec)
	//console.log('%O, %O, %O, origW=%O / origH=%O', XXXuseIntel, origW / origH === 16/9, possibleOutputHeightsForNow.includes(h), origW, origH)
	//const useVaapiInputParams = XXXuseIntel && !useIntelInputParams && !useIntelOutputParams && ['h264', 'hevc', 'vp9'].includes(origVideoCodec) && !whichYUV.includes('p10')
	//const useVaapiOutputParams = false && XXXuseIntel && !useIntelOutputParams && 'vp8' === origVideoCodec
	//const useVaapiOutputParams = !useIntelOutputParams && XXXuseIntel
	// intel может декодить vp8, но не инкодить vp8.
	// перед -i
	const origVideoCodecShort = origVideoCodec.replace(/(vp9|hevc)(_\d+)/, '$1')
	const beforeInputParams = (
		//useVaapiOutputParams
		/*!useIntelInputParams && !useIntelOutputParams && */finalVdec === 'vaapi'
		? ` -hwaccel vaapi -hwaccel_output_format vaapi `
		: (
			finalVdec === 'nv'
			? ` -hwaccel cuvid -hwaccel_output_format cuda -c:v ${origVideoCodecShort}_cuvid `
			: (
				(
					finalVdec === 'qsv'
					? ` -hwaccel qsv -c:v ${origVideoCodecShort}_qsv `
					: ''
				)
				+
				(
					finalVenc === 'qsv'
					? ` -init_hw_device qsv=qsv:MFX_IMPL_hw_any `
					: ``
				)
			)
		)
	) + (
		``
		// fixme: если sdp, то -framerate не знает
		//hasInputClearEnd  ? `` : ` -framerate ${origFps} `
	)

	// -af aresample=resampler=soxr:precision=28


	//const fpsFix = newFPS !== origFps ? `-r:v${III} ` + newFPS : ''
	// ^ закомментил, ибо дало 19001/317 (59.94006) для 59.94 FPS, входное видео = https://www.youtube.com/watch?v=LXb3EKWsInQ
	const fpsFix = /*!useIntel||*/1 ? `-r:v${III} ` + v.fps.join('/') : ``
	//const fpsFix = ''
	//console.log('getVideoParams(h=%O, origFps=%O, newFPSForHighResolutions=%O) gave fpsFix=%O', h, origFps, newFPSForHighResolutions, newFPS)


	// https://support.google.com/youtube/answer/2853702?hl=en
	/*const level: string = ` -level${III} ` + (
		(finalH === 2160 && newFPS > 30)
		? '5.2 -refs 5'
		: (
			(
				(finalH === 2160 && newFPS <= 30)
				||
				(finalH === 1440 && newFPS > 30)
			)
			? '5.1 -refs 5'
			: (
				finalH === 1440 && newFPS <= 30
				? '5.0 -refs 5'
				: (
					newFPS > 30 && finalH >= 1080// ибо если 30<fps<40, то fps до снижается 30, а если 40<=fps<50, то до 50 дотягивает, см. выше
					? '4.2'
					: (
						finalH <= 486
						? (
							finalH >= 406 && newFPS > 30
							? '3.2'
							: '3.1'
						)
						: '4.1'
					)
				)
			)
		)
	)*/
	const levelNumber = utils.getH264LevelNumber(landscapeWidth, landscapeHeight, v.fps)
	const level: string = ` -level${III} ${levelNumber.toString()[0]}.${levelNumber.toString()[1]}`
	// youtube 144p @ level 1.2 (Main@L1.2)
	// youtube 240p @ level 2.1 (Main@L2.1)
	// youtube 360p @ level 3.0 (Main@L3) level=30
	// youtube 480p @ level 3.1 (Main@L3.1)
	// youtube 480p,720p@30 @ level 3.1 (Main@L3.1)
	// youtube 720p@60 @ level 3.2 (Main@L3.2)



	//const profileV = 'high'//whichYUV ? '-profile:'
	//const baselineUpto: PossibleHeight = 0//useNvidia ? 0 : 234
	// при nvenc + baseline каждые 2 секунды мыло на 0.3 секунды.
	//const mainUpto: PossibleHeight = 486
	//const profileVValue = (h <= baselineUpto ? 'baseline' : (h <= mainUpto ? 'main' : 'high'))
	const profileVValue = commons.getH264Profile(levelNumber, landscapeWidth)
	const profileV = '-profile:v' + III + ' ' + profileVValue

	const pcSpecific = finalVenc === 'nv'
	//-i_qfactor:v${III} 2 -b_qfactor:v${III} 1.1
	? `-threads:v${III} 4 -c:v${III} h264_nvenc ${profileVValue === 'baseline' ? '' : `-coder:v${III} cabac`} ${howMuchLive ? '-zerolatency 1' : `-b_ref_mode:v${III} middle`}`// -rc vbr'
	: (
		finalVenc === 'qsv'
		? (
			`-threads:v${III} 4 -c:v${III} h264_qsv -rdo 1 -pic_timing_sei 1 -recovery_point_sei 1 -aud 1 -bitrate_limit 1 -avbr_accuracy 10 -avbr_convergence 150`
			
		)
		: (
			finalVenc === 'sw'
			?
			/*useVaapiOutputParams
			? `-threads:v${III} 4 -c:v${III} h264_vaapi`
			: */`-threads:v${III} ${finalVencThreadsOrGpuInfo} -c:v${III} libx264`
			: ``
		)
	)
	

	//const keyFrameEveryFrames: number = TestSegmentsModeDebug ? lowerFps(newFPSForHighResolutions) : /*engine.toPrecision(*/newFPS * keyFrameEverySeconds/*, 6)*/
	const keyFrameEveryFrames: number = Math.ceil(engine.fixNumber(vFpsAsNumber * keyFrameEverySeconds))
	//const keyFrameEveryFrames: number = TestSegmentsModeDebug ? lowerFps(newFPSForHighResolutions) : engine.toPrecision(newFPS * keyFrameEverySeconds, 6)
	// ^ lowerFps because of 240p
	//const keyFrameEveryFrames: number = /*Math.round*/(newFPS * keyFrameEverySeconds)
	console.log('keyFrameEveryFrames=%O * keyFrameEverySeconds=%O = %O', vFpsAsNumber, keyFrameEverySeconds, keyFrameEveryFrames)
	const lookAheadFrames = /*(newFPS*1.6|0)||*/Math.min(
		keyFrameEveryFrames - 1,
		Math.max(
			finalVenc === 'qsv' ? 100 : 0,
			landscapeHeight < 250 ? 64 : 32
		)
	)
	console.log('lookAheadFrames=%O', lookAheadFrames)
	const swsFlagsValue = 'bicubic'//howMuchLive ? 'bicubic' : `print_info+lanczos+accurate_rnd+full_chroma_inp`


	let vf = ` -filter:v${III} `
	{
		/*
		https://developer.nvidia.com/blog/nvidia-ffmpeg-transcoding-guide/
		Sometimes it might be necessary to mix CPU and GPU processing.
		For example you may need to decode on the CPU,
		because the format is unsupported on the GPU decoder,
		or because a filter is not available on the GPU.
		In those cases, you can’t use the -hwaccel cuvid flag.
		Instead, you need to manage uploading
		the data from system to GPU memory
		using the hwupload_cuda filter.
		In the example below,
		an H.264 stream is decoded on the GPU and
		downloaded to system memory since -hwaccel cuvid is not set.
		The fade filter is applied in system memory and
		the processed image uploaded to GPU memory using the
		hwupload_cuda filter.
		Finally, the image is scaled
		using scale_npp and encoded on the GPU.
		*/
		let beforeCudaParamsBothCudaAndSw: Array<string> = []
		const cudaParams: Array<string> = []
		const afterCudaParamsSwOnly: Array<string> = []
		//const afterCudaParamsCudaOnlyButBeforeTheCudaAndSwOnly: Array<string> = []
		const afterCudaParamsBothCudaAndSw: Array<string> = []

		// https://trac.ffmpeg.org/ticket/7753#comment:11
		// On the other hand, cuvid provides access to nvidia's own deinterlacer and you don't need yadif_cuda.
		//+ (isInterlaced ? 'yadif_cuda,' : '')
		// cuvid в nvidiaVideoInputOptions
		//if (!useIntelInputParams && !useVaapiInputParams && useIntelOutputParams)
		if (finalVdec === 'sw' && finalVenc === 'qsv')// FIXME finalVdec чтобы nvidia декодила, а qsv инкодил
		{
			beforeCudaParamsBothCudaAndSw.push(`hwupload=extra_hw_frames=${/*lookAheadFrames*/Math.min(60, origFpsAsNumber)},format=qsv`)
		}
		/*else if (
			finalVdec === 'qsv'
			// && !useIntelOutputParams
			&& (
				finalVenc !== 'qsv'
				||
				//h !== origH
				//||
				origW / origH !== 16/9
			)
		)
		{
			afterCudaParamsBothCudaAndSw.push(`hwdownload,format=nv12`)
		}*/
		const isWebRTC = pr === Protocol.SDP
		if (isWebRTC || v.h !== origLandscapeH || origLandscapeW / origLandscapeH !== 16/9)
		{
			if (finalVenc === 'nv')
			{
				cudaParams.push(
					`scale_cuda=${v.w}:${v.h}`
					+ (
						supports$scale_cuda__interp_algo__lanczos
						? ':interp_algo=lanczos'
						: ''
					)
				)
			}
			else if (origLandscapeW / origLandscapeH === 16/9 && (finalVdec === 'vaapi' || finalVenc === 'qsv'))
			{
				if (finalVdec === 'vaapi')
				{
					if (origFpsAsNumber !== vFpsAsNumber)
					{
						afterCudaParamsBothCudaAndSw
						.push(`fps=${v.fps.join('/')}`)
					}
					afterCudaParamsBothCudaAndSw
					.push(`scale_vaapi=w=${v.w}:h=${v.h}`)
					if (finalVenc === 'qsv')
					{
						afterCudaParamsBothCudaAndSw
						.push(`hwmap=derive_device=qsv,format=qsv`)
					}
					else if (finalVenc === 'vaapi')
					{
						afterCudaParamsBothCudaAndSw
					}
				}
				else if (finalVenc === 'qsv')
				{
					const vpp_qsv: Array<string> = []
					if (isInterlaced) vpp_qsv.push(`deinterlace=2`)
					//if (origFps !== newFPS) vpp_qsv.push(`framerate=${newFPS}`)
					//vpp_qsv.push(`w=${v.w}:h=${v.h}`)
					afterCudaParamsBothCudaAndSw
					.push(`vpp_qsv=${vpp_qsv.join(':')}`)
					afterCudaParamsBothCudaAndSw
					.push(`scale_qsv=${v.w}:${v.h}`)
				}
			}
			else
			{
					if (/*!XXXuseNvidia && !useIntelOutputParams && */isInterlaced)
					{
						afterCudaParamsSwOnly.push('yadif=1')//todo: *_qsv умеет ли
					}
				//}
				// swapping scaleLanczos and unsharp makes it like it was just scaleBilinear
				// zscale spline36 = 11.656s
				// zscale lanczos = 14.729s
				// swscale lanczos = 6.527s
				// swscale = 4.996s
				// source for unsharp: https://forum.videohelp.com/threads/381911-ffmpeg-scaling-video-resolution
				const flags = `:flags=${swsFlagsValue}`
				//const flags = ':flags=lanczos'
				let scaleQsvDone = false
				if (finalVdec !== 'sw')
				{
					if ((process.env.scq || (0&&isWebRTC)) && finalVdec === 'qsv' && finalVenc !== 'qsv')
					{
						scaleQsvDone = true
						afterCudaParamsBothCudaAndSw
						.push(`scale_qsv=${v.w}:${v.h}`)
					}
					afterCudaParamsBothCudaAndSw.push('hwdownload,format=nv12')
				}
				else if (finalVdec === 'sw' && finalVenc === 'qsv')
				{
					beforeCudaParamsBothCudaAndSw = beforeCudaParamsBothCudaAndSw.filter(
						x => x !== `hwupload=extra_hw_frames=${/*lookAheadFrames*/Math.min(60, origFpsAsNumber)},format=qsv`
					)
				}
				if (scaleQsvDone) {}
				else if (isWebRTC && (origLandscapeW / origLandscapeH === 16/9))
				{
					afterCudaParamsBothCudaAndSw
					.push(`scale=w=${v.w}:h=${v.h}${flags}`)
				}
				else
				{
					/*if (finalVenc !== 'sw')
					{
						afterCudaParamsBothCudaAndSw.push('hwdownload')
					}*/
					afterCudaParamsBothCudaAndSw
					.push(`scale=(iw*sar)*min(${v.w}/(iw*sar)\\,${v.h}/ih):ih*min(${v.w}/(iw*sar)\\,${v.h}/ih)${flags}`)
				}
				if (!howMuchLive && v.h !== origLandscapeH)
				{
					afterCudaParamsBothCudaAndSw
					.push(`unsharp=luma_msize_x=3:luma_msize_y=3:luma_amount=0.8`)
				}
				if (finalVenc !== 'sw')
				{
					if (finalVenc === 'qsv')
					{
						afterCudaParamsBothCudaAndSw.push(`hwupload=extra_hw_frames=${/*lookAheadFrames*/Math.min(60, origFpsAsNumber)},format=qsv`)
					}
					else
					{
						afterCudaParamsBothCudaAndSw.push('hwupload,format=nv12')
					}
				}
			}
			if (
				//!possibleOutputLandscapeHeightsForNow.includes(landscapeHeight as any)
				//||
				origLandscapeW / origLandscapeH !== 16/9
			)
			{
				afterCudaParamsBothCudaAndSw
				.push(`pad=${v.w}:${v.h}:(${v.w}-iw)/2:(${v.h}-ih)/2`)
			}
			if (false && origFpsAsNumber < vFpsAsNumber/* && !howMuchLive*/)
			{
				afterCudaParamsBothCudaAndSw.push(
					`minterpolate=fps=${v.fps.join('/')}:mi_mode=blend`
				)
			}
			if (finalVenc !== 'qsv' && finalVenc !== 'vaapi')
			{
				//if (pixelformat) afterCudaParamsBothCudaAndSw/*afterCudaParamsCudaOnlyButBeforeTheCudaAndSwOnly*/.push(pixelformat)
				if (finalVdec !== 'nv') afterCudaParamsBothCudaAndSw.push('setsar=1/1')
				if (finalVdec !== 'nv') afterCudaParamsBothCudaAndSw.push('format=nv12')// FIXME:or if (finalVenc !== 'nv')
			}
			else if (finalVdec === 'qsv' && finalVenc === 'qsv' && origLandscapeW / origLandscapeH !== 16/9)
			{
				afterCudaParamsBothCudaAndSw.push(`setsar=1/1,hwupload=extra_hw_frames=${/*lookAheadFrames*/Math.round(Math.min(60, origFpsAsNumber))},format=qsv`)
			}
			//if (pixelformat) afterCudaParamsBothCudaAndSw/*afterCudaParamsCudaOnlyButBeforeTheCudaAndSwOnly*/.push(pixelformat)
			0&&afterCudaParamsBothCudaAndSw.push('setsar=1/1')
			//afterCudaParamsBothCudaAndSw.push(`hwupload=extra_hw_frames=${lookAheadFrames},format=qsv`)
		}
		else if (finalVenc === 'vaapi')
		{
			if (finalVdec === 'vaapi')
			{
				afterCudaParamsBothCudaAndSw
				.push(`format=nv12,hwdownload`)
			}
			afterCudaParamsBothCudaAndSw
			.push(`scale=(iw*sar)*min(${v.w}/(iw*sar)\\,${v.h}/ih):ih*min(${v.w}/(iw*sar)\\,${v.h}/ih):flags=print_info+lanczos+accurate_rnd+full_chroma_inp`)
			if (v.h !== origLandscapeH)
			{
				afterCudaParamsBothCudaAndSw
				.push(`unsharp=luma_msize_x=3:luma_msize_y=3:luma_amount=0.8`)
			}
			afterCudaParamsBothCudaAndSw
			.push(`pad=${v.w}:${v.h}:(${v.w}-iw)/2:(${v.h}-ih)/2`)
			//if (pixelformat) afterCudaParamsBothCudaAndSw/*afterCudaParamsCudaOnlyButBeforeTheCudaAndSwOnly*/.push('pixelformat')			
			afterCudaParamsBothCudaAndSw.push('setsar=1/1')
			afterCudaParamsBothCudaAndSw.push('format=nv12|vaapi')
			afterCudaParamsBothCudaAndSw
			.push(`hwupload`)
			
		}
		else if (finalVenc !== 'qsv' && finalVenc !== 'vaapi' && finalVenc !== 'nv'/*так-то useNvidiaOutputParams*/)
		//TODO: or finalVdec !== 'nv' ???
		{
			//if (pixelformat) afterCudaParamsBothCudaAndSw/*afterCudaParamsCudaOnlyButBeforeTheCudaAndSwOnly*/.push(pixelformat)
			if (finalVdec !== 'sw')
			{
				if (!afterCudaParamsBothCudaAndSw.join('').includes('hwdownload'))
				{
					afterCudaParamsBothCudaAndSw.push('hwdownload')
				}
			}
			afterCudaParamsBothCudaAndSw.push('setsar=1/1,format=nv12')
		}

		let a: Array<string> = []
		if (cudaParams.length)// || afterCudaParamsBothCudaAndSw/*todo, только первая проверка должна быть, ибо inputParams*/)
		{
			/*if (beforeCudaParamsBothCudaAndSw.length) 
			{
				a.push('hwdownload_cuda')
				a.push(...beforeCudaParamsBothCudaAndSw)
				a.push('hwupload_cuda')
			}*/
			a.push(...cudaParams)
			if (
				//afterCudaParamsCudaOnlyButBeforeTheCudaAndSwOnly.length
				//||
				afterCudaParamsBothCudaAndSw.length
			)
			{
				//a.push('hwdownload_cuda')
				/*if (afterCudaParamsCudaOnlyButBeforeTheCudaAndSwOnly.length) 
				{
					//a.push('hwupload_cuda')
					a.push(...afterCudaParamsCudaOnlyButBeforeTheCudaAndSwOnly)
				}*/
				if (afterCudaParamsBothCudaAndSw.length)
				{
					a.push(...afterCudaParamsBothCudaAndSw)
				}
				if (finalVdec !== 'nv')
				{
					if (finalVdec !== 'sw') throw 'not implemented'
					a.push('hwupload_cuda')
				}
			}
			
		}
		else
		{
			a = [
				...beforeCudaParamsBothCudaAndSw,
				...afterCudaParamsSwOnly,
				...afterCudaParamsBothCudaAndSw,
			]
		}
		if (!a.filter(x=>x).length) vf = ''
		else vf += a.filter(x=>x).join(',')
	}





	// эти две переменные из https://github.com/pluginfactory/HLS-transcoding-nodejs/blob/master/create-hls-vod.sh
	// они две же как бы тут https://docs.peer5.com/guides/production-ready-hls-vod/
	//const max_bitrate_ratio         = v.bitrateInBits * 1.07 | 0//maxrate
	//const rate_monitor_buffer_ratio = v.bitrateInBits * 1.5 | 0// bufsize

	// time-tested
	const max_bitrate_ratio         = v.bitrateInBits | 0//maxrate
	const rate_monitor_buffer_ratio = v.bitrateInBits | 0// bufsize

	//const max_bitrate_ratio         = finalVenc === 'sw' ? (v.bitrateInBits * 1.07 | 0) : v.bitrateInBits | 0//maxrate
	//const rate_monitor_buffer_ratio = finalVenc === 'sw' ? (v.bitrateInBits * 1.5 | 0 ) : v.bitrateInBits * 0.35 | 0// bufsize

	//0.30 => 675..791
	//0.35 => 665..808.5
	const maxrate = `-maxrate:v${III} ${max_bitrate_ratio} -minrate:v${III} ${max_bitrate_ratio}`
	const bufsize = `-bufsize:v${III} ${rate_monitor_buffer_ratio} ` + (finalVenc === 'qsv' ? ` -mbbrc:v${III} 1` : ``)

	const bFrames = /^\d+$/.test(process.env.BF)
	? Math.min(+process.env.BF, lookAheadFrames)
	: (howMuchLive ? 0 : (
		finalVenc === 'nv'
		? Math.min(4, Math.min(lookAheadFrames, 12))
		: Math.min(lookAheadFrames, 12)
	))


	// nvidia: For high throughput, low latency performance, ensure you're using either llhp or llhq presets

	if (finalVenc === 'sw' && typeof finalVencThreadsOrGpuInfo !== 'number')
	{
		console.error('arzetBUG 872')
		finalVencThreadsOrGpuInfo = howMuchLive ? 5 : 2
	}
	//finalVencThreadsOrGpuInfo = 1 if test multi threaded output 

	if (typeof finalVencThreadsOrGpuInfo === 'number')
	{
		finalVencThreadsOrGpuInfo = Math.min(CPU_THREADS, finalVencThreadsOrGpuInfo)
		finalVencThreadsOrGpuInfo = 16
	}

	const ret = {
		//v.bitrateInBits,
		//pixelformat,
		beforeInputParams,
		finalVenc: [finalVencThreadsOrGpuInfo, finalVenc],
		finalVdec: [finalVdecThreadsOrGpuInfo, finalVdec],
		vf,
		videoParams: [
			'\n',
			...(
				pr !== Protocol.FILE && pk === 'ff'
				// http://ffmpeg.org/pipermail/ffmpeg-user/2019-February/043179.html
				? [``]//[`-muxdelay:v${III} 2.25 -muxpreload:v${III} 2.25`]// -muxpreload 1.0 // -muxpreload 1.5
				: [``]
			),
			` -sws_flags ${swsFlagsValue}`,// -sws_flags is a global flag, therefore -sws_flags:v is an invalid flag
			pcSpecific,
			...(
				force_key_framesIsBugged
				? []
				//: [` -force_key_frames:v${III} expr:n_forced*${keyFrameEverySeconds}`]
				//: [` -force_key_frames:v${III} expr:gte(t,n_forced*${keyFrameEverySeconds})`]
				//: [` -force_key_frames:v${III} expr:n_forced*${keyFrameEverySeconds}`]
				//: [` -force_key_frames:v${III} expr:${keyFrameEverySeconds}`]
				: [` -force_key_frames:v${III} expr:if(isnan(prev_forced_n),1,eq(n,prev_forced_n+${keyFrameEverySeconds}))`] // https://marcinchmiel.com/articles/2020-10/why-you-should-force-fixed-closed-gops-and-how-to-do-it-in-ffmpeg/
				
			),
			//`-avoid_negative_ts disabled `,// -async_depth 1`,
			(hasInputClearEnd
			? (
				finalVenc === 'qsv'
				? ``//`-look_ahead:v${III} 1 -look_ahead_depth:v${III} ${lookAheadFrames}`
				: (
					`-rc-lookahead:v${III} ${lookAheadFrames}`
					+ (finalVenc === 'nv' ? ` -cbr 1 -rc:v${III} cbr -spatial_aq:v${III} 1 -temporal-aq:v${III} 1` : ``)
				)
			)
			: `${finalVenc === 'nv' ? ` -cbr 1 -rc:v${III} cbr` : ``}`),
			// https://stackoverflow.com/questions/58021105/ffmpeg-encoded-hls-makes-audio-and-video-progressively-out-of-sync
			`${bFrames > 1 && !process.env.FORCE_PYRAMID ? `-b-pyramid:v${III} none` : ``} -bf:v${III} ${bFrames}`,
			// ^ баг при bFrames > 1: первый аудио сегмент короче
			// -b_strategy:v${III} ${finalVenc === 'qsv' ? 1 : 2}
			//-b-pyramid:v${III} strict
			level,
			fpsFix,
			vf,
			(
				finalVenc === 'nv'
				? ` -strict_gop:v${III} 1 -no-scenecut:v${III} 1`// -forced-idr 1`
				: ` -mpv_flags:v${III} +strict_gop`
			),
			`-preset:v${III} ${
				process.env.preset
				||
				(
					howMuchLive === 0 && finalVenc === 'sw'
					? (process.argv.join('').includes('tc-cli') ? 'veryslow' : 'faster')
					: (
						finalVenc === 'nv'
						? (
							landscapeHeight < 250
							? (howMuchLive >= 3 ? 'llhq' : (isFfmpegMin44 ? 'p7' : 'slow'))
							: (howMuchLive >= 3 ? 'llhp' : (howMuchLive >= 2 ? 'll' : 'medium'))
						)
						: (
							howMuchLive <= 1
							? 'medium'
							: (
								howMuchLive === 2
								? 'faster'
								// ==== 3
								: (
									(howMuchLive === 3 || finalVenc === 'qsv') && pr !== Protocol.RTMP
									? 'veryfast'
									: (howMuchLive === 4 ? 'ultrafast' : 'superfast')
								)
							)
						)
					)
				)
			}`,
			profileV,
			`-b:v${III} ${v.bitrateInBits}`,
			maxrate,
			bufsize,
			         `-g:v${III} ${Math.ceil(keyFrameEveryFrames)}`,
			//16 frames @ 30 fps === 0.533333s, который делится на 1 фрейм aac (1s/48000hz*1024framesPerPacket=0.02133333s), но не 1 фрейм opus (в основном 20ms, реже 2.5, 5, 10, 40, 60; еще 80, 100, 120, если #ifdef OPUS_FRAMESIZE_120_MS)
			//8 frames @ 25 fps === 0.32s, который делится на 1 фрейм aac (1s/48000hz*1024framesPerPacket=0.02133333s) И 1 фрейм opus (в основном 20ms и 10ms, реже 2.5, 5, 10, 40, 60; еще 80, 100, 120, если #ifdef OPUS_FRAMESIZE_120_MS)
			//у AAC audioframes per packet size всегда 1024 (960 почти никто не умеет),
			// а у OPUS может быть 480, 960, 1920, 2880; #ifdef OPUS_FRAMESIZE_120_MS 3840, 4800, 5760
			// 1/48000*960=0.02
			// 1/48000*480=0.01
			// 1/48000*2880=0.06
			// 1/48000*3840=0.08
			// 1/48000*4800=0.1
			// 1/48000*5760=0.12
			// если opus audioframes per packet size=2880или5760, то надо -keyint_min 0.096s
			// если opus audioframes per packet size=3840, то 0.032s
			// если opus audioframes per packet size=4800, то забить на -keyint_min
			`-keyint_min:v${III} ${engine.roundToEven(keyFrameEveryFrames)}`,
			(
				finalVenc === 'sw'
				? (
					howMuchLive
					/////////теперь разница между veryfast  и faster только в rc
					/////////rc-lookahead+ trellis=1 = 0.98x
					? ` -x264opts:v${III} nal-hrd=cbr:no-scenecut:force-cfr=1:threads=${finalVencThreadsOrGpuInfo}${howMuchLive && landscapeHeight > 360 ? '' : ''}`
					: ` -x264opts:v${III} nal-hrd=cbr:no-scenecut:force-cfr=1:threads=${finalVencThreadsOrGpuInfo}${1?'':`:vbv-bufsize=${Math.ceil(v.bitrateInBits / vFpsAsNumber / 1000)}`}`
				)
				// nal-hrd=cbr:
				: ``
			),
			...(
				finalVenc === 'nv'
				? (
					howMuchLive >= 3
					? [`-tune:v${III} ull`]
					: (
						howMuchLive >= 2
						? [`-tune:v${III} ll`]
						: []
					)
				)
				: (
					finalVenc === 'sw'
					? [
						howMuchLive
						? `-tune:v${III} zerolatency`
						: `-tune:v${III} film`
					]
					: []
				)
			),
		].filter(x=>x).join(' ')
	}
	return ret
}

const convertVideoToPng = function convertVideoToPng (
	inputFile: string,
	outputFile: string,
	opts: Partial<{
		seek: number//ms
		w: number
		h: number
		nice?: number|null|void
		killMethods?: ReturnType<typeof AbortController2>
	}> = {}
)//: Promise<string>
{
	if (!opts) opts = {}
	else
	{
		// @ts-ignore
		if (opts.w > 0)
		{
			opts.w = Math.round(opts.w!)
			if (opts.w % 2 === 1) opts.w -= 1
		}
		// @ts-ignore
		if (opts.h > 0)
		{
			opts.h = Math.round(opts.h!)
			if (opts.h % 2 === 1) opts.h -= 1
		}
	}
	const seek = Math.floor(opts.seek || 0)
	
	const rescaleOpts = !opts.w && !opts.h
	? ''
	: (`-vf scale=w=${opts.w}:h=${opts.h}:`
		+ `${
			/*opts.w === -1
			||
			opts.h === -1
			||
			(opts.w > 0 && opts.w % 2 !== 0)
			||
			(opts.h > 0 && opts.h % 2 !== 0) 
			? 'force_original_aspect_ratio=decrease:force_divisible_by=2'
			: */'force_original_aspect_ratio=2'
		}`
	+ `,crop=${opts.w}:${opts.h},format=pix_fmts=yuv444p`
	)
	//00:00:01.5
	// -deinterlace 
	if (typeof opts.nice === 'number')
	{
		if (Number.isNaN(opts.nice)) opts.nice = void 0
		else
		{
			opts.nice = Math.min(opts.nice, 19)
			opts.nice = Math.max(opts.nice, -20)
		}
	}
	return spawnAsyncVoid(
		typeof opts.nice === 'number' ? 'nice' : FFMPEG_PATH,
		(typeof opts.nice === 'number' ? `-n ${opts.nice} ${FFMPEG_PATH} ` : ``)
		+
`-y -i ${escapeSpawnArg(inputFile)} \
-ss ${seek}ms \
-level 6.2 \
-preset veryslow \
-crf 0 \
-pix_fmt yuv444p \
-an \
-qmin 1 \
-qscale:v 1 \
-vframes 1 \
${rescaleOpts} \
${escapeSpawnArg(outputFile)}`, {log: true, logCmd: true, killMethods: opts.killMethods,}
	)
	// .then(() => {
	// 	return outputFile
	// })
}
const convertVideoToWebp = async function convertVideoToWebp (
	inputFile: string,
	outputWebpFile: string,
	opts: Partial<{
		seek: number//ms
		w: number
		h: number
		m: 0|1|2|3|4|5|6
		nice?: number|null|void
		killMethods?: ReturnType<typeof AbortController2>
	}> = {}
): ReturnType<typeof convertVideoToPng> {
	const tempPngFile = outputWebpFile + '.png'
	await convertVideoToPng(inputFile, tempPngFile, opts)
	console.log('convertVideoToPng(%O, %O) done, ls=', inputFile, tempPngFile)
	//console.log(await shellSpawnAsyncUTF8('ls -lA /tmp/up/'))
	const ret = await commons.convertImageToWebp(tempPngFile, outputWebpFile, 100, opts.m, opts.nice, opts.killMethods)
	console.log('commons.convertImageToWebp(%O, %O) done, ls=', tempPngFile, outputWebpFile)
	//console.log(await shellSpawnAsyncUTF8('ls -lA /tmp/up/'))
	await safeUnlinkAsync(tempPngFile)
	return ret
	/*return convertVideoToPng(inputFile, outputFile + '.png', opts)
	.then(() => {
		return commons.convertImageToWebp(outputFile + '.png', outputFile, 100).then((ret) => {
			fsAsync.unlink(outputFile + '.png')
			return Promise.resolve(ret)
		})
	})*/
}

// у cover ключ по-другому, разница только в типизации
// сначала string являются filePath, но в конце становятся хэшами+расширение
type pppp = Omit<Omit<Omit<Video, 'cover'>, 'subtitles'>, 'id'> & {
	cover: string/*{//Map<
		//[number, number],
		[WxH: string]:
		{
			0?: {webp: string},
			1?: {jpg: string, webp: string},
			2?: {jpg: string, webp: string},
		}
	}*/
}
type WhichFormatsAlreadyDone = Array<['h264', number]|['aac','high']|['opus','high']>



export default async function processMedia ({
	inputFile,
	outputDir: outputRootDir,
	partNumber,
	howMuchLive,
	//forceInputFps,
	alsoSingleFile,
	alsoCover,
	deleteOnFail,
	enableVideo,
	enableAudio,
	venc,
	vdec,
	jsonFail,
	onFfprobe,
	requestKeyFrame,
	onPreprestarted,
	onPrestarted,
	onStarted,
	_onTranscodeProgress,
	inputWidth,
	inputHeight,
	inputFramerate,
	inputHasAudio,

	hacks = {},
	outputAllowUpscaleButOnlyIfLandscapeHeightIsLessThanMaxLandscapeHeightInTimes,
	forceOutputs,
	//forceOutputsWhileShootingInTheLeg,
	outputOrientation,

	outputMinActualWidth,
	outputMinActualHeight,
	outputMinLandscapeWidth,
	outputMinLandscapeHeight,
	outputMinFramerate,

	outputMaxActualWidth,
	outputMaxActualHeight,
	outputMaxLandscapeWidth,
	outputMaxLandscapeHeight,
	outputMaxFramerate,

	outputAudioMaxBitrate,

	forceSegDuration,
}: {
	//inputFileDir: string,
	//inputFileName: string,
	inputFile: string,
	outputDir: string,
	partNumber?: void|number,
	howMuchLive: HowMuchLive,//0 = not live, 1 = "right now", otherwise ms
	//forceInputFps?: Urational,
	alsoSingleFile: boolean,
	alsoCover: boolean,
	deleteOnFail?: boolean
	enableVideo?: boolean,
	enableAudio?: boolean,
	venc: 'auto'|GpuVariants,
	vdec: 'auto'|GpuVariants,
	jsonFail: {(message: string, params?: Object, errorCode?: number): void|Promise<void>},
	onFfprobe?: {(/*{
		metaVersio[n, 0, 0],
		inputs,
		outputAudioMaxBitrate,
		outputMaxFps,
		outputMaxWidth,
		outputMaxHeight,
		outputs,
	}*/meta: MediaPremeta): void}
	requestKeyFrame?: {(type?: 'v'|'a'): Promise<boolean>}
	onPreprestarted?: {(): Promise<void>},
	onPrestarted?: {(): Promise<void>},
	onStarted?: {
		(liveStartedOn: DateNumber): Promise<void>
	},
	_onTranscodeProgress?: {(
		type: MediaType,
		//format: 'h264'|'av1'|'aac'|'opus'|'webp',
		//videoHeightOrAudioQualityOrNothingIfCover: number|'high'|null,
		//filePath: string,
		//fileSize: number,
		howMuchLocalTcPercentShouldBe: number,
		whenFinishedHowMuchTcPercentWasAdded: number,
	): Promise<void>},
	/* вообще все параметры (хром): {
	aspectRatio: 1.7777777777777777
	deviceId: "aed58f93c46dfd2e70657c7942ad764e44f0d0cdd67ce145a5d6a8b8be8a9335"
	frameRate: 60
	groupId: "07e2144faf589374bef084d8bb42f1cfe68775927c329fe2c2d98fe0a5632cc4"
	height: 486
	resizeMode: "none"
	whiteBalanceMode: "none"
	width: 864
	}
	*/
	inputWidth?: number,
	inputHeight?: number,
	inputFramerate?: number|Urational,
	inputHasAudio?: boolean

	hacks?: {
		allowHeAacForMoreThan64kbps?: true
		allowSegmentDurationNotBeExact?: 'just a little'
		dontFailIfFfmpegHasLessCapabilitiesButAdaptiveMediaMetaRequiresMore?: true // todo
	},
	outputAllowUpscaleButOnlyIfLandscapeHeightIsLessThanMaxLandscapeHeightInTimes?: number
	forceOutputs?: {
		v?: Array<Output_v>|undefined|{(kaka: any, prefinalOutputsV: Array<Output_v>): Array<Output_v>|undefined}
		a?: Array<Output_a>|undefined|{(kaka: any, prefinalOutputsV: Array<Output_v>, prefinalOutputsA: Array<Output_a>): Array<Output_a>|undefined}
	},
	dontResizeIfDiffIsLessThan?: ['px'|'%', u32|Floto] // todo
	//forceOutputsWhileShootingInTheLeg?: boolean,
	outputOrientation?: Orientation,
	//outputRotate?: 0|90|180|270,

	outputMinActualWidth?: u32,
	outputMinActualHeight?: u32,
	outputMinLandscapeWidth?: u32,
	outputMinLandscapeHeight?: u32,
	outputMinFramerate?: Urational|number,

	outputMaxActualWidth?: u32,
	outputMaxActualHeight?: u32,
	outputMaxLandscapeWidth?: u32,
	outputMaxLandscapeHeight?: u32,
	outputMaxFramerate?: Urational|number,

	outputAudioMaxBitrate?: number,

	forceSegDuration?: number|string,
	/*uploadFile: {(
		type: 'c'|'v'|'a',
		format: 'h264'|'aac'|'opus'|'webp',
		videoHeightOrAudioQualityOrNothingIfCover: number|'high'|null,
		filePath: string,
		callback: {(): void}
	): Promise<void>}// или boolean, вдруг ошибка?
	*/
}): Promise</*Array<string>|*/void|{
	//promises: [tcJob, doImageJob],
	promise: Promise<void>,
	premeta: MediaPremeta,
	sigint: {(): void}
}>
{
	const startedToFormPremetaOn = Date.now()
	enableAudio = !process.env.NOAUDIO && enableAudio !== false
	enableVideo = !process.env.NOVIDEO && enableVideo !== false
	//const WWenc: Array<GraphicsControllerDataExtended|number> = []
	//const WWdec: Array<GraphicsControllerDataExtended|number> = []
	if (!venc) venc = 'auto'
	if (!vdec) vdec = 'auto'
	if (venc === 'auto')
	{
		venc = []
		if (gpuInfo.filter(x=>x.enc.nv.length).length)  venc.push(['nv', 0])
		if (gpuInfo.filter(x=>x.enc.qsv.length).length) venc.push(['qsv', 0])
		venc.push(['sw', -1])
	}
	if (vdec === 'auto')
	{
		vdec = []
		if (gpuInfo.filter(x=>x.dec.nv.length).length)  vdec.push(['nv', 0])
		if (gpuInfo.filter(x=>x.dec.qsv.length).length) vdec.push(['qsv', 0])
		vdec.push(['sw', -1])
	}

	let inputFramerateAsNumber: Floto|undefined = (
		typeof inputFramerate === 'number' && engine.isSafePositiveFloat(inputFramerate)
		? inputFramerate
		: (
			Array.isArray(inputFramerate)
			? inputFramerate.$fromRational()
			: void 0
		)
	)
	if (typeof inputFramerate === 'number') inputFramerate = utils.numberToFractional(inputFramerate)
	if (typeof outputMinFramerate === 'number') outputMinFramerate = utils.numberToFractional(outputMinFramerate)
	else if (!outputMinFramerate) outputMinFramerate = [25, 1]//[24000, 1001]
	if (typeof outputMaxFramerate === 'number') outputMaxFramerate = utils.numberToFractional(outputMaxFramerate)
	else if (!outputMaxFramerate) outputMaxFramerate = [60, 1]
	outputMinFramerate = outputMinFramerate as Urational
	outputMaxFramerate = outputMaxFramerate as Urational
	if (typeof outputMinFramerate === 'number') throw new Error('ts')
	if (typeof outputMaxFramerate === 'number') throw new Error('ts')
	if (!outputMinFramerate) throw new Error('ts')
	if (!outputMaxFramerate) throw new Error('ts')
	if (!Array.isArray(outputMinFramerate)) throw new Error('ts')
	if (!Array.isArray(outputMaxFramerate)) throw new Error('ts')
	outputMinFramerate as Urational
	outputMaxFramerate as Urational
	console.log(outputMinFramerate)
	console.log(outputMaxFramerate)
	//if (outputMaxFramerate){}
	


	
	// enc:
	//if (venc === 'auto')
	//{
		//venc = gpuInfo.filter(x=>['nv', 'qsv'].includes(x.vendorShort))
		//if (!venc.length) venc = [['sw', -1]]
		//for (const gpu of gpuInfo)
		//{

		//}
		/*for (const gpu of gpuInfo)
		{
			if (gpu.vendorShort === 'nv' && (gpu.enc.nv.length || gpu.enc.vaapi.length))
			{
				WWenc.push(gpu)
			}
			else if (gpu.vendorShort === 'intel' && (gpu.enc.qsv.length || gpu.enc.vaapi.length))
			{
				WWenc.push(gpu)
			}
		}
		if (WWenc.length === 0) WWenc.push(-1)
	}
	else
	{
		for (const reqV of venc)
		{
			if (reqV[0] === 'sw') WWenc.push(reqV[1])
			else if (reqV[0] === 'nv')
			{
				const list = gpuInfo.filter(x => x.enc.nv.length)
				if (!list.length) throw 'no NVENC devices'
				//WWenc.push(list[0])
				//const p = list[reqV[1]]
				//if (p) WWenc.push(p)
				/*if (reqV[1] === 'rand') WWenc.push(list[0])
				else if (typeof reqV[1] === 'number')
				{
					if (reqV[1] >= 0)
					{
						const p = list[reqV[1]]
						if (p) WWenc.push(p)
						else throw 'not found ' + p
					}
				}*
			}
			else if (reqV[0] === 'qsv')
			{
				const list = gpuInfo.filter(x => x.enc.qsv.length)
				if (!list.length) throw 'no Intel QSV (QuickSync) devices [err1]'
				WWenc.push(list[0])
				/*if (reqV[1] === 'rand') WWenc.push(list[0])
				else if (typeof reqV[1] === 'number')
				{
					if (reqV[1] >= 0)
					{
						const p = list[reqV[1]]
						if (p) WWenc.push(p)
						else throw 'not found ' + p
					}
				}*
			}
		}
	}
	// dec:
	/*if (vdec === 'auto')
	{
		for (const gpu of gpuInfo)
		{
			if (gpu.vendorShort === 'nv' && (gpu.dec.nv.length || gpu.dec.vaapi.length))
			{
				WWdec.push(gpu)
			}
			if (gpu.vendorShort === 'intel' && (gpu.dec.qsv.length || gpu.dec.vaapi.length))
			{
				WWdec.push(gpu)
			}
		}
		if (WWdec.length === 0) WWdec.push(-1)
	}
	else
	{
		for (const reqV of vdec)
		{
			if (reqV[0] === 'sw') WWdec.push(reqV[1])
			else if (reqV[0] === 'nv')
			{
				const list = gpuInfo.filter(x => x.dec.nv.length)
				if (!list.length) throw 'no NVDEC devices'
				WWdec.push(list[0])
				/*if (reqV[1] === 'rand') WWdec.push(list[0])
				else if (typeof reqV[1] === 'number')
				{
					if (reqV[1] >= 0)
					{
						const p = list[reqV[1]]
						if (p) WWdec.push(p)
						else throw 'not found ' + p
					}
				}*
			}
			else if (reqV[0] === 'qsv')
			{
				const list = gpuInfo.filter(x => x.dec.qsv.length)
				if (!list.length) throw 'no Intel QSV (QuickSync) devices [err1]'
				WWdec.push(list[0])
				/*if (reqV[1] === 'rand') WWdec.push(list[0])
				else if (typeof reqV[1] === 'number')
				{
					if (reqV[1] >= 0)
					{
						const p = list[reqV[1]]
						if (p) WWdec.push(p)
						else throw 'not found ' + p
					}
				}*
			}
		}
	}*/
	/*if ((venc === 'nv' || vdec === 'nv') && !hasNvidiaGpu)
	{
		console.error('venc=`%O`, vdec=`%O`, no NVIDIA GPU found (or you have no clinfo installed)')
		return
	}
	if ((venc === 'qsv' || vdec === 'qsv' || venc === 'qsvElseVaapi' || vdec === 'qsvElseVaapi') && !hasIntelGpu)
	{
		console.error('venc=`%O`, vdec=`%O`, no Intel GPU found (or you have no vainfo installed)')
		return
	}*/
	if (deleteOnFail !== true) deleteOnFail = false
	const outputDirIncludingPartNumberIfPresent = (
		typeof partNumber === 'number'
		? pathJoinSmart(outputRootDir, partNumber)
		: outputRootDir
	)
	if (deleteOnFail && !/v\/[fs]\/[^\/.]{2}\/[^\/.]+/.test(outputDirIncludingPartNumberIfPresent))
	{
		console.error('cannot bad rm rf, outputDirIncludingPartNumberIfPresent=`%O`'.bgRed.white, outputDirIncludingPartNumberIfPresent)
		return
	}
	const tcToUpQueue = new PQueue({concurrency: 1})
	const onTranscodeProgress = (...args: Parameters<NonNullable<typeof _onTranscodeProgress>>) => {
		if (typeof _onTranscodeProgress === 'function')
		{
			// TODO: если tc-server и up-server на разных серверах,
			// то закачать следует на up-server, то есть это следующая строка
			//tcToUpQueue.add(async () => {
				//console.log('a1{'.bgRed)
				_onTranscodeProgress(...args)
				//console.log('a2}'.bgRed)
			//})
		}
	}
	//if (!inputFileDir) aaa()
	//if (!inputFileName) bbb()
	//if (!outputDir) ccc()
	//if (!jsonFail) eee()
	const imageJobKillMethods = AbortController2()
	const tcJobInputKillMethods = AbortController2()
	const tcJobSingleFileKillMethods = AbortController2()
	const tcJobMultiFileKillMethods  = AbortController2()
	const shakaKillMethods = AbortController2()
	let imageJobInProcess = false
	let tcJobInputInProcess = false
	let tcJobSingleFileInProcess = false
	let tcJobMultiFileInProcess = false
	let tcJobMultiFilePidObject: {fn: void|{(): number}} = {fn: void 0}
	let tcJobMultiFileLogFile: null|FileHandle = null
	const tcJobMultiFileLogFileWriteQueue = new PQueue({concurrency: 1})

	const sigkillTc = async () => {

	}

	let siginted = false
	const sigint = async () => {
		try {
			siginted = true
			const willWait = (
				tcJobInputInProcess
				||
				tcJobSingleFileInProcess
				||
				(
					tcJobMultiFileInProcess
					&&
					!(
						!dashStarted
						&&
						dashFailed
						&&
						tcJobMultiFilePidObject.fn
					)
				)
				||
				imageJobInProcess
			)
			if (tcJobInputInProcess)
			{
				tcJobInputKillMethods.smoothKill?.()
				tcJobInputInProcess = false
			}
			if (tcJobSingleFileInProcess)
			{
				tcJobSingleFileKillMethods.smoothKill?.()
				tcJobSingleFileInProcess = false
			}
			if (tcJobMultiFileInProcess)
			{
				tcJobMultiFileKillMethods.smoothKill?.()
				tcJobMultiFileInProcess = false
			}
			if (imageJobInProcess)
			{
				imageJobKillMethods.smoothKill?.()
				imageJobInProcess = false
			}
			try {
				shakaKillMethods.smoothKill?.()
			} catch (e)
			{

			}
			if (!dashStarted && dashFailed && tcJobMultiFilePidObject.fn)
			{
				await engine.wait(100)
				const pid = tcJobMultiFilePidObject.fn()
				if (pid)
				{
					await kill(pid, 'SIGKILL')
				}
			}
			else if (willWait) await engine.wait(400)
			//tcJobMultiFileLogFileWriteQueueByController.delete(tcJobMultiFileKillMethods)
			if (tcJobMultiFileLogFile)
			{
				await tcJobMultiFileLogFileWriteQueue.onIdle()
				const u = tcJobMultiFileLogFile
				tcJobMultiFileLogFile = null
				console.log('logFile.close()')
				await u.close()
			}
		} catch (e)
		{
			console.error(e)
		}
	}

	let dashStarted = false
	let dashFailed = false
	{
		let jsonFailDone = false
		jsonFail = ((origJsonFail) => {
			return async (...args) => {
				if (jsonFailDone) return// jsonFail instanceof AsyncFunction ? Promise.resolve() : void 0
				console.error('[start] jsonFail'.bgRed)
				console.error(...args)
				console.error('[end] jsonFail'.bgRed)
				jsonFailDone = true
				dashFailed = true
				await sigint()
				if (!dashStarted)
				{
					if (
						deleteOnFail
						&&
						await commons.fsExistsAsync(outputDirIncludingPartNumberIfPresent)
						&&
						!await commons.fsExistsAsync( // if another process was started simultaneously with this process accidentally.
							partNumber === 0
							? premetaMultipartFilePath!
							: premetaFilePath
						)
					)
					{
						console.log('!!!!!       rm -rf %s'.bgBlue, outputDirIncludingPartNumberIfPresent)
						// if (!outputDirContainsOtherFiles) // todo
						await fsAsync.rm(
							//outputDirIncludingPartNumberIfPresent,
							(
								partNumber === 0
								? outputRootDir
								: outputDirIncludingPartNumberIfPresent
							),
							{ recursive: true, maxRetries: 3, retryDelay: 100 }
						);
					}
				}
				return await origJsonFail(...args)
			}
		})(jsonFail)
	}
	const PREWRITTEN_SEGMENT_DURATION_FILEPATH = pathJoinSmart(
		outputRootDir, 'SEGDUR'
	)
	const prewrittenSegmentDuration = engine.atof(await gracefullyReadFileUtf8(
		PREWRITTEN_SEGMENT_DURATION_FILEPATH
	))
	if (prewrittenSegmentDuration)
	{
		if (forceSegDuration)
		{
			console.warn(
				'forceSegDuration=%O is not applied because there is already SEGDUR file with value %O'.yellow,
				forceSegDuration,
				prewrittenSegmentDuration
			)
		}
		else
		{
			console.warn(
				'Found SEGDUR file, setting the segment duration to %O'.yellow,
				prewrittenSegmentDuration,
			)
		}
		forceSegDuration = prewrittenSegmentDuration
	}
	//UV_FS_O_FILEMA()
	let writeThisSegmentDuration: number = 0
	let liveStartedOn: DateNumber|0 = 0
	let premeta: void|MediaPremeta = void 0
	const premetaMultipartFilePath = typeof partNumber === 'number' ? pathJoinSmart(
		outputDirIncludingPartNumberIfPresent,
		'PREJOB_META.json'
	) : void 0
	const premetaFilePath = pathJoinSmart(
		outputRootDir,
		'PREJOB_META.json'
	)
	{
		let onStartedDone = false
		// todo: rename to wrappedOnStarted in order to have different type
		onStarted = ((origOnStarted: typeof onStarted) => {
			return async () => {
				if (onStartedDone === true) return// onStarted instanceof AsyncFunction ? Promise.resolve() : void 0
				console.log('onStartedDone'.bold)
				onStartedDone = true
				dashStarted = true
				liveStartedOn ||= Date.now()

				const promises: Array<Promise<any>> = []
				if (!prewrittenSegmentDuration)
				{
					promises.push(fsAsync.writeFile(
						PREWRITTEN_SEGMENT_DURATION_FILEPATH,
						writeThisSegmentDuration.toString()
					))
				}
				//engine.narrow<Exclude<typeof premeta, undefined>>(premeta)
				{
					const premetaAsJSON = JSON.stringify(premeta!, void 0, '\t')
					promises.push(
						fsAsync.writeFile(
							premetaFilePath,
							premetaAsJSON,
							{encoding: 'utf-8'}
						)
					)
					if (premetaMultipartFilePath && partNumber === 0)
					{
						promises.push(
							fsAsync.writeFile(
								premetaMultipartFilePath,
								premetaAsJSON,
								{encoding: 'utf-8'}
							)
						)
					}
				}
				if (outputRootDir !== outputDirIncludingPartNumberIfPresent)
				{
					promises.push(fsAsync.writeFile(
						pathJoinSmart(
							outputDirIncludingPartNumberIfPresent,
							'LAST_PARTNUMBER'
						),
						writeThisSegmentDuration.toString()
					))
				}
				if (typeof partNumber === 'number')
				{
					promises.push(fsAsync.writeFile(
						pathJoinSmart(
							outputRootDir, 'LAST_PARTNUMBER'
						),
						partNumber.toString()
					))
				}
				await Promise.all(promises)
				if (typeof origOnStarted === 'function')
				{
					await origOnStarted(liveStartedOn)
				}
			}
		})(onStarted)
	}
	if (typeof onPreprestarted === 'function')
	{
		let onPreprestartedDone = false
		onPreprestarted = ((origOnPreprestarted) => {
			return (...args) => {
				if (onPreprestartedDone) return// onPreprestarted instanceof AsyncFunction ? Promise.resolve() : void 0
				console.log('onPreprestartedDone'.bold)
				onPreprestartedDone = true
				return origOnPreprestarted(...args)
			}
		})(onPreprestarted)
	} else onPreprestarted = () => {}
	let jqfBg: Function
	if (typeof onPrestarted === 'function')
	{
		let onPrestartedDone = false
		onPrestarted = ((origOnPrestarted) => {
			return (...args) => {
				if (dashStarted)return//if (onPrestartedDone) return// onPrestarted instanceof AsyncFunction ? Promise.resolve() : void 0
				console.log('onPrestartedDone'.bold)
				onPrestartedDone = true
				if (typeof jqfBg === 'function') jqfBg()
				return origOnPrestarted(...args)
			}
		})(onPrestarted)
	} else onPrestarted = () => {}
	console.log('processMedia (inputFile=%O, outputRootDir=%O, args=%O)', inputFile, outputRootDir, arguments)
	const ifFailThenUnlink: Array<string> = []//inputFile]
	/*const result: pppp = {
		cover: '',
		video: {
			h264: []
		},
		audio: {
			aac: []
		},
	}*/
	//const inputFile = pathJoin(inputFileDir, inputFileName)
	const isSdp = inputFile.endsWith('.sdp') || inputFile === 'pipe.sdp'
	const pr = (
		isSdp
		? Protocol.SDP
		: (
			inputFile/*.toLowerCase()*/.startsWith('rtmp://')
			? Protocol.RTMP
			: (
				inputFile.includes('://') || inputFile.startsWith('/dev/') || inputFile.startsWith('pipe.')
				? Protocol.HTTP_OR_DEV_OR_SOCK
				: Protocol.FILE
			)
		)
	)
	if (pr !== Protocol.RTMP && !await commons.fsExistsAsync(inputFile))
	{
		await jsonFail('%upv__remote__fileNotFound%')
		return
	}
	const ffprobable = pr !== Protocol.SDP
	const hasInputClearEnd = pr === Protocol.FILE && !inputFile.includes('sdp.webm')
	console.log('isSdp=%O', isSdp)
	console.log('isRtmp=%O', pr === Protocol.RTMP)
	console.log('isSrt=%O', pr === Protocol.SRT) // not implemented
	console.log('isSimpleInputFile=%O', pr === Protocol.FILE)
	console.log('hasInputClearEnd=%O', hasInputClearEnd)
	if (inputFile.startsWith('/dev/') && !howMuchLive)
	{
		throw 'howMuchLive cannot be 0 when inputFile is ' + inputFile
	}

	
	//console.log('isSidp')
	const whichFormatsAlreadyDone: WhichFormatsAlreadyDone = []
	let tcPercent: number = 0

	const [,whs,easilyReadableFfprobe] = !ffprobable ? [null,null] : await spawnAsyncUTF8(
FFPROBE_PATH,
`-v error ${isSdp ? '-protocol_whitelist file,rtp,udp' : ''} -show_entries stream=width,height,codec_name,profile,pix_fmt,sample_rate,bit_rate,r_frame_rate -of json ${escapeSpawnArg(inputFile)}`
	, {logCmd: true, returnStdoutAndStderrorSeparately: true, timeLimit: howMuchLive ? 8e3 : 60e3})
	let ff
	if (ffprobable) try {
		ff = JSON.parse(whs!)
		if (
			!ff
			|| !ff.streams
			|| !ff.streams[0]
		)
		{
			await jsonFail('%upv__remote__cantGetWidthOrHeight%')
			return
		}
		if (!ff.streams[0].width && ff.streams[1].width)
		{
			const a = ff.streams[0]
			const v = ff.streams[1]
			ff.streams[0] = v
			ff.streams[1] = a
		}
		if (
			!ff.streams[0].width
			|| !ff.streams[0].height
		)
		{
			await jsonFail('%upv__remote__cantGetWidthOrHeight%')
			return
		}
	} catch (e)
	{
		await jsonFail('%upv__remote__cantGetWidthOrHeight%')
		return
	}
	inputHasAudio             = !!(ffprobable ? ff.streams[1] : inputHasAudio)
	const inputHasVideo = true // fixme
	const origVideoCodec      = (
		isSdp
		? (await fsAsync.readFile(inputFile, {encoding: 'utf-8'})).match(/^a=rtpmap:\d+\s+(VP8|VP9|H264)/m)?.[1].toLowerCase().replace(/^vp9$/, 'vp9_0')
		: (
			'vp9' !== ff.streams[0].codec_name//vp9_[0123], hevc, h264
			? ff.streams[0].codec_name
			: ff.streams[0].codec_name + (ff.streams[0].profile.match(/\d/)?.[0] ? '_' + ff.streams[0].profile.match(/\d/)?.[0] : '_0')
		)
	)
	if (origVideoCodec === null)
	{
		await jsonFail('bad video codec')
		return
	}
	const origAudioCodec      = inputHasAudio ? (ffprobable ?  ff.streams[1].codec_name  : 'opus'): null
	const origAudioSampleRate = inputHasAudio ? (ffprobable ? +ff.streams[1].sample_rate : 48e3)  : null
	let origAudioBitrate      = inputHasAudio ? (ffprobable ? +ff.streams[1].bit_rate    : 192e3) : null


	const isInterlaced = (
		ffprobable
		? /Interlaced/.test(await spawnAsyncUTF8('mediainfo', `--Inform=Video;%ScanType% ${inputFile}`))
		: false
	)
	console.log(isInterlaced)
	const deinterlaceFix = isInterlaced ? 1.5 : 1

	//const origWidth  = Math.max(1920, deinterlaceFix * +ff[].streams[0].width)
	//const origHeight = Math.max(1080, deinterlaceFix * +ff.streams[0].height)
	const origActualWidth  = deinterlaceFix * (ffprobable ? +ff.streams[0].width  : inputWidth! )
	const origActualHeight = deinterlaceFix * (ffprobable ? +ff.streams[0].height : inputHeight!)

	if (origActualHeight < 360/*476*/ && !inputFile.includes('/tmp/') && inputFile !== 'a.mp4')
	{
		await jsonFail('%upv__remote__origActualHeightIsLow%');
		return
	}
	const ratio9 = (origActualWidth / origActualHeight * 9).toFixed(1).replace(/\.0+$/, '') + ':9'
	if (origActualWidth / origActualHeight > 21.4/9)
	{
		await jsonFail('%upv__remote__tooWide%', ratio9)
		return
	}
	if (origActualHeight / origActualWidth > 21.4/9)
	{
		await jsonFail('%upv__remote__tooVertical%', ratio9)
		return
	}

	const origOrientation = origActualWidth >= origActualHeight ? 'h' : 'v'

	const [,meta2] = ffprobable && hasInputClearEnd ? await shellSpawnAsyncUTF8(
		`${FFMPEG_PATH} -hide_banner -i "${inputFile}" -map 0:0 2>&1 
		| /usr/bin/egrep '[0-9]{3,}x[0-9]{3,}|Duration'`
	) : [null, null]
	if (hasInputClearEnd) console.log('meta2=%O', meta2)
	const durationMatch = hasInputClearEnd ? (
		meta2!
		.match(/Duration: (\d+):(\d\d):(\d\d)\.(\d+),/)
	) : null
	if (!durationMatch && hasInputClearEnd)
	{
		await jsonFail('%upv__remote__%!duration');
		return
	}


	//const _origVideoBitrateInKilobitsMatch = hasInputClearEnd ? ()
	//const origVideoBitrateInBits = (ffprobable ? +ff.streams[0].bit_rate : 0)

	console.log('checkpoint 3ueueo')

	const ttToMs = function (x: [string, string, string, string, string]): number {
		if (!x)
		{
			console.trace()
		}
		return (
			+x[1] * 3600e3
			+
			+x[2] * 60e3
			+
			+x[3] * 1e3
			+
			+x[4].toString().padEnd(3, '0').slice(0, 3)
		)
	}
	const durationInMs = !ffprobable || !hasInputClearEnd ? 0 : ttToMs(durationMatch as Parameters<typeof ttToMs>[0])
	const origBitrateMatch = !ffprobable || !hasInputClearEnd ? null : meta2!.match(
		/[bB]itrate: (\d{2,}) [kK][bB]/
	)
	if (ffprobable && hasInputClearEnd && (!origBitrateMatch || !origBitrateMatch[1]))
	{
		await jsonFail('%upv__remote__%!origBitrateMatch')
		return
	}
	if (ffprobable && !ff.streams[0].pix_fmt)
	{
		await jsonFail('unknown pixel format')
		return
	}
	const whichYUV = isSdp ? 'yuv420p' : ff.streams[0].pix_fmt.match(/^yuv(\d\d\dp(?:\d+[a-z]+)?)/)?.[1]
	console.log('whichYUV=%O', whichYUV)

	// для thumbnails
	const numberOfFrames: number = (
		ffprobable && hasInputClearEnd
		? engine.atoi((await spawnAsyncUTF8('mediainfo', ['--Inform=Video;%FrameCount%', inputFile]))[1].trim())
		: 0
	)
	if (ffprobable && hasInputClearEnd && !numberOfFrames)
	{
		//await jsonFail('%upv__remote__unknownFramesCount%')
		//return
	}


	// todo: check for negative

	//let _origFps = NaN
	let _origFps: Urational|undefined = void 0
	/*if (forceInputFps) _origFps = forceInputFps//engine.toPrecision(forceInputFps, 3)
	else */if (inputFramerate) _origFps = inputFramerate
	else if (ff)
	{
		const {r_frame_rate/*, avg_frame_rate*/} = ff.streams[0]
		if (r_frame_rate && r_frame_rate.slice(-2) !== '/0')
		{
			const i = r_frame_rate.match(/^(\d+)\/(\d+)$/)
			//_origFps = i[1]/i[2]
			_origFps = [i[1], i[2]]
		}
		//else if (!avg_frame_rate || avg_frame_rate === '0/0') _origFps = 30
		/*else if (avg_frame_rate)
		{
			const i = avg_frame_rate.match(/^(\d+)\/(\d+)$/)
			//_origFps = i[1]/i[2]
			_origFps = [i[1], i[2]]
		}*/
		/*if (_origFps) float
		{
			//else _origFps = +avg_frame_rate
			_origFps = engine.toPrecision(_origFps, 3)
			console.log('------------------------')
			console.log(r_frame_rate)
		}*/
	}
	//if (!Number.isFinite(_origFps) || _origFps <= 1)//на всякий случай <=1
	if (
		!_origFps
		||
		(
			_origFps.$fromRational() <= 1 // if it is an image, then 1?
			&&
			!(
				typeof inputFramerateAsNumber === 'number'
				&&
				inputFramerateAsNumber > 0 && inputFramerateAsNumber <= 1
			)
		)
	)
	{
		if (_origFps)
		{
			console.error('bad FPS: %O', _origFps)
		}
		else
		{
			console.error('original FPS is unknown!!!')
		}
		_origFps = [30,1]
	}
	//console.log('rounded origFps=%O', origFps)
	const origFps = _origFps


	const origLandscapeHeight = Math.min(origActualWidth, origActualHeight)
	const origLandscapeWidth  = Math.max(origActualWidth, origActualHeight)


	const origVideoBitrateInBits = (
		ffprobable
		&&
		hasInputClearEnd
		&&
		origBitrateMatch // just in case
		&&
		// if caluclated input file bitrate is ≤30%,
		// then it is incorrectly calculated:
		+origBitrateMatch[1] * 1000
		>
		engine.getIdealBitrateInBits(
			2, origVideoCodec, origLandscapeHeight, origFps
		) * 0.3

		? +origBitrateMatch[1] * 1000
		: 0
	)



	if (!origAudioBitrate) origAudioBitrate = 160e3
	//if (outputAudioMaxBitrate && origAudioBitrate > outputAudioMaxBitrate) origAudioBitrate = outputAudioMaxBitrate






	let oldForceOutputs = null
	if (IS_ARZET_PC || (typeof partNumber === 'number' && partNumber > 0))
	{
		const kk: undefined|MediaPremeta = engine.gracefullyJSONParse(
			await gracefullyReadFileUtf8(
				IS_ARZET_PC
				? (premetaMultipartFilePath || premetaFilePath)
				: premetaMultipartFilePath!
			)
		)
		if (kk) {
			oldForceOutputs = forceOutputs
			forceOutputs = {
				v: kk.flatOutputs.adaptive.v.map(x => engine.objectWithoutKeys_shallowCopy(x, ['segmentDurationInSecs', 'fragType'])),
				a: kk.flatOutputs.adaptive.a.map(x => engine.objectWithoutKeys_shallowCopy(x, ['segmentDurationInSecs', 'fragType'])),
			}
		}
	}




	const kaka = {
		howMuchLive,
		hasInputClearEnd,
		origActualHeight,
		origActualWidth,
		origLandscapeHeight,
		origLandscapeWidth,
		origFps,
		origVideoCodec,
		origVideoBitrateInBits,
		origAudioBitrate,
		durationInMs,
		numberOfFrames,
		whichYUV,
		isInterlaced,
		pr,
		ff,
	}
	/*if (typeof forceOutputs?.v === 'function')
	{
		forceOutputs.v = forceOutputs.v(kaka)
	}
	const forceOutputsV = Array.isArray(forceOutputs?.v) ? forceOutputs!.v : void 0*/
	const forceOutputsV = void 0
	if (forceOutputsV)
	{
		if (
			engine.isNumOrFn(outputMinActualWidth)
		|| engine.isNumOrFn(outputMinActualHeight)
		|| engine.isNumOrFn(outputMinLandscapeWidth)
		|| engine.isNumOrFn(outputMinLandscapeHeight)
		|| engine.isNumOrFn(outputMaxActualWidth)
		|| engine.isNumOrFn(outputMaxActualHeight)
		|| engine.isNumOrFn(outputMaxLandscapeWidth)
		|| engine.isNumOrFn(outputMaxLandscapeHeight)
		)
		{
			console.warn('ignoring outputOrientation and output{Min,Max}{Actual,Landscape}{Width,Height} because forceOutput.v is an array')
		}
	}
	else
	{
		if (typeof outputMinActualWidth === 'function') outputMinActualWidth = outputMinActualWidth(kaka)
		if (typeof outputMaxActualWidth === 'function') outputMaxActualWidth = outputMaxActualWidth(kaka)
		if (typeof outputMinActualHeight === 'function') outputMinActualHeight = outputMinActualHeight(kaka)
		if (typeof outputMaxActualHeight === 'function') outputMaxActualHeight = outputMaxActualHeight(kaka)
		if (typeof outputMinLandscapeWidth === 'function') outputMinLandscapeWidth = outputMinLandscapeWidth(kaka)
		if (typeof outputMaxLandscapeWidth === 'function') outputMaxLandscapeWidth = outputMaxLandscapeWidth(kaka)
		if (typeof outputMinLandscapeHeight === 'function') outputMinLandscapeHeight = outputMinLandscapeHeight(kaka)
		if (typeof outputMaxLandscapeHeight === 'function') outputMaxLandscapeHeight = outputMaxLandscapeHeight(kaka)
	}

	if (
		typeof outputMinActualWidth === 'number'
		&&
		typeof outputMaxActualWidth === 'number'
		&& 
		outputMinActualWidth > outputMaxActualWidth
	)
	{
		throw new RangeError(`outputMinActualWidth > outputMaxActualWidth (${outputMinActualWidth} > ${outputMaxActualWidth})`)
	}
	if (
		typeof outputMinActualHeight === 'number'
		&&
		typeof outputMaxActualHeight === 'number'
		&& 
		outputMinActualHeight > outputMaxActualHeight
	)
	{
		throw new RangeError(`outputMinActualHeight > outputMaxActualHeight (${outputMinActualHeight} > ${outputMaxActualHeight})`)
	}
	if (
		typeof outputMinLandscapeWidth === 'number'
		&&
		typeof outputMaxLandscapeWidth === 'number'
		&&
		outputMinLandscapeWidth > outputMaxLandscapeWidth
	)
	{
		throw new RangeError(`outputMinLandscapeWidth > outputMaxLandscapeWidth (${outputMinLandscapeWidth} > ${outputMaxLandscapeWidth})`)
	}
	if (
		typeof outputMinLandscapeHeight === 'number'
		&&
		typeof outputMaxLandscapeHeight === 'number'
		&&
		outputMinLandscapeHeight > outputMaxLandscapeHeight
	)
	{
		throw new RangeError(`outputMinLandscapeHeight > outputMaxLandscapeHeight (${outputMinLandscapeHeight} > ${outputMaxLandscapeHeight})`)
	}
	if (
		typeof outputMinLandscapeWidth === 'number'
		&&
		typeof outputMinLandscapeHeight === 'number'
		&&
		outputMinLandscapeWidth < outputMinLandscapeHeight
	)
	{
		throw new RangeError(`outputMinLandscapeWidth < outputMinLandscapeHeight (${outputMinLandscapeWidth} < ${outputMinLandscapeHeight})`)
	}
	if (
		typeof outputMaxLandscapeWidth === 'number'
		&&
		typeof outputMaxLandscapeHeight === 'number'
		&&
		outputMaxLandscapeWidth < outputMaxLandscapeHeight
	)
	{
		throw new RangeError(`outputMinLandscapeWidth < outputMinLandscapeHeight (${outputMinLandscapeWidth} < ${outputMinLandscapeHeight})`)
	}

	if (forceOutputsV === void 0 && typeof outputMinActualHeight === 'number')
	{
		if (outputMinActualHeight < minPossibleOutputLandscapeHeightForNow)
		{
			console.warn(
				'outputMinActualHeight (%O) can\'t be less than %O, setting it to %O',
				outputMinActualHeight, minPossibleOutputLandscapeHeightForNow, minPossibleOutputLandscapeHeightForNow
			)
			outputMinActualHeight = minPossibleOutputLandscapeHeightForNow
		}
		else if (outputMinActualHeight > maxPossibleOutputLandscapeHeightForNow)
		{
			console.error(
				'outputMinActualHeight (%O) can\'t be greater than %O, setting it to %O',
				outputMinActualHeight, maxPossibleOutputLandscapeHeightForNow, maxPossibleOutputLandscapeHeightForNow
			)
			outputMinActualHeight = maxPossibleOutputLandscapeHeightForNow
		}
		else
		{
			outputMinActualHeight ||= minPossibleOutputLandscapeHeightForNow
		}
	}
	else
	{
		outputMinActualHeight ||= minPossibleOutputLandscapeHeightForNow
	}

	if (forceOutputsV === void 0 && typeof outputMaxActualHeight === 'number')
	{
		if (outputMaxActualHeight > maxPossibleOutputLandscapeHeightForNow)
		{
			console.warn(
				'outputMaxActualHeight (%O) can\'t be greater than %O, setting it to %O',
				outputMaxActualHeight, maxPossibleOutputLandscapeHeightForNow, maxPossibleOutputLandscapeHeightForNow
			)
			outputMaxActualHeight = maxPossibleOutputLandscapeHeightForNow
		}
		else if (outputMaxActualHeight < minPossibleOutputLandscapeHeightForNow)
		{
			console.error(
				'outputMaxActualHeight (%O) can\'t be less than %O, setting it to %O',
				outputMaxActualHeight, minPossibleOutputLandscapeHeightForNow, minPossibleOutputLandscapeHeightForNow
			)
			outputMaxActualHeight = minPossibleOutputLandscapeHeightForNow
		}
		else
		{
			outputMaxActualHeight ||= maxPossibleOutputLandscapeHeightForNow
		}
	}
	else
	{
		outputMaxActualHeight ||= maxPossibleOutputLandscapeHeightForNow
	}


	if (forceOutputsV === void 0 && typeof outputMinActualWidth === 'number')
	{
		if (outputMinActualWidth < minPossibleOutputLandscapeWidthForNow)
		{
			console.warn(
				'outputMinActualWidth (%O) can\'t be less than %O, setting it to %O',
				outputMinActualWidth, minPossibleOutputLandscapeWidthForNow, minPossibleOutputLandscapeWidthForNow
			)
			outputMinActualWidth = minPossibleOutputLandscapeWidthForNow
		}
		else if (outputMinActualWidth > maxPossibleOutputLandscapeWidthForNow)
		{
			console.error(
				'outputMinActualWidth (%O) can\'t be greater than %O, setting it to %O',
				outputMinActualWidth, maxPossibleOutputLandscapeWidthForNow, maxPossibleOutputLandscapeWidthForNow
			)
			outputMinActualWidth = maxPossibleOutputLandscapeWidthForNow
		}
		else
		{
			outputMinActualWidth ||= minPossibleOutputLandscapeWidthForNow
		}
	}
	else
	{
		outputMinActualWidth ||= minPossibleOutputLandscapeWidthForNow
	}

	if (forceOutputsV === void 0 && typeof outputMaxActualWidth === 'number')
	{
		if (outputMaxActualWidth > maxPossibleOutputLandscapeWidthForNow)
		{
			console.warn(
				'outputMaxActualWidth (%O) can\'t be greater than %O, setting it to %O',
				outputMaxActualWidth, maxPossibleOutputLandscapeWidthForNow, maxPossibleOutputLandscapeWidthForNow
			)
			outputMaxActualWidth = maxPossibleOutputLandscapeWidthForNow
		}
		else if (outputMaxActualWidth < minPossibleOutputLandscapeWidthForNow)
		{
			console.error(
				'outputMaxActualWidth (%O) can\'t be less than %O, setting it to %O',
				outputMaxActualWidth, minPossibleOutputLandscapeWidthForNow, minPossibleOutputLandscapeWidthForNow
			)
			outputMaxActualWidth = minPossibleOutputLandscapeWidthForNow
		}
		else
		{
			outputMaxActualWidth ||= maxPossibleOutputLandscapeWidthForNow
		}
	}
	else
	{
		outputMaxActualWidth ||= maxPossibleOutputLandscapeWidthForNow
	}
	if (forceOutputsV === void 0)
	{
		if (typeof outputOrientation === 'function')
		{
			/*kaka.outputMinActualWidth = outputMinActualWidth
			kaka.outputMaxActualWidth = outputMaxActualWidth
			kaka.outputMinActualHeight = outputMinActualHeight
			kaka.outputMaxActualHeight = outputMaxActualHeight
			kaka.outputMinLandscapeWidth = outputMinLandscapeWidth
			kaka.outputMaxLandscapeWidth = outputMaxLandscapeWidth
			kaka.outputMinLandscapeHeight = outputMinLandscapeHeight
			kaka.outputMaxLandscapeHeight = outputMaxLandscapeHeight*/
			outputOrientation = outputOrientation(kaka)
		}
		if (outputOrientation)
		{
			// @ts-ignore
			if (['l', 'landscape', 'horiz', 'horizontal'].includes(outputOrientation)) outputOrientation = 'h'
			// @ts-ignore
			else if (['vert', 'vertical'].includes(outputOrientation)) outputOrientation = 'v'
			else if (!['v', 'h'].includes(outputOrientation))
			{
				console.warn(
					'ignoring outputOrientation (%O) because it is neither "h" nor "v"',
					outputOrientation
				)
				outputOrientation = void 0
			}
			else if (outputOrientation === 'v')
			{
				if (
					// @ts-ignore
					VERTICAL_VIDEOS_ALLOWED === false
				)
				{
					console.warn('Ignoring outputOrientation = v because VERTICAL_VIDEOS_ALLOWED is false')
				}
			}
		}
	}

	if (forceOutputsV === void 0 || !forceOutputsV[0])
	{
		if (
			// @ts-ignore
			VERTICAL_VIDEOS_ALLOWED === false
		)
		{
			outputOrientation = 'h'
		}
		else
		{
			// fixme: if min === max
			outputOrientation ||= (origActualWidth >= origActualHeight ? 'h' : 'v')
		}
		if (forceOutputsV === void 0)
		{
			if (!outputMinLandscapeHeight) outputMinLandscapeHeight = Math.min(outputMinActualWidth, outputMinActualHeight)
			if (!outputMinLandscapeWidth)  outputMinLandscapeWidth  = Math.max(outputMinActualWidth, outputMinActualHeight)
			if (!outputMaxLandscapeHeight) outputMaxLandscapeHeight = Math.min(outputMaxActualWidth, outputMaxActualHeight)
			if (!outputMaxLandscapeWidth)  outputMaxLandscapeWidth  = Math.max(outputMaxActualWidth, outputMaxActualHeight)
		}
	}
	else
	{
		outputOrientation = forceOutputsV[0].w >= forceOutputsV[0].h ? 'h' : 'v'
	}






	/*const finalOutputShouldBeRotated = (
		VERTICAL_VIDEOS_ALLOWED && origActualHeight > origActualWidth * 1.1
	)//todo: или не 1.1?
	console.log('finalOutputShouldBeRotated=%O', finalOutputShouldBeRotated)
	*/


	alsoSingleFile = false && howMuchLive


	

	// -level auto
	//for (c)
	/*
240p = 352 x 240
360 p = 480 x 360
480p = 858 x 480 — also known as SD
	*/
	//let status = 'ok'
	//const h1 = origLandscapeHeight >= 630 && origLandscapeHeight <= 720 ? 720 :
	/*const h1 = origLandscapeHeight < 630 ? false : 720/*(
		origLandscapeHeight >= 630 && origLandscapeHeight <= 721
		? 720 // origLandscapeHeight
		: (origLandscapeHeight > 740 ? 720 : origLandscapeHeight)
	)*/
	//const h2 = origLandscapeHeight < 630 ? origLandscapeHeight : 480//выше проверка, что нельзя h<476

	if (forceOutputsV?.length === 0) enableVideo = false
	//if (forceOutputsA?.length === 0) enableAudio = false //todo

	const seekCover = howMuchLive ? 2000 : 1500//'00:00:01.5'
	const doRawLandscapeH: Array<PossibleHeight> = enableVideo === false ? [] : []//[486]//[486, h1, 360, 234, 144].filter(x => x)
	
	/*if (
		!finalOutputShouldBeRotated
		&&
		possibleOutputHeightsForNow.includes(144)
		&&
		enableVideo
	)
	{
		doH264LandscapeHeights.push(144)
	}*/
	// первым пусть 486p
	// разрешения 480p и 240p как на ютубе не получаются, т.к. ffmpeg ругается на разный aspect ratio
	// 1920/1080 = ~1.77777777777777777778
	// 1280/720 = ~1.77777777777777777778
	// 960/540 = ~1.77777777777777777778
	// 832/468 = ~1.77777777777777777778
	// 640/360 = ~1.77777777777777777778
	// 256/144 = ~1.77777777777777777778
	// вместо 480p (854x480) надо либо 832x468, либо 864x486. Выбираю 486p, ибо в гугле результатов больше (46 против 232000).
	// вместо 240p (426x240) надо либо 416x234, либо 448x252. Выбрал бы 252p, ибо в гугле результатов больше (23400 против 319000), но ругается ffmpeg



	let newAacBitrateKbpsWithK: string

	if (!oldForceOutputs)
	{
		console.log({
			outputMinActualWidth,
			outputMinActualHeight,
			outputMinLandscapeWidth,
			outputMinLandscapeHeight,
			outputMinFramerate,

			outputMaxActualWidth,
			outputMaxActualHeight,
			outputMaxLandscapeWidth,
			outputMaxLandscapeHeight,
			outputMaxFramerate,
		})
	}


	let finalOutputsV: Array<DeepRequired<Output_vActual2>> = (
		(typeof forceOutputs?.v === 'function' ? null : forceOutputs?.v)
		||
		(inputHasVideo && enableVideo
		? (() => 
		{
			const ret: Array<Output_v> = []


			const doAv1LandscapeHeights: Array<PossibleHeight> = enableVideo === false ? [] : []
			outputAllowUpscaleButOnlyIfLandscapeHeightIsLessThanMaxLandscapeHeightInTimes = (
				outputAllowUpscaleButOnlyIfLandscapeHeightIsLessThanMaxLandscapeHeightInTimes === 0
				? 1.0
				: +(outputAllowUpscaleButOnlyIfLandscapeHeightIsLessThanMaxLandscapeHeightInTimes || 0.83) || 0.83
			)
			const doH264LandscapeHeights: Array<PossibleHeight> = enableVideo === false ? [] : (inputFile.includes('Qongqothw')||inputFile.includes('vp8vert')||inputFile.includes('3de90242') ? [234] : (
				TestSegmentsModeDebug
				// @ts-ignore
				//? [Math.max(h1, 486)]
				? [234] as Array<PossibleHeight>
				: possibleOutputLandscapeHeightsForNow.filter(candidateLandscapeH => {
					//if (origLandscapeHeight >= candidateLandscapeH * 0.83) return true // 1920*0.83=1593 1080*0.83 =896.4  Хочу, чтобы 1600x900 в 1920x1080.
					if (candidateLandscapeH <= 160 && outputOrientation === 'v') return false// fixme: 144? 160? 180?
					if (
						candidateLandscapeH < outputMinLandscapeHeight!
						||
						candidateLandscapeH * outputAllowUpscaleButOnlyIfLandscapeHeightIsLessThanMaxLandscapeHeightInTimes!
						>
						outputMaxLandscapeHeight!
					)
					{
						return false
					}
					const candidateActualH = (
						outputOrientation === 'v'
						? candidateLandscapeH/9*16|0
						: candidateLandscapeH
						//todo?: change 9*16 to allow nonstandard aspect ratios
					)
					if (
						candidateActualH < outputMinActualHeight!
						||
						candidateActualH * outputAllowUpscaleButOnlyIfLandscapeHeightIsLessThanMaxLandscapeHeightInTimes!
						>
						outputMaxActualHeight!
					)
					{
						return false
					}
					return true
				}) as Array<PossibleHeight>
			)/*.filter(h => (
				possibleOutputHeightsForNow.includes(
					// @ts-ignore
					h
				)
				//&&
				//!(h === 1080 && howMuchLive)
			))*/.sort((a, b) => {
				return a === 486 ? -1 : (b === 486 ? 1 : 0) // fixme: is it worth it to do .sort() if there's a .sort() later
			}))
			if (
				0 === forceOutputs?.v?.length
				&&
				0 === doRawLandscapeH.length + doAv1LandscapeHeights.length + doH264LandscapeHeights.length
			)
			{
				throw new Error('no output heights! Change your output{Min,Max}{Actual,Landscape}{Width,Height}')
			}

			doH264LandscapeHeights.forEach(lh => {
				ret.push({
					codec: 'h264',
					w: outputOrientation === 'h' ? lh/9*16 : lh,
					h: outputOrientation === 'h' ? lh : lh/9*16,
				})
			})
			return ret
		})()
		: []
		)
	)
	.map(x => {
		engine.narrow<Urational>(outputMinFramerate)
		engine.narrow<Urational>(outputMaxFramerate)
		return fixFinalOutputV(x, howMuchLive, origOrientation, origFps, origLandscapeHeight, origVideoCodec, origVideoBitrateInBits, outputMinFramerate, outputMaxFramerate, hacks)
	})
	if (typeof forceOutputs?.v === 'function')
	{
		const pending = forceOutputs.v(kaka, finalOutputsV)
		if (pending) finalOutputsV = pending.map(x => fixFinalOutputV(x, howMuchLive, origOrientation, origFps, origLandscapeHeight, origVideoCodec, origVideoBitrateInBits, outputMinFramerate, outputMaxFramerate, hacks))
	}
	/*if (!forceOutputs)finalOutputsV = finalOutputsV.map(x => {
		// todo: For DASH/HLS, FPSes should be X, X/2, X*2;
		// if (hacks.) return x
		return x
	})*/
	finalOutputsV = finalOutputsV.sort((oa, ob) => {
		let a: number
		let b: number
		if (oa.codec === 'copy') a = 0
		else if (oa.codec === 'h264') a = 1
		else if (oa.codec === 'h265') a = 2
		else if (oa.codec === 'h266') a = 3
		else if (oa.codec === 'vp8') a = 4
		else if (oa.codec === 'vp9') a = 5
		else if (oa.codec === 'av1') a = 6
		else throw new Error('finalOutputA.sort():: unknown codec')
		if (ob.codec === 'copy') b = 0
		else if (ob.codec === 'h264') b = 1
		else if (ob.codec === 'h265') b = 2
		else if (ob.codec === 'h266') b = 3
		else if (ob.codec === 'vp8') b = 4
		else if (ob.codec === 'vp9') b = 5
		else if (ob.codec === 'av1') b = 6
		else throw new Error('finalOutputA.sort():: unknown codec')
		if (a < b) return -1
		if (a > b) return 1
		const oa8 = (oa.w === 864 && oa.h === 486) || (oa.w === 486 && oa.h === 864)
		const ob8 = (ob.w === 864 && ob.h === 486) || (ob.w === 486 && ob.h === 864)
		if (!(oa8 || ob8))
		{
			if (oa.w < ob.w) return -1
			if (oa.w > ob.w) return 1
			if (oa.h < ob.h) return -1
			if (oa.h > ob.h) return 1
		}
		if (oa8 && !ob8) return -1
		if (!oa8 && ob8) return 1
		if (oa.fps < ob.fps) return -1
		if (oa.fps > ob.fps) return 1
		if (oa.bitrateInBits < ob.bitrateInBits) return -1
		if (oa.bitrateInBits > ob.bitrateInBits) return 1
		console.warn('finalOutputV.sort():: two outputs whose codec, width/height, fps, bitrate are same??????')
		return 0
	})







	// https://developer.apple.com/documentation/http_live_streaming/http_live_streaming_hls_authoring_specification_for_apple_devices#2969514
	// 7.5. Target durations SHOULD be 6 seconds.
	// 7.6. Segment durations SHOULD be nominally 6 seconds (for example, NTSC 29.97 may be 6.006 seconds).
	// Однако:
	// https://www.wowza.com/blog/hls-latency-sucks-but-heres-how-to-fix-it
	// Currently, in the HLS Cupertino default settings, Apple recommends a minimum of six seconds for the length of each segment duration. We have seen success manipulating the size to half a second. 
	/*const ___secs = (
		howMuchLive <= 1
		? 2.0
		: (
			howMuchLive === 2
			? 1 
			: (
				//howMuchLive === 3
				//?
				0.7
			)
		)
	)*/
	const highestOutputFps = finalOutputsV.$mathMaxRationalByField('fps')
	const lowestOutputFps  = finalOutputsV.$mathMinRationalByField('fps')
	console.log(
		'list of outputs\' unique FPSs=%O',
		engine.array_unique(finalOutputsV.map(x=>x.fps!)).$sortRationals()
	)
	console.log('highestOutputFps=%O', lowestOutputFps)
	console.log('lowestOutputFps=%O', lowestOutputFps)
	/*
(24000/1001)fps requires min seg dur 21.3546666666666s
https://www.wolframalpha.com/input/?i=1%2F%2824000%2F1001%29+*+x+mod+%281%2F48000*1024%29+%3D+0
says 512n
1/(24/1001)*512=21354.6666666666666ms
(30000/1001)fps says 640n = min26.6666666s; (60000/1001) is also min26.6666666s

https://www.wolframalpha.com/input/?i=1%2F%2824%29+*+x+mod+%281%2F48000*1024%29+%3D+0
24fps says 64n, so min 2.66666666s


https://anton.lindstrom.io/gop-size-calculator/
Segment duration in sec	Video Frames	Audio Frames
2.66666666
5.33333333
8.0
24fps 24 fps with AAC 48000 HZ

Segment duration in sec	Video Frames	Audio Frames
0.32	8	15
0.64	16	30
0.96	24	45
1.28	32	60
1.6	40	75
1.92	48	90
2.24   56
2.56   64
2.88   72
3.2    80
3.84	96	180
6.4	160	300
25fps 25 fps with AAC 48000 HZ

Segment duration in sec	Video Frames	Audio Frames
0.53333333
1.06666666
1.6	48	75
2.13333333
2.66666666
3.2
4.8	144	225
6.4	192	300
30fps 30 fps with AAC 48000 HZ
	*/
	/* BTW:
	https://stackoverflow.com/a/42152270
	quote: there is a variant of AAC which decodes to 960 samples. So, a 48 kHz audio could be encoded to a stream exactly 10 seconds long. FFmpeg does not sport such an AAC encoder. AFAIK, many apps including itunes will not play such a file correctly. If you want to encode to this spec, there's an encoder available at https://github.com/Opendigitalradio/ODR-AudioEnc
	*/
	const SEGMENT_DURATION_mathExpr: string = (
		forceSegDuration
		||
		(
			(() => {
				//if (pr === Protocol.RTMP) return 3.2
				if (Math.round(highestOutputFps.$fromRational()) % 25 === 0)
				{
					//case 24: return 2.048
					//case 24: return 2.6666666666666665
					//2.24 is not friend to he-aac
					//1.92 and 2.56 are friends to he-aac @video25fps
					return howMuchLive ? 1.92 : 2.56 // or 1.6, 1.92, 2.24, 2.56, 2.88, 3.2, 3.52, 3.84, 4.16
				}
				if (Math.round(highestOutputFps.$fromRational()) % 15 === 0)
				{
					//1.066666, 2.13333, 3.2 are friends to he-aac @video30fps
					return howMuchLive ? 1.6 : 2.13333333333333333 // or 1.6, 2.13333333333333333, 2.6666666666666665, 3.2, 3.7333333333333334, 4.266666666666667, 4.8
				}
				return howMuchLive ? 1.6 : 3.2
			})()
		)
		|| 1.6//engine.toPrecision(Math.ceil(___secs * 1000 / (1000/lowestOutputFps)) * (1000/lowestOutputFps) / 1000, 4)
		// or maybe 8 secs because 8 secs are good any non-float fps
	).toString()
	/*
	// lcm = least common multiple
	// HE-AAC (v1, v2) is always 24000 Hz, OPUS is always 48000 Hz.
	// Some implementations of lcm don't support float. Sanity check:
	assert(lcm(2, 3) === 6)
	assert(lcm(2, 3, 4) === 12)
	assert(lcm(1024/24000, 1/30) === 3.2/3)
	assert(lcm(1024/24000, 960/48000, 1/30) === 3.2)
	//const minPossibleSegmentDuration = lcm(1024/24000, 1/30) // AAC only
	//const minPossibleSegmentDuration = lcm(1024/24000, 1/30) // HE-AAC_v2+30FPS (+60FPS)
	//const minPossibleSegmentDuration = lcm(1024/48000, 1/30) // AAC-LC+30FPS (+60FPS)
	const minPossibleSegmentDuration = lcm(1024/24000, 960/48000, 1/30) // HE-AAC_v2+OPUS+30FPS(+AAC-LC+60FPS)
	const minDesirableSegmentDuration = isLive ? 1.6 : 2.0
	const optimalSegmentDuration = ceil(
		minDesirableSegmentDuration / minPossibleSegmentDuration
	) * minPossibleSegmentDuration
	*/
	const SEGMENT_DURATION_float: number = engine.fixNumber(mthEvaluate(SEGMENT_DURATION_mathExpr))
	if (!prewrittenSegmentDuration) writeThisSegmentDuration = SEGMENT_DURATION_float
	console.log('SEGMENT_DURATION=%O'.yellow, SEGMENT_DURATION_mathExpr)



	// https://developer.apple.com/documentation/http_live_streaming/http_live_streaming_hls_authoring_specification_for_apple_devices#overview
	// 1.13. Key frames (IDRs) SHOULD be present every two seconds.
	// если live, то keyframe должен быть <= segment duration
	const keyFrameEverySeconds: number = SEGMENT_DURATION_float / 2 >= 1.6 ? 3.2 : SEGMENT_DURATION_float








	/*
	Согласно
	https://developer.apple.com/documentation/http_live_streaming/http_live_streaming_hls_authoring_specification_for_apple_devices#overview
	2.4. You SHOULD NOT use HE-AAC if your audio bit rate is above 64 kbit/s.
	(говорится про HE-AAC, но не написано v1 или v2, следовательно касается всех версий HE-AAC)

	OPUS 48 / HE-AAC v2 48
	OPUS 72 / HE-AAC v2 64
	OPUS 128 / LC-AAC 160
	OPUS 192 / LC-AAC 256
	Выше aac256 смысла нет, а opus192 это как aac256 или mp3 320kbps.

	Всем LC-AAC от 128kbps соответствует OPUS с битрейтом - 64, поэтому у наикрутого OPUS битрейт меньше на 64, чем у наикрутого LC-AAC.
	*/
	// много аудиоформатов и битрейтов только для hls/dash.
	// т.е. для того h480p.mp4 файла сверху только один вариант.
	console.log('origAudioCodec=%O origAudioBitrate=%O', origAudioCodec, origAudioBitrate)
	let finalOutputsA: Array<DeepRequired<Output_a>> = (
		(typeof forceOutputs?.a === 'function' ? null : forceOutputs?.a)
		||
		(inputHasAudio && enableAudio
		? (() => 
		{
			const ret: Array<Output_a> = []

			//48, 64, 160, 256
			// первым пусть opus128/aac160
			const doAudioAacBitrates: Array<number> = []
			const doAudioOpusBitrates: Array<number> = []

			if (origAudioCodec !== 'opus')//aac, vorbis
			{
				if (!origAudioBitrate || origAudioBitrate! >= 96e3)
				{
					doAudioAacBitrates.push(Math.min(160e3, origAudioBitrate!))
					doAudioOpusBitrates.push(Math.min(128e3, origAudioBitrate! * 0.77))
				}
				if (!origAudioBitrate || origAudioBitrate! >= 61e3)
				{
					doAudioAacBitrates.push(64e3)
					doAudioOpusBitrates.push(72e3)
				}
				if (origAudioBitrate! >= 220e3)
				{
					// на всякий случай (не проверял) aac в конец, ибо apple
					doAudioOpusBitrates.push(Math.min(192e3, origAudioBitrate! * 0.77))
				}
				//doAudioAacBitrates.push(48e3)
				doAudioOpusBitrates.push(48e3)
				if (origAudioBitrate! >= 220e3)
				{
					doAudioAacBitrates.push(Math.min(256e3, origAudioBitrate!))
				}
			}
			else
			{// OPUS:
				if (!origAudioBitrate || origAudioBitrate! >= 96e3)
				{
					doAudioOpusBitrates.push(Math.min(128e3, origAudioBitrate!))
					doAudioAacBitrates.push(Math.min(160e3, origAudioBitrate! * 1.25))
				}
				if (!origAudioBitrate || origAudioBitrate! >= 61e3)
				{
					doAudioOpusBitrates.push(Math.min(72e3, origAudioBitrate!))
					doAudioAacBitrates.push(64e3)
				}
				if (origAudioBitrate! >= 180e3)
				{
					// на всякий случай (не проверял) aac в конец, ибо apple
					doAudioOpusBitrates.push(Math.min(192e3, origAudioBitrate!))
				}
				//doAudioAacBitrates.push(48e3)
				doAudioOpusBitrates.push(Math.max(32e3, Math.min(origAudioBitrate!, 48e3)))
				if (origAudioBitrate! >= 180e3)
				{
					doAudioAacBitrates.push(Math.min(256e3, origAudioBitrate! * 1.25))
				}
			}
			const IS_BUGGY_OPUS = origAudioCodec !== 'opus' // I forgot what's the bug in ffmpeg, maybe it was whole input being written into single segment (or was that only with qsv?)?
			const WILL_OUTPUT_OPUS = enableAudio && !IS_OPUS_BANNED && !IS_BUGGY_OPUS && !howMuchLive
			if (!WILL_OUTPUT_OPUS) while (doAudioOpusBitrates.length) doAudioOpusBitrates.pop()
			if (IS_AAC_BANNED) while (doAudioAacBitrates.length) doAudioAacBitrates.pop()

			doAudioOpusBitrates.forEach((curBr) => {
				// лучше заполучить -vbr:a constrained, чем сохранить 1% качества с помощью copy
				ret.push({
					codec: 'opus',
					bitrateInBits: curBr,
				})
			})
			doAudioAacBitrates.forEach((curBr) => {
				//let cutoff: Output_a['cutoff']
				let codec: Output_a['codec']
				if (
					false
					&&
					origAudioCodec === 'aac'
					&&
					origAudioBitrate
					&&
					GOOD_AUDIO_SAMPLE_RATES.includes(origAudioSampleRate!)
					&&
					Math.abs(curBr - origAudioBitrate!) < 10e3
				)
				{
					codec = 'copyaudio'
				}
				else
				{
					codec = 'aac-lc'
				}
				ret.push({
					bitrateInBits: curBr,
					codec,
				})
			})
			return ret
		})()
		: []
		)
	).map(x => fixFinalOutputA(x, hacks, lowestOutputFps, SEGMENT_DURATION_float))
	if (typeof forceOutputs?.a === 'function')
	{
		const pending = forceOutputs.a(kaka, finalOutputsV, finalOutputsA)
		if (pending) finalOutputsA = pending.map(x => fixFinalOutputA(x, hacks, lowestOutputFps, SEGMENT_DURATION_float))
	}
	finalOutputsA = finalOutputsA.sort((oa, ob) => {
		let a: number
		let b: number
		if (oa.codec === 'copyaudio') a = 0
		else if (oa.codec === 'opus') a = 1
		else if (oa.codec === 'aac-lc') a = 2
		else if (oa.codec === 'he-aac_v2') a = 3
		else throw new Error('finalOutputA.sort():: unknown codec')
		if (ob.codec === 'copyaudio') b = 0
		else if (ob.codec === 'opus') b = 1
		else if (ob.codec === 'aac-lc') b = 2
		else if (ob.codec === 'he-aac_v2') b = 3
		else throw new Error('finalOutputA.sort():: unknown codec')
		if (a < b) return -1
		if (a > b) return 1
		if (oa.bitrateInBits < ob.bitrateInBits) return -1
		if (oa.bitrateInBits > ob.bitrateInBits) return 1
		console.warn('finalOutputA.sort():: two outputs whose codec and bitrate are same??????')
		return 0
	})
	// todo remove duplicates

	const finalMinH = Math.min(
		Infinity,//finalOutputsV.$mathMinByField('w'),
		finalOutputsV.$mathMinByField('h'),
	)
	const finalMaxH = Math.max(
		-Infinity,//finalOutputsV.$mathMaxByField('w'),
		finalOutputsV.$mathMaxByField('h'),
	)
	const finalMinLandscapeH = Math.min(
		finalOutputsV.$mathMinByField('w'),
		finalMinH//finalOutputsV.$mathMinByField('h'),
	)
	const finalMaxLandscapeH = Math.max(
		...finalOutputsV.map(x=>Math.min(x.w, x.h))
	)

	const finalMinLandscapeW = Math.min(
		...finalOutputsV.map(x=>Math.max(x.w, x.h))
	)
	const finalMaxLandscapeW = Math.max(
		...finalOutputsV.map(x=>Math.max(x.w, x.h))
	)

	const outputInputMediaFilePath = pathJoinSmart(
		outputDirIncludingPartNumberIfPresent,
		'inputMediaFile.mkv'
	)
	const outputMediaManifestFilePath = pathJoinSmart(
		outputDirIncludingPartNumberIfPresent,
		'manifest.mpd'
	)
	const outputOrigAudioFilePath = pathJoinSmart(
		outputDirIncludingPartNumberIfPresent,
		isSdp ? 'origAudio.opus' : 'origAudio.mkv'
	)
	const outputOrigVideoFilePath = pathJoinSmart(
		outputDirIncludingPartNumberIfPresent,
		'origVideo.mkv'
	)
	const outputOrigVideoPlusAudioFilePath = pathJoinSmart(
		outputDirIncludingPartNumberIfPresent,
		'origInput.mkv'
	)
	//740/444 -- ~1.6666 oplata
	//474/284 -- ~1.6690 list-item (desktop)
	//750/410 -- ~1.8292 slider (desktop)
	//262/172 -- ~1.5232 mainpage
	//222/146 -- ~1.5205 list kurs item (desktop)
	//286/188 -- ~1.5212 list kurs item (mobile)
	const outputVideoCoverNoExt = pathJoinSmart(
		outputRootDir,
		/*(
			typeof partNumber === 'number'
			? [partNumber]
			: ''
		),*/
		//inputFileName.replace(/(?:\.(mp4|webm|mkv|3gp|mov|flv))?$/i, '.cover')
		'cover',
	)
	const outputVideoCoverFinalPath = outputVideoCoverNoExt + (
		hasInputClearEnd
		? '.webp'
		: '.png'
	)
	const outputThumbsDir = pathJoinSmart(
		outputDirIncludingPartNumberIfPresent,
		'thumbs'
	)
	const thumbTplPath = pathJoinSmart(
		outputRootDir,
		'thumbs',
		'%03d.webp'
	)




	const audioOutputsCount = finalOutputsA.length
	const videoOutputsCount = finalOutputsV.length
	const ffmpegMainProcessBin = isSdp ? 'env' : 'nice'
	const ffmpegMainProcessOpts: string = (
		(
			isSdp
			? `W=${origActualWidth} H=${origActualHeight} nice -n -10 ${FFMPEG_PATH}`
			: (
				pr === Protocol.RTMP
				? `-n -10 ${FFMPEG_PATH}`
				: `-n 19 ${FFMPEG_PATH}`
			)
		)
		+ ' '
	) + await (async () =>
	{
		//const tune = howMuchLive ? '-tune zerolatency'
		//const tune = ' -tune film'

		//const audioCodecSpecific = !inputHasAudio ? '' : origAudioCodec === 'aac' && (h >= 480 || origAudioBitrate! < 132*1024) ? '-acodec copy' : '-c:a aac -b:a ' + (h === 240 ? '128' : (h === 360 ? '192' : '256')) + 'k'
		//const audioSpecific = inputHasAudio ? `${audioCodecSpecific} -ac 2` : '-an'
		//const audioSpecific = '-an'
		// -an это без звука
		// -ac 2 это mono to stereo


		const LOGLEVEL: string = (() => {
			let ret = 'level+'
			if (process.env.TRACE && process.env.TRACE !== '0') ret += 'trace'
			else if (process.env.DEBUG && process.env.DEBUG !== '0') ret += 'debug'
			else if (process.env.VERBOSE && process.env.VERBOSE !== '0') ret += 'verbose'
			else ret += 'info'
			return ret
		})()

		// *_qsv (Intel Quick Sync) encoder has a bug: it causes video (not audio) to be written into a *single* file when force_key_frames is present
		let opts = (
`-hide_banner -nostdin -threads ${CPU_THREADS} -loglevel ${LOGLEVEL} `
+ (
howMuchLive
? ` -flags +low_delay`
: ``
)
+ (
pr !== Protocol.FILE
//? ` -loglevel level+debug
// max_ts_probe 0 ???
? ` -protocol_whitelist file,rtp,udp,tcp,rtmp ${pr === Protocol.SDP ? '-use_wallclock_as_timestamps 1' : ``} ` //  -ss 300ms -noaccurate_seek
// -rtsp_flags listen -rtsp_transport tcp 
: ``
// usewallclock 1 for input=file gives  '2030727167 frame duplication too large, skipping\n' +
) 
// -r ${origFps} ускоренным делает
+ (!isSdp ? '' : `   -abort_on empty_output_stream `/*+nofillin*/ /* +discardcorrupt */)
// если убрать -thread_queue_size или -reorder_queue_size или оба (не знаю), то цветовой шум иногда. Или не влияет?
+ (pr === Protocol.SDP  ? '' :  ` -thread_queue_size  ${
//0
9*(videoOutputsCount*10 + audioOutputsCount*2) / 10 | 0
}`)
+ (pr === Protocol.SDP ? ` -reorder_queue_size ${
0

}` : (pr === Protocol.RTMP ? 9*(videoOutputsCount*10 + audioOutputsCount*2) : ``))
+ (!isSdp || !FFMPEG_PATH.includes('-sand') ? '' : ` -video_size ${origActualWidth}x${origActualHeight}`)// -pixel_format yuv420p`)
+ (!howMuchLive/*||pr===Protocol.RTMP*/ ? ' -fflags +genpts+discardcorrupt' : ` -fflags +genpts+discardcorrupt+flush_packets`)//+igndts +flush_packets +discardcorrupt
+ (pr !== Protocol.FILE ? '+igndts' : '')
+ ` -flags +cgop -start_at_zero -copyts`
//+ (pr===Protocol.RTMP || howMuchLive ? ` -fflags +genpts` : '')
// +discardcorrupt делает цветой шум?
/* don't remove +flush_packets or it will repeat:
cur_dts is invalid st:0 (0) [init:1 i_done:0 finish:0] (this is harmless if it occurs once at the start per stream)
Last message repeated 52 times

*/
// +genpts+nobuffer+discardcorrupt+noparse+flush_packets
//-s ${settings.width}x${settings.height} -input_format vp8 -pix_fmt yuv420p -framerate ${settings.framerate}
+ ` -y ${howMuchLive && !inputFile.startsWith('/dev/') ? '' : ''}  ${isFfmpegMin51 ? '' : '-vsync cfr'} ${audioOutputsCount ? ''/*'-async 1'*/ : ''}`
+ (hasInputClearEnd ? `` : (
isSdp
? (
//` -analyzeduration 2147483647 -probesize 2147483647`
` -analyzeduration ${2147483647||Math.round(SEGMENT_DURATION_float * 1e6)} -probesize ${engine.getIdealBitrateInBits(howMuchLive, origVideoCodec, origActualHeight, origFps) / 8 * 2000.2   | 0}`
+ ` -max_interleave_delta ${Math.round(SEGMENT_DURATION_float * 1e6 * 2)} -max_delay ${Math.round(SEGMENT_DURATION_float * 1e6)}`
//+ ` -dts_delta_threshold 0`
//+ ` -start_at_zero`// -dts_delta_threshold 1 -copyts`
//+ ` -avoid_negative_ts disabled`
//+ ` -rtbufsize 1000M -itsoffset 0.1 -itsscale 1`
)
: ` -analyzeduration 2147483647 -probesize 2147483647 -audio_preload ${0.512 * 1e6}`//-max_delay ${0.500 * 1e6}
))
//+ (hasInputClearEnd ? `` : `   -copyts `)//-start_at_zero
+ (enableVideo ? ` $$beforeInputParams$$` : '')
+ (enableAudio === false ? ` -an` : ``)
+ (enableVideo === false ? ` -vn` : ``)
+ (enableAudio !== false && audioOutputsCount ? ` -filter_complex ${!videoOutputsCount||1 ? '' :
`[0:v]`
+ `fps=fps=${highestOutputFps.join('/')}:start_time=0:round=near,setsar=1/1,format=yuv420p,split=${videoOutputsCount}${finalOutputsV.map(x=>`[h264lh${Math.min(x.w, x.h)}]`).join('')}`
}${0&&videoOutputsCount && audioOutputsCount ? ';' : ''}${!audioOutputsCount ? '' :
`[0:a]`
+ `aresample=async=1:min_hard_comp=0.1:first_pts=0`//:comp_duration=${SEGMENT_DURATION_float}
+ `${has$Afftdn ? `,afftdn` : ``}${has$Bs2b ? `,bs2b=profile=jmeier` : ``},dynaudnorm=f=40:g=19:m=30`
//,apad=pad_dur=42666us
+ (
(
	origAudioSampleRate !== 48000
	/*
	WILL_OUTPUT_OPUS && origAudioSampleRate !== 48000
	||
	!WILL_OUTPUT_OPUS && !GOOD_AUDIO_SAMPLE_RATES.includes(origAudioSampleRate!)*/
)
? `,aformat=sample_fmts=flt,volume=-1dB,aresample=48000:resampler=soxr:precision=33`
: ''
)
//
+ `,asplit=${audioOutputsCount}${
finalOutputsA.map(x => `[${x.codec}${x.bitrateInBits}]`)
.join('')
}`
}` : ``)

+ (process.env.SSB && +process.env.SSB.replace(/^(\d+)(\.\d)?.+/, '$1$2') ? ` -ss ${process.env.SSB}` : ``)
+ (process.env.SSBV && +process.env.SSBV.replace(/^(\d+)(\.\d)?.+/, '$1$2') ? ` -ss:v ${process.env.SSBV}` : ``)
+ (process.env.ITS && +process.env.ITS.replace(/^(\d+)(\.\d)?.+/, '$1$2') ? ` -itsoffset ${process.env.ITS}` : ``)
+ (process.env.ITSBV && +process.env.ITSBV.replace(/^(\d+)(\.\d)?.+/, '$1$2') ? ` -itsoffset:v ${process.env.ITSBV}` : ``)
+ ` -i ${escapeSpawnArg(inputFile)}`
+ (process.env.SSA && +process.env.SSA.replace(/^(\d+)(\.\d)?.+/, '$1$2') ? ` -ss:a ${process.env.SSA}` : ``)
+ (process.env.SSV && +process.env.SSV.replace(/^(\d+)(\.\d)?.+/, '$1$2') ? ` -ss:v ${process.env.SSV}` : ``)
+ (process.env.ITSA && +process.env.ITSA.replace(/^(\d+)(\.\d)?.+/, '$1$2') ? ` -itsoffset:a ${process.env.ITSA}` : ``)
+ (process.env.ITSV && +process.env.ITSV.replace(/^(\d+)(\.\d)?.+/, '$1$2') ? ` -itsoffset:v ${process.env.ITSV}` : ``)

+ ` ${isFfmpegMin51 ? '-fps_mode cfr' : ''} -sc_threshold 0`
+ (hasInputClearEnd ? `` : 
'' //` -max_interleave_delta ${1.5 * 1e6 | 0} -max_delay ${1.5 * 1e6} -audio_preload ${1.5 * 1e6}`
//+ ` -bufsize:a 1`
)
//+ (pr === Protocol.FILE ? ` -output_ts_offset ${lowestOutputFps === 30 ? '116666us' : '140000us'}` : '')
+ (howMuchLive ? ' -flags +global_header' : '')
+ (
process.env.hasOwnProperty('TSOFFSET') && !+process.env.TSOFFSET!.replace('us', '')
|| (pk === 'sh' && !(process.env.hasOwnProperty('TSOFFSET') && +process.env.TSOFFSET!.replace('us', '')))
? ''
: ` -output_ts_offset ${+process.env.TSOFFSET?.replace('us', '') || 21333.3333333333333333333333333333*12}${isFfmpegMin40 ? 'us' : ''}`
) // -output_ts_offset 64 // 100 / (1000 / 60) = 6
//+ (process.env.TSOFFSETV?.replace('us', '') ? ` -output_ts_offset:v ${+(process.env.TSOFFSETV!.replace('us', ''))}us` : ``)
// or 533333us   ибо делится на 1000/30 и 21.3333ms
//-output_ts_offset ${Math.round(21333.333333333*6)}us
// one AAC audio fragment is 21333.3333333µs
// sometimes the first audio is late by 21333.333333µs*4=85333.3333333µs
// or maybe actually is it so?:
// sometimes the first audio is late by 21333.333333µs*5=106666.6666666µs
/*+ (
howMuchLive
?  `   1`//` -max_muxing_queue_size ${32*8}`//` -start_at_zero  -copyts -avoid_negative_ts disabled`// -thread_queue_size 1024 -max_muxing_queue_size 1024`
: ``
)*/
//+ ` ${tune}`
//+ ` -g          ${/*Math.round*/(newFPSForHighResolutions * keyFrameEverySeconds|0)}`
//+ ` -keyint_min ${/*Math.round*/(newFPSForHighResolutions * keyFrameEverySeconds|0)}`

// time-tested:
//+ ` -g          ${/*Math.round*/Math.ceil(lowerFps(newFPSForHighResolutions) * keyFrameEverySeconds)}`
//+ ` -keyint_min ${/*Math.round*/Math.ceil(lowerFps(newFPSForHighResolutions) * keyFrameEverySeconds)}`

//+isml
		)
		
		for (const _ of finalOutputsV) opts += ' -map 0:v:0 '
		//for (const h of doH264LandscapeHeights) opts += ' -map [h264h' + h + ']'
		opts += finalOutputsA.map(x=>` -map [${x.codec}${x.bitrateInBits}]`).join('')

		let _finalVdec
		{
			let _finalVenc
			let i = 0
			for (const x of finalOutputsV)
			{
				if (IS_ARZET_PC) console.error('@@  keyFrameEverySeconds=%O', keyFrameEverySeconds)
				const {beforeInputParams, videoParams, finalVenc, finalVdec} = getVideoParams(
					x,
					howMuchLive,
					hasInputClearEnd,
					outputOrientation === 'v',
					// @ts-ignore
					engine.cloneObject(_finalVenc || venc),
					// @ts-ignore
					engine.cloneObject(_finalVdec || vdec),
					origLandscapeWidth,
					origLandscapeHeight,
					origFps,
					origVideoCodec,
					origVideoBitrateInBits,
					keyFrameEverySeconds,
					whichYUV,
					isInterlaced,
					pr,
					i,
				)
				if (!_finalVdec)
				{
					_finalVdec = finalVdec
					if (
						_finalVdec[1] === 'sw'
						&&
						typeof vdec === 'object'
						&&
						vdec
						&&
						vdec.filter(x=>x[0]==='sw')[0]?.[1] === Infinity
					)
					{
						_finalVdec[0] = Infinity
					}
					_finalVenc = finalVenc
					if (
						_finalVenc[1] === 'sw'
						&&
						typeof venc === 'object'
						&&
						venc
						&&
						venc.filter(x=>x[0]==='sw')[0]?.[1] === Infinity
					)
					{
						_finalVenc[0] = Infinity
					}
				}
				if (i === 0) opts = opts.replace('$$beforeInputParams$$', beforeInputParams)
				opts += ' ' + videoParams
				i++
			}
		}
		let aTrackIdx = 0
		if (finalOutputsA.length)
		{
			finalOutputsA.forEach(x => {
				const curBr = Math.round(x.bitrateInBits / 1000)
				if (x.codec === 'copy')
				{
					opts += ` -c:a:${aTrackIdx} copy`
				}
				else if (x.codec === 'opus')
				{
					opts +=
` -c:a:${aTrackIdx} libopus -vbr:a:${aTrackIdx} off -b:a:${aTrackIdx} ${curBr}k`
				}
				else if (x.codec === 'aac-lc' || x.codec === 'he-aac_v2')
				{
					if (has$Libfdk_aac)
					{
						opts += ` -c:a:${aTrackIdx} libfdk_aac -b:a:${aTrackIdx} ${curBr}k`
						if (!x.cutoff)
						{
							console.warn('bad value x.cutoff=%O', x.cutoff)
						}
						else if (
							x.cutoff !== 'encoderDefaults'
						)
						{
							if (typeof x.cutoff === 'number' && x.cutoff !== Infinity) // although fixFinalOutputA already throws on `Infinity` 
							{
								opts += ` -cutoff:a:${aTrackIdx} ${x.cutoff}`
							}
							else if (x.cutoff === 'helperDefaults')
							{
								if (
									(x.codec === 'he-aac_v2' || x.codec === 'aac-lc')
									&&
									has$Libfdk_aac// fixme: what about `aac`?
									&&
									x.bitrateInBits <= 220e3
								)
								{
									opts += ` -cutoff:a:${aTrackIdx} ` + (
										x.bitrateInBits >= 96e3 ? 17500 : 16000
									)
								}
							}
							else if (x.cutoff === 'disabled')
							{
								opts += ` -cutoff:a:${aTrackIdx} 22050` // fixme: change 22_050 to the output sample rate
							}
							else
							{
								throw new Error('impossible \`cutoff\` value: \`' + x.cutoff + '\`')
							}
						}
						// https://developer.apple.com/documentation/http_live_streaming/http_live_streaming_hls_authoring_specification_for_apple_devices#2969504
						// 2.22. HE-AAC in fMP4 files MUST use explicit signaling of SBR data.
						if (
							x.codec === 'he-aac_v2'
						)
						{
							opts += ` -profile:a:${aTrackIdx} aac_he_v2 -signaling:a:${aTrackIdx} explicit_sbr`
							// -ar:a:${aTrackIdx} 48000 // even with 48khz it's still <Resync dT="42667" type="1"/>
						}
					}
					else
					{
						opts += ` -c:a:${aTrackIdx} aac -b:a:${aTrackIdx} ${curBr}k`
					}
				}
				else throw new Error(`unknown audio codec: '${x.codec}'`)
				opts += ` -metadata:s:a:${aTrackIdx} language=rus` // todo
				aTrackIdx++
			})
		}

		/*opts += ' -var_stream_map '
		
		for (let i = 0; i < doH264LandscapeHeights.length; i++)
		{
			if (i !== 0) opts += ','
			opts += 'h264h' + doH264LandscapeHeights[i]
		}
		for (let i = 0; i < doAudioAacBitrates.length; i++)
		{
			if (i !== 0 || doH264LandscapeHeights.length) opts += ','
			opts += `aac${doAudioAacBitrates[i]}`
		}
		for (let i = 0; i < doAudioOpusBitrates.length; i++)
		{
			if (i !== 0 || doH264LandscapeHeights.length || doAudioAacBitrates.length) opts += ','
			opts += `opus${doAudioOpusBitrates[i]}`
		}*/
		
		/*if (!inputHasAudio)
		{
			for (let i = 0; i < doH264LandscapeHeights.length; i++)
			{
				if (i !== 0) opts += ','
				opts += 'v:' + i
			}
		}
		else
		{
			for (let i = 0; i < doH264LandscapeHeights.length; i++)
			{
				if (i !== 0) opts += ' '
				opts += 'v:' + i
				//if ()
			}
			//if (inputHasAudio) opts += ',a:0'
			for (let i = 0; i < aTrackIdx; i++)
			{
				opts += `,a:${i}`
			}
		}*/



		const window_size = Math.round(2 / SEGMENT_DURATION_float * 10) // 20 seconds
		const extra_window_size = Math.round(2 / SEGMENT_DURATION_float * 10 / 3)
		
		let ports: {[key: number]: number} = {}
		if (pk === 'sh')
		{
			{
				let i = 0;
				for (; i < finalOutputsV.length; i++)
				{
					ports[i] = 20000+i
				}
				for (; i < finalOutputsV.length + aTrackIdx; i++)
				{
					ports[i] = 20000+i
				}
			}
			//jqfBg = async () => {
			if (0) {
				const dir = `/tmp/sh`
				//await engine.wait(100)
				await spawnAsyncVoid(`killall`, `packager`)
				spawnAsyncVoid(`packager`,
				(
`${(() => {
let ret = ''
let i = 0;
for (; i < finalOutputsV.length; i++)
{
const di = i//+1
ret += (
	`in=udp://127.0.0.1:${ports[i]},stream=video,init_segment=${dir}/${di}/init.m4s,segment_template=${dir}/${di}/s$Number$.m4s `
)
}
for (; i < finalOutputsV.length + aTrackIdx; i++)
{
const di = i//+1
ret += (
	`in=udp://127.0.0.1:${ports[i]},stream=audio,init_segment=${dir}/${di}/init.m4s,segment_template=${dir}/${di}/s$Number$.m4s `
)
}
return ret
})()}
-mpd_output ${dir}/manifest.mpd
--io_block_size 65536
-hls_base_url ${dir}/
-transport_stream_timestamp_offset_ms 0
--segment_duration ${SEGMENT_DURATION_float}
--utc_timings "urn:mpeg:dash:utc:http-xsdate:2014"="https://cdn.1meditat.com/utc_timestamp"
--segment_template_constant_duration=true --dash_add_last_segment_number_when_needed=true
-hls_master_playlist_output ${dir}/master.m3u8
-hls_playlist_type ${howMuchLive ? 'LIVE' : 'VOD'}
-segment_template_constant_duration`)
				//--allow_approximate_segment_timeline
				, {
					detached: true,
					logCmd: true,
					log: true,
					killMethods: shakaKillMethods,
				}
				)
				await engine.wait(50)
			}
		}
		//const useShaka = outputMediaManifestFilePath.includes('/29/9/')
		if (isFfmpegMin41 && pk === 'ff') opts += ` -format_options movflags=+cmaf`//+skip_sidx
		if (pk === 'ff') opts += (
			!howMuchLive
			? ` ${isFfmpegMin41 ? `-write_prft 0` : ``}  ${isFfmpegMin43 ? `-frag_type none` : ``}`
			// TODO: или не none, коли можно в эфиры превращать? Или польза every_frame только в получении последнего файла (LDASH) поколе он пишется?
			:
			//` -frag_type duration -frag_duration ${0.23333333333333333/*Math.min(SEGMENT_DURATION_float, 0.7)*/} -min_frag_duration 0.16666666666666666`
			// string 0.5333333333333333325 
			//` -frag_type duration -frag_duration ${SEGMENT_DURATION_float / 4 > 0.7 ? 0.7 : (new Decimal(SEGMENT_DURATION_float)).div(4).toFixed(7)} -min_frag_duration ${SEGMENT_DURATION_float / 4 > 0.7 ? (0.7 / 1.4) : (new Decimal(SEGMENT_DURATION_float)).div(4).div(1.4).toFixed(7)}`
			` ${isFfmpegMin43 ? `-frag_type every_frame` : ``} ${isFfmpegMin41 ? `-write_prft 1` : ``}`
		)
		/*if (isSdp) */opts += ' -fflags +shortest' // звук ускоряет. Ибо не хватает max_interleave_delta=1.6 при segment duration=1.6?
		/*if (isSdp) */opts += ' -shortest'
		const start_from_unixtime = 0
		{
			
		}
		const fTee = false
		const dashVars = {
			...(
				!howMuchLive
				?
				(
					IS_ARZET_PC
					? {
						update_period: 40,
						master_m3u8_publish_rate: 40,
					}
					: {}
				)
				// TODO: или не none, коли можно в эфиры превращать? Или польза every_frame только в получении последнего файла (LDASH) поколе он пишется?
				:
				{
					//` -frag_type duration -frag_duration ${0.23333333333333333/*Math.min(SEGMENT_DURATION_float, 0.7)*} -min_frag_duration 0.16666666666666666`
					// string 0.5333333333333333325 
					//` -frag_type duration -frag_duration ${SEGMENT_DURATION_float / 4 > 0.7 ? 0.7 : (new Decimal(SEGMENT_DURATION_float)).div(4).toFixed(7)} -min_frag_duration ${SEGMENT_DURATION_float / 4 > 0.7 ? (0.7 / 1.4) : (new Decimal(SEGMENT_DURATION_float)).div(4).div(1.4).toFixed(7)}`
					//`  ${isFfmpegMin41 ? `-index_correction 1` : ``}`
					...(isFfmpegMin43 ? {ldash: 1} : {}),
					...(isFfmpegMin42 ? {lhls: 1} : {}),
					strict: 'experimental',
					...(isFfmpegMin44 ? {update_period: 3} : {}),
					//master_m3u8_publish_rate: 1,
					//window_size: window_size,
					//extra_window_size: extra_window_size,
					...(isFfmpegMin43 ? {target_latency: engine.fixNumber(SEGMENT_DURATION_float * 1)} : {}),
					//remove_at_exit: 1,
					streaming: 1,
					utc_timing_url:
						//config.utc_timing_url // fixme
						'https://1meditat.com/utc_timestamp'
					,
					// if http, then -ignore_io_errors 1 -http_persistent 0 -timeout 0.2
					// https://lynne.ee/dash-streaming-from-the-top-down.html
				}
			),
			init_seg_name: `$RepresentationID$/init.${isFfmpegMin42 ? '$ext$' : 'm4s'}`,
			media_seg_name: `$RepresentationID$/$RepresentationID$s$Number${+start_from_unixtime ? '' : '%07d'}$.${isFfmpegMin42 ? '$ext$' : 'm4s'}`,
			//start_from_unixtime: start_from_unixtime,
			use_timeline: /*typeof partNumber === 'number' && partNumber > 0 ? 1 : */0,
			use_template: howMuchLive ? 1 : 1,
			[isFfmpegMin41 ? 'seg_duration' : 'min_seg_duration']: SEGMENT_DURATION_float,
			adaptation_sets: escapeSpawnArg(audioOutputsCount ? 'id=0,streams=v id=1,streams=a' : 'id=0,streams=v'),
			//hls_flags: '+independent_segments',
			...(isFfmpegMin40 ? {hls_playlist: 'true'} : {}),
			//...(supports$hls_master_name ? {hls_master_name: manifestM3U8Filename} : {}),
			f: 'dash',
		}
		if (fTee === false)
		{
			if (pk !== 'ff') throw 'fTee === true but pk is not `ff`'
			opts += Object.entries(dashVars).map(([key, value]) => {
				return ' -' + key + ' ' + value
			}).join('') + ' ' + escapeSpawnArg(outputMediaManifestFilePath)
		}
		else
		{
			// -muxdelay 0
			opts += ' -use_fifo 1 -f tee '
			opts += (() => {
				let ret = ''
				const Z = `\\'`

				if (pk === 'ff')
				{
					ret += `[select=${Z}`
					for (let j = 0; j < finalOutputsV.length; j++)
					{
						if (j !== 0) ret += `,`
						ret += `v:${j}`
					}
					if (aTrackIdx && (finalOutputsV.length))
					{
						//ret += `|`
					}
					for (let j = 0; j < aTrackIdx; j++)
					{
						ret += `,a:${j}`
					}
					ret += Z + ':'

					ret += Object.entries(dashVars).map(([key, value]) => {
						return (
							key
							+ '='
							+ (
								//typeof value === 'string' && value !== 'true'
								value.toString().includes(' ')
								? Z + value + Z
								: value
							)
						)
					}).join(':') + `]${outputMediaManifestFilePath}`
				}
				else if (ff === 'sh')
				{
					let i = 0;
					for (let j = 0; j < finalOutputsV.length; i++,j++)
					{
						if (i !== 0)
						ret += `|`
						ret += (
							`[select=${Z}v:${j}${Z}:mpegts_copyts=0:f=mpegts]`
							+ `udp://127.0.0.1:${ports[i]}?pkt_size=1316`
						)
					}
					if (aTrackIdx && (finalOutputsV.length))
					{
						//ret += `|`
					}
					for (let j = 0; j < aTrackIdx; i++,j++)
					{
						//if (i !== 0)
						ret += `|`
						ret += (
							`[select=${Z}a:${j}${Z}:mpegts_copyts=0:f=mpegts]`
							+ `udp://127.0.0.1:${ports[i]}?pkt_size=1316`
						)
					}
				}
				
				return ret
			})()
		}
		if (audioOutputsCount && inputHasAudio !== false && (!hasInputClearEnd||howMuchLive||1))//fixme: length
		{
			const origAudioOutputIdx = (finalOutputsV.length - 1) + 1
			//
			//opts += ` -map 0:a:1 -vn:${origAudioOutputIdx} -c:a:${origAudioOutputIdx} copy  ${outputOrigAudioFilePath} -map 0:v:2 -an:${origAudioOutputIdx+1} -c:v:${origAudioOutputIdx+1} copy ${outputOrigVideoFilePath}`
			//opts += ` -vn -c:a copy  ${outputOrigAudioFilePath}  -an -c:v copy ${outputOrigVideoFilePath}`
			//opts += ` -c copy  ${outputOrigVideoPlusAudioFilePath}`
			//if (pk !== 'sh') opts += ` -vn -c:a copy ${outputOrigAudioFilePath} -f mp4`
			// 28 байт весит до остановки ffmpeg
			//opts += ` -vn -movflags empty_moov+omit_tfhd_offset+frag_keyframe+default_base_moof -c:a copy ${outputOrigAudioFilePath}`
			opts += ` -vn -movflags empty_moov+omit_tfhd_offset+frag_keyframe+default_base_moof -c:a copy ${outputOrigAudioFilePath}`
		}
		/*else
		{
			const nextOutputIdx = (finalOutputsV.length - 1) + 1
			// делать thumbnail каждые 1/384 длительности, но не чаще, чем каждые 1600мс.
			//const thumbnailEveryFrames = Math.floor(Math.min(192, Math.floor(durationInMs / 1600 / 25)) * origFps)//наверно одного floor хватит
			const thumbnailEveryFrames = (
				durationInMs / 384 < 1600 * 384
				? 1.6 * origFps
				: durationInMs / 384 * origFps
			)
			console.log('thumbnailEveryFrames (cond=%O)=%O', durationInMs / 384 < 1600 * 384, thumbnailEveryFrames)
			opts += ' -map 0:v:0'
			opts += ` -an:${nextOutputIdx} -vf:${nextOutputIdx} select=not(mod(n\\,${thumbnailEveryFrames})),scale=160:90,tile=5x5 ${ffmpegVsyncParamName}:v:${nextOutputIdx} passthrough -vcodec:v:${nextOutputIdx} libwebp -qscale:v:${nextOutputIdx} 83 -compression_level:v:${nextOutputIdx} 0  ${outputThumbsDir}/th%03d.webp`
		}*/
		return opts
	})()

	const listStatsAdaptiveV: ListStats<'v'> = {
		minFps: lowestOutputFps,
		minWidth:  outputOrientation === 'h' ? finalMinH / 9 * 16 : finalMinH,
		minHeight: outputOrientation === 'h' ? finalMinH : finalMinH / 9 * 16,
		minLandscapeWidth:  finalMinLandscapeW,
		minLandscapeHeight: finalMinLandscapeH,
		minBitrateInBits: finalOutputsV.$mathMinByField('bitrateInBits'),

		maxFps: highestOutputFps,
		maxWidth:  outputOrientation === 'h' ? finalMaxH / 9 * 16 : finalMaxH,
		maxHeight: outputOrientation === 'h' ? finalMaxH : finalMaxH / 9 * 16,
		maxLandscapeWidth:  finalMaxLandscapeW,
		maxLandscapeHeight: finalMaxLandscapeH,
		maxBitrateInBits: finalOutputsV.$mathMaxByField('bitrateInBits'),
	}
	const listStatsAdaptiveA: ListStats<'a'> = {
		minBitrateInBits: finalOutputsA.$mathMinByField('bitrateInBits'),
		maxBitrateInBits: finalOutputsA.$mathMaxByField('bitrateInBits'),
	}
	const listStatsNonAdaptiveI: ListStats<'i'> = {
		minWidth:  outputOrientation === 'h' ? finalMaxH / 9 * 16 : finalMaxH,
		minHeight: outputOrientation === 'h' ? finalMaxH : finalMaxH / 9 * 16,
		minLandscapeWidth:  finalMaxLandscapeW,
		minLandscapeHeight: finalMaxLandscapeH,

		maxWidth:  outputOrientation === 'h' ? finalMaxH / 9 * 16 : finalMaxH,
		maxHeight: outputOrientation === 'h' ? finalMaxH : finalMaxH / 9 * 16,
		maxLandscapeWidth:  finalMaxLandscapeW,
		maxLandscapeHeight: finalMaxLandscapeH,
	}
	premeta = {
		metaVersion: [1, 0, 0],
		startedToFormPremetaOn,
		finishedToFormPremetaOn: Date.now(),
		inputs: [
			{
				protocol: pr === Protocol.RTMP ? 'rtmp' : (pr === Protocol.SRT ? 'srt' : 'file'),
				uri: inputFile.replace(/.+:\/\//, ''),
				w: origActualWidth,
				h: origActualHeight,
				fps: origFps,
				hasAudio: inputHasAudio,
				durationInMs: durationInMs,
				allMetaFfprobe: (whs || '') + (easilyReadableFfprobe + ''),
			}
		],
		//outputOrientation,
		outputs: {
			//useInput: 0,
			applyFilters: '_beta1_' as any/*[
				{filter: 'split': options: 3}
			],*/,
			thenWithOutputs: {
				main: {
					adaptive: [{
						protocol: 'file',
						uri: outputMediaManifestFilePath,
						adaptationSets: [
							...(finalOutputsV.length ? [{
								type: 'v',

								listStats: {v: listStatsAdaptiveV},

								segmentDurationInSecs: SEGMENT_DURATION_float,
								fragType: howMuchLive ? 'every_frame' : 'none',

								list: finalOutputsV,
							} as AdaptationSetV] : []),
							...(finalOutputsA.length ? [{
								type: 'a',

								listStats: {a: listStatsAdaptiveA},

								segmentDurationInSecs: SEGMENT_DURATION_float,
								fragType: howMuchLive ? 'every_frame' : 'none',

								list: finalOutputsA,
							} as AdaptationSetA] : []),
						]
					}],
					nonAdaptive: {
						listStats: {
							i: listStatsNonAdaptiveI,
						},

						list: [
							{
								protocol: 'file',
								uri: outputVideoCoverFinalPath,
								codec: howMuchLive ? 'png' : 'webp',
								seekUsecs: seekCover * 1000,
								w: outputOrientation === 'h' ? finalMaxH / 9 * 16 : finalMaxH,
								h: outputOrientation === 'h' ? finalMaxH : finalMaxH / 9 * 16,
							} as Output_i,
							{
								protocol: 'file',
								uri: outputOrigAudioFilePath,
								codec: 'copyaudio',
								bitrateInBits: 0,
							} as DeepRequired<Output_a>,
						]
					},
				},
			}
		},
		helperParams: {
			...arguments[0],
			forceOutputs: oldForceOutputs || forceOutputs,
		},
		usingPremetaJsonInsteadOfSuppliedForceOutputs: !!oldForceOutputs,
		flatOutputs: {
			adaptive: {
				listStats: {
					v: listStatsAdaptiveV,
					a: listStatsAdaptiveA,
				},
				v: [],
				a: [],
			},
			nonAdaptive: {
				listStats: {
					i: listStatsNonAdaptiveI,
				},
				v: [],
				a: [],
				i: [],
				multi: [],
			},
		},
		ffmpegMainProcessCmd: ffmpegMainProcessBin + ' ' + ffmpegMainProcessOpts,
		//bitrate: origBitrateKbps,
	}
	premeta.flatOutputs.nonAdaptive.v = (premeta.outputs.thenWithOutputs.main as OutputSet).nonAdaptive!.list.filter((x): x is Output_v => x.w).map(x=>({/*adaptive: false,*/...x}))
	premeta.flatOutputs.nonAdaptive.a = (premeta.outputs.thenWithOutputs.main as OutputSet).nonAdaptive!.list.filter((x): x is Output_a => ['aac-lc', 'he-aac_v2', 'opus', 'copyaudio'].includes(x.codec)).map(x=>({/*adaptive: false,*/...x}))
	premeta.flatOutputs.nonAdaptive.i = (premeta.outputs.thenWithOutputs.main as OutputSet).nonAdaptive!.list.filter((x): x is Output_i => ['png', 'webp'].includes(x.codec)).map(x=>({/*adaptive: false,*/...x}))

	premeta.flatOutputs.adaptive.v.push(
		...(premeta.outputs.thenWithOutputs.main as OutputSet).adaptive![0]!.adaptationSets.filter((x): x is AdaptationSetV => x.type === 'v')
		.map(as=>as.list
			.filter((x): x is Output_vActual2 => x.w)
			.map(x=>({/*adaptive: true,*/...engine.objectWithoutKeys_shallowCopy(as, ['type', 'listStats', 'list']),...x}))
		).flat()
	)
	premeta.flatOutputs.adaptive.a.push(
		...(premeta.outputs.thenWithOutputs.main as OutputSet).adaptive![0]!.adaptationSets.filter((x): x is AdaptationSetA => x.type === 'a')
		.map(as=>as.list
			.filter((x): x is Output_a => ['aac-lc', 'he-aac_v2', 'opus', 'copyaudio'/*?*/].includes(x.codec))
			.map(x=>({/*adaptive: true,*/...engine.objectWithoutKeys_shallowCopy(as, ['type', 'listStats', 'list']),...x}))
		).flat()
	)
	console.dir(premeta, {depth:null, maxArrayLength: Infinity})
	//process.exit(0)
	//return
	if (typeof onFfprobe === 'function')
	{
		onFfprobe(premeta)
	}

	let endedSuccessfully: undefined|true = void 0
	return (async () => {
		await safeMkdirAsync(outputRootDir)
		//await safeMkdirAsync(outputRootDir + '/cover')
		if (typeof partNumber === 'number')
		{
			await safeMkdirAsync(outputDirIncludingPartNumberIfPresent)
		}

		await safeMkdirAsync(outputThumbsDir)
		for (let i = 0; i < finalOutputsV.length + finalOutputsA.length; i++)
		{
			await safeMkdirAsync(pathJoinSmart(
				outputDirIncludingPartNumberIfPresent,
				i.toString()
			))
		}

		/*const playlistM3U8Path = pathJoinSmart(
			outputRootDir,
			'master.m3u8'
		)
		const manifestM3U8Filename = 'manifest.m3u8'
		const manifestM3U8Path = pathJoinSmart(
			outputRootDir,
			manifestM3U8Filename
		)
		if (!supports$hls_master_name)
		{
			await safeUnlinkAsync(playlistM3U8Path)
			await safeUnlinkAsync(manifestM3U8Path)
			await fsAsync.symlink(manifestM3U8Path, playlistM3U8Path)
		}*/

		if (alsoSingleFile)
		{
			await safeMkdirAsync(outputDirIncludingPartNumberIfPresent + '/raw')
			await [
				safeMkdirAsync(outputDirIncludingPartNumberIfPresent + '/raw/h264'),
				safeMkdirAsync(outputDirIncludingPartNumberIfPresent + '/raw/aac'),
			]
		}









		let coverDone = typeof partNumber === 'number' && partNumber > 0

		{
			console.log('alsoCover=%O', alsoCover)
			alsoCover = alsoCover && (isSdp || pr === Protocol.RTMP || pr === Protocol.SRT || pr === Protocol.FILE)
			console.log('alsoCover=%O', alsoCover)
		}

		//let KAJ = 0 // сколько % добавить когда всё готово
		let howMuchPercentDoAllJobsExceptABSGrant = 0 // все % кроме папки /abs/
		const howMuchImageJobGrants = 1.6
		if (alsoCover) howMuchPercentDoAllJobsExceptABSGrant += howMuchImageJobGrants
		console.log(
			'%O %O %O %O'.blue,
			seekCover,
			origFps,
			hasInputClearEnd,
		)
		let found0000003m4s = false
		let found0000012m4s = false
		let found0000015m4s = false
		const doImageJob = !alsoCover ? Promise.resolve() : imagesQueue.add(async () => {
			console.log('doImageJobdoImageJobdoImageJobdoImageJobdoImageJobdoImageJobdoImageJob')
			if (coverDone) return
			if (!hasInputClearEnd && !await new Promise<boolean>(resolve => {
				const ii = setInterval2(500, () => {
					if (siginted)
					{
						clearInterval(ii)
						resolve(false)
						return
					}
					if (found0000003m4s || dashFailed)
					{
						console.log('dashStarted=%O, found0000003m4s=%O, dashFailed=%O', dashStarted, found0000003m4s, dashFailed)
						clearInterval(ii)
						resolve(found0000003m4s && !dashFailed)
					}
					else
					{
						console.log('ii:: %O %O %O'.red, dashStarted, dashFailed)
					}
				})
			}))
			{
				console.log('dadadaqqqqqqqqqqqda'.red.bold)
				return
			}
			console.log('dadadada'.yellow.bold)
			if (!hasInputClearEnd)
			{
				await engine.wait(1000)
				if (siginted) return
				await engine.wait(
					Math.max(// Math.max is in case seekCover = 0
						SEGMENT_DURATION_float,
						Math.ceil(seekCover / SEGMENT_DURATION_float) * SEGMENT_DURATION_float
					)// + 1000// ждать 1000 мс (todo: или меньше сделать?), пока в пустой манифест не включится этот последний сегмент
				)
				if (siginted) return
			}
			//await engine.wait(2000)
			//if (origLandscapeHeight > 740)
			console.log('doImageJob'.yellow.bold)
			{
				console.log('outputVideoCoverFinalPath=%O', outputVideoCoverFinalPath)
				//ifFailThenUnlink.push(outputVideoCoverNoExt)
				imageJobInProcess = true
				if (!hasInputClearEnd)
				{
					ifFailThenUnlink.push(outputVideoCoverFinalPath)
					console.log('mpd={%s}', await gracefullyReadFileUtf8(outputMediaManifestFilePath))
				}
				else
				{
					ifFailThenUnlink.push(outputVideoCoverFinalPath)
				}
				const convertVideoToCoverExitCode = (
					hasInputClearEnd
					? await convertVideoToWebp(
						inputFile,
						outputVideoCoverFinalPath,
						{
							seek: seekCover,
							m: !hasInputClearEnd ? 1 : 6,
							nice: howMuchLive ? void 0 : 19,
							killMethods: imageJobKillMethods,
						},
					)
					: await convertVideoToPng(
						outputMediaManifestFilePath.replace('manifest.mpd', 'master.m3u8'),
						outputVideoCoverFinalPath,
						{
							seek: seekCover - 1000/origFps.$fromRational() - 1,
							nice: howMuchLive ? void 0 : 19,
							killMethods: imageJobKillMethods,
						},
					)
				)
				imageJobInProcess = false
				/*if (convertVideoToCoverExitCode === Infinity)
				{
					return
				}*/
				coverDone = true
				console.log('cover done %O', outputVideoCoverFinalPath)
				//result.cover = outputVideoCoverNoExt + '.webp'
				onTranscodeProgress('c', /*'webp', 100, *//*outputVideoCoverNoExt + '.webp', 0, */100, howMuchImageJobGrants)
				if (false) await Promise.all([
					[0, 0, 0],
					/*...doH.map(h => [1, -1, h]),
					...[
						[286, 188],
						[750, 410],
						[740, 444],
					].map(([w, h]) => [
						[1, w, h],
						[2, w, h]
					]).flat()*/
				].map(([f, w, h]) => {
					const cname = `${outputVideoCoverNoExt}.${w}x${h}`
					const outputJpg  = `${cname}${f > 1 ? '@'+f+'x' : ''}.jpg`
					const outputPng  = `${cname}${f > 1 ? '@'+f+'x' : ''}.png`
					const outputWebp = `${cname}${f > 1 ? '@'+f+'x' : ''}.webp`
					if (f) ifFailThenUnlink.push(outputJpg)
					ifFailThenUnlink.push(outputPng)
					ifFailThenUnlink.push(outputWebp)
					const ret = convertVideoToPng(
						inputFile,
						outputPng,
						{w: w*f, h: h*f, seek: seekCover}
					)
					if (f)
					{
						return ret
						.then(commons.convertPngToJpgAndWebp)
						.then(() => {
							/*if (!result.cover[`${w}x${h}`]) result.cover[`${w}x${h}`] = {}
							result.cover[`${w}x${h}`][f] = {
								[f]: {
									jpg: outputJpg,
									webp: outputWebp,
								}
							}*/
							return safeUnlinkAsync(`${cname}.png`)
						})
					}
					else
					{
						return ret
						.then(x => commons.convertImageToWebp(x, void 0, 6, 100))
						.then(() => {
							/*if (!result.cover[`${origLandscapeWidth}x${origLandscapeHeight}`]) result.cover[`${origLandscapeWidth}x${origLandscapeHeight}`] = {}
							result.cover[`${origLandscapeWidth}x${origLandscapeHeight}`] = {
								0: {
									webp: outputWebp
								}
							}*/
							return safeUnlinkAsync(`${cname}.png`)
						})
					}
					
				}))
				// https://stackoverflow.com/a/32244923/332012 :
				// ffmpeg -itsoffset -4 -i http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_1mb.mp4 -vcodec mjpeg -vframes 1 -an -f rawvideo -s 320x240 /sdcard/thumb.jpg
			}
		})







		if (false&&!howMuchLive && alsoSingleFile && inputHasAudio)
		{
			//KAJ += 1.6
			howMuchPercentDoAllJobsExceptABSGrant += 1.6
		}
		if (false&&!howMuchLive && alsoSingleFile && inputHasAudio) await tcQueue.add(async () => {
			/*const opusFilePath = pathJoinSmart(
				outputRootDir,
				inputFileName.replace(/(?:\.(mp4|webm|mkv|3gp|mov|flv))?$/i, '.opus')
			)*/
			const aacFilePath = pathJoinSmart(
				outputRootDir,
				'raw',
				'aac',
				'high.mp4'
			)
			const origAudioFilePath = pathJoinSmart(
				outputRootDir,
				'raw',
				//inputFileName.replace(/(?:\.(mp4|webm|mkv|3gp|mov|flv))?$/i, '.origaudio.webm')
				'origaudio.webm'
			)
			
			/*if (origAudioSampleRate === 48000)
			{
				ifFailThenUnlink.push(opusFilePath)
				if (origAudioCodec === 'opus')
				{
					await spawnAsyncVoid(FFMPEG_PATH, [
						'-i', inputFile,
						'-vn',
						'-acodec', 'copy',
						opusFilePath
					])//FIXME: mono->stereo
				}
				else
				{
					await spawnAsyncVoid(FFMPEG_PATH, [
						'-i', inputFile,
						'-vn',
						'-c:a', 'libopus', '-b:a', '256k',
						opusFilePath
					])
				}
				onTranscodeProgress('a', 'opus', 'high'/*, opusFilePath, 0*, 100, KAJ += 1.6)
			}
			else
			{
				onTranscodeProgress('a', 'opus', 'high'/*, opusFilePath, 0*, -1, KAJ += 1.6)
			}*/

			ifFailThenUnlink.push(aacFilePath)
			await spawnAsyncVoid(FFMPEG_PATH, [
				'-hide_banner', '-progress', '-',
				'-y',
				'-vsync', 'cfr',//fixme
				'-i', inputFile,
				'-vn',
				'-g',          /*Math.round*/(lowerFps(newFPSForHighResolutions) * keyFrameEverySeconds|0),
				'-keyint_min', /*Math.round*/(lowerFps(newFPSForHighResolutions) * keyFrameEverySeconds|0),
				'-force_key_frames', `expr:gte(t,n_forced*${keyFrameEverySeconds})`,
				'-sc_threshold', 0,
				//'-flags', '+ildct+ilme'
				'-movflags', '+faststart+isml+frag_keyframe+empty_moov',
				'-c:a', 'copy',
				origAudioFilePath
			])
			if (!origAudioBitrate || origAudioBitrate <= 1) origAudioBitrate = (
				(await fsAsync.stat(origAudioFilePath)).size
			) / (durationInMs / 1000) * 8 | 0
			if (origAudioCodec === 'aac' && origAudioBitrate && origAudioBitrate < 275000)
			{
				await spawnAsyncVoid(FFMPEG_PATH, [
					'-hide_banner', '-progress', '-',
					'-y',
					'-vsync', 'cfr',//fixme
					'-i', inputFile,
					'-vn',
					'-g',          /*Math.round*/(lowerFps(newFPSForHighResolutions) * keyFrameEverySeconds|0),
					'-keyint_min', /*Math.round*/(lowerFps(newFPSForHighResolutions) * keyFrameEverySeconds|0),
					'-force_key_frames', `expr:gte(t,n_forced*${keyFrameEverySeconds})`,
					'-sc_threshold', 0,
					//'-flags', '+ildct+ilme'
					'-movflags', '+faststart+isml+frag_keyframe+empty_moov',
					'-c:a', 'copy',
					aacFilePath
				])
			}
			else
			{
				/*if (!origAudioBitrate || origAudioBitrate <= 1)
				{
					const kjq = Date.now()
					await spawnAsyncVoid(FFMPEG_PATH, [
						'-y',
						'-vsync', 'cfr',//fixme
						'-i', inputFile,
						'-vn',
						'-c:a', 'copy',
						`/tmp/${kjq}.webm`
					])
					origAudioBitrate = (
						(await fsAsync.stat(`/tmp/${kjq}.webm`)).size
					) / (durationInMs*1000)
				}*/
				await spawnAsyncVoid(FFMPEG_PATH, [
					'-hide_banner', '-progress', '-',
					'-y',
					'-vsync', 'cfr',//fixme
					'-i', inputFile,
					'-vn',
					'-ac', '2',
					...(
						has$Libfdk_aac
						? [
							'-c:a', 'libfdk_aac',
							'-vbr:a', 'constrained',
						]
						: ['-c:a', 'aac']
					),
					'-b:a', newAacBitrateKbpsWithK = (
						origAudioBitrate
						? Math.min(MAX_AAC_BITRATE_KBPS, Math.max(
							64,
							(
								Math.floor(origAudioBitrate / 1024 / 64 - 0.2)
								<
								Math.floor(origAudioBitrate / 1024 / 64)
							)
							? Math.floor(origAudioBitrate / 1024 / 64) * 64
							: Math.round(origAudioBitrate / 1024 / 64) * 64
						)) + 'k'
						: MAX_AAC_BITRATE_KBPS + 'k'
					),
					aacFilePath
				])
			}
			onTranscodeProgress('a'/*, 'aac', 'high'*//*, aacFilePath, 0*/, 100, 1.6)
			/*if (!result.audio.aac) result.audio.aac = ['high']
			else result.audio.aac.push('high')
			whichFormatsAlreadyDone.push(['aac', 'high'])*/
		})

		

		const inputMediaShouldBeWritten = pr === Protocol.RTMP || pr === Protocol.SRT


		const doAllAbsGivesPercent = alsoSingleFile ? 85 : 100 - howMuchPercentDoAllJobsExceptABSGrant
		const doAllRawGivesPercent = (
			100 - doAllAbsGivesPercent - howMuchPercentDoAllJobsExceptABSGrant//3.2 это raw aac (1.6) + webp (1.6)  не надо совать переменную KAJ!!
			<= 0.01
			? 0 : 100 - doAllAbsGivesPercent - howMuchPercentDoAllJobsExceptABSGrant
		)
		alsoSingleFile = false && alsoSingleFile && !TestSegmentsModeDebug && !howMuchLive && doRawLandscapeH.length
		//let tcJobDone = false

		if (typeof requestKeyFrame === 'function')
		{
			let lastCalled = 0
			requestKeyFrame = ((origRequestKeyFrame) => {
				return async (type) => {
					if (Date.now() - lastCalled < 300) return false
					lastCalled = Date.now()
					return origRequestKeyFrame(type)
				}
			})(requestKeyFrame)
			const requestKeyFrameInterval = setInterval2(12e3, () => {
				if (siginted || endedSuccessfully)
				{
					clearInterval(requestKeyFrameInterval)
					return
				}
				if (dashStarted) requestKeyFrame!()
			})
		}
		
		const tcJob = tcQueue.add(async () => {
			if (false && alsoSingleFile) for (const h of doRawLandscapeH)
			// for (const w of [,])
			{
				const {newFPS, beforeInputParams, videoParams} = getVideoParams(
					howMuchLive,
					hasInputClearEnd,
					outputOrientation === 'v',
					h,
					// @ts-ignore
					engine.cloneObject(venc),
					// @ts-ignore
					engine.cloneObject(vdec),
					origLandscapeWidth,
					origLandscapeHeight,
					origFps,
					origVideoCodec,
					origVideoBitrateInBits,
					newFPSForHighResolutions, keyFrameEverySeconds, whichYUV, isInterlaced, pr
				)
				//const tune = howMuchLive ? '-tune zerolatency'
				//const tune = ' -tune film'
				const outputVideoFilename = (
//inputFileName.replace(/(?:\.(mp4|webm|mkv|3gp|mov|flv))?$/i, '.h264.' + h + 'p.mp4')
					'h264.' + h + 'p.mp4'
				)
				const outputVideoFilePath = pathJoinSmart(
					outputRootDir,
					'raw',
					outputVideoFilename
				)

				ifFailThenUnlink.push(outputVideoFilePath)

				{


					// -crf 23
// -field_order progressive
//-vf scale=-1:${h}${pixelformat}



					// ¿или
					// https://unix.stackexchange.com/questions/175090/burning-a-video-file-with-non-standard-aspect-ratio-to-dvd => http://ffmpeg.gusari.org/viewtopic.php?f=25&t=1235#p4566
					// -filter:v "scale='w=min(720,trunc((480*33/40*dar)/2+0.5)*2):h=min(480,trunc((720*40/33/dar)/2+0.5)*2)',pad='w=720:h=480:x=(ow-iw)/2:y=(oh-ih)/2',setsar='r=40/33'"
					
					tcJobSingleFileInProcess = true
					const code = await spawnAsyncVoid(
						FFMPEG_PATH,
						(
							/*`-hide_banner -progress - -y -vsync ${forceInputFps ? 'cfr' : 'cfr'} ${beforeInputParams} -i ${escapeSpawnArg(inputFile)}`
							//+ ` ${tune} -rc-lookahead 32`
							//+ ` -2pass true`
							+ (
								useNvidia
								? ` -strict_gop 1 -no-scenecut 1`// -forced-idr 1`
								: ` -mpv_flags +strict_gop`
							)
							+ ` -rc-lookahead 32`
							+ ` -movflags +faststart+isml+frag_keyframe+empty_moov`
							+ ` -sc_threshold 0 -bf:v 3`// ¿(sc_threshold === no-scenecut)?
							+ ` ${videoParams}`
							+ ` -preset:v fast`
							+ ` -an`
							+ ` ${escapeSpawnArg(outputVideoFilePath)}`
							*/''
						),
						{
							logCmd: true,
							killMethods: tcJobSingleFileKillMethods,
							stdout: (data: Buffer) => {
								//console.log('\noo\n' + data.toString().blue + 'oo\n')
								const curMs = Math.min(
									durationInMs,
									ttToMs(
										data.toString()
										.match(/(?:^out_|\s)time=\s*(\d+):(\d\d):(\d\d)\.(\d+)/)
									)
								)
								onTranscodeProgress(
									'v', //'h264', h, /*outputVideoFilePath, 0,*/
									Math.min(curMs / durationInMs * 100, 99), doAllRawGivesPercent
								)
								/*console.log(
									'duration%Oms-curMs%Oms=left%O, percent done=%O',
									durationInMs,
									curMs,
									durationInMs - curMs,
									curMs / durationInMs * 100
								)*/
							},
							stderr: (data: Buffer) => {
								console.error(data.toString())
								if (/\[FAIL\]/.test(data.toString()))
								{
									//resolveWhat = false
								}
							},
						}
					)
					tcJobSingleFileInProcess = false
					console.log(`${code ? '[FAIL]' : '[SUCCESS]'} resolution h=${h} child process exited with code ${code}`)
					if (code) return// false
					onTranscodeProgress('v'/*, 'h264', h, *//*outputVideoFilePath, 0*/, 100, doAllRawGivesPercent)
				}
				/*if (
					!await commons.fsExistsAsync(outputVideoFilePath)
				)
				{
					throw new Error(
'outputVideofilePath not found: ' + outputVideoFilePath
					)
				}
				if (
					!await commons.fsExistsAsync(outputVideoFilePath)
					||
					(await fsAsync.stat(outputVideoFilePath)).size < 30e3
				)
				{
					throw new Error(
'small size file ' + outputVideoFilePath + ' : ' + (await fsAsync.stat(outputVideoFilePath)).size
					)
				}*/

				;
				/*if (!result.video.h264) result.video.h264 = []
				result.video.h264.push(h)
				whichFormatsAlreadyDone.push(['h264', h])
				tcPercent += everyFormatGivePercent
				onTranscode(tcPercent, whichFormatsAlreadyDone)*/
				
				//outputVideoFilename
				/*ifFailThenUnlink.push(`${outputVideoCoverNoExt}.png`)
				ifFailThenUnlink.push(`${outputVideoCoverNoExt}.webp`)
				ifFailThenUnlink.push(`${outputVideoCoverNoExt}.jpg`)
				await convertVideoToPng(
					outputVideoFilePath,
					`${outputVideoCoverNoExt}.png`,
					{w: 0, h: 0, seek: seekCover}
				)
				.then(commons.convertPngToJpgAndWebp)
				.then(() => {
					return fsAsync.unlink(`${outputVideoCoverNoExt}.png`)
				})*/
			}
			// конец для вывода в один единственный видеофайл.

			{
				let writeAvailabilityStartTimeNumberInterval: NodeJS.Timeout|null = null
				if (!(SDP_TEST && isSdp) && typeof partNumber === 'number')
				{
					//if (partNumber === 0) {
					const availabilityStartTimeNumberFilepath = pathJoinSmart(
						outputRootDir,
						partNumber.toString(),
						'availabilityStartTimeNumber'
					)
					const availabilityStartTimeNumberManifest = pathJoinSmart(
						outputRootDir,
						partNumber.toString(),
						'manifest.mpd'
					)
					let iterationNum: number = 0
					const writeAvailabilityStartTimeNumberIntervalMsecs = 200
					writeAvailabilityStartTimeNumberInterval = setInterval2(writeAvailabilityStartTimeNumberIntervalMsecs, async () => {
						if (siginted)
						{
							clearInterval(writeAvailabilityStartTimeNumberInterval!)
							writeAvailabilityStartTimeNumberInterval = null
							return
						}
						console.log('reading %s'.bgCyan, availabilityStartTimeNumberManifest)
						const k = await gracefullyReadFileUtf8(availabilityStartTimeNumberManifest) as null|string
						if (siginted)
						{
							clearInterval(writeAvailabilityStartTimeNumberInterval!)
							writeAvailabilityStartTimeNumberInterval = null
							return
						}
						if (k?.[0])
						{
							console.log('read success'.bgCyan)
							const availabilityStartTimeMatch = k.match(/availabilityStartTime="([^"]+)"/)
							if (availabilityStartTimeMatch)
							{
								console.log('match success'.bgCyan)
								clearInterval(writeAvailabilityStartTimeNumberInterval!)
								writeAvailabilityStartTimeNumberInterval = null
								await fsAsync.writeFile(
									availabilityStartTimeNumberFilepath,
									(
										liveStartedOn ||= Date.parse(availabilityStartTimeMatch[1])
									).toString()
								)
								if (siginted)
								{
									clearInterval(writeAvailabilityStartTimeNumberInterval!)
									writeAvailabilityStartTimeNumberInterval = null
									return
								}
								await fsAsync.writeFile(
									availabilityStartTimeNumberManifest + '.whenJustStarted',
									k,
									{encoding: 'utf-8'}
								)
								if (siginted)
								{
									clearInterval(writeAvailabilityStartTimeNumberInterval!)
									writeAvailabilityStartTimeNumberInterval = null
									return
								}
							}
						}
						else if (false&&iterationNum >= Math.ceil((SEGMENT_DURATION_float + 3000) / writeAvailabilityStartTimeNumberIntervalMsecs))//todo: 1.001 to 1.0, not 2.0
						{
							clearInterval(writeAvailabilityStartTimeNumberInterval!)
							writeAvailabilityStartTimeNumberInterval = null
							if (!siginted)/*await */jsonFail('sdp died')
							return
						}
						iterationNum++
					})
					/*const kkk = jsonFail
					jsonFail = (function (kkk) {
						return function () {
							if (writeAvailabilityStartTimeNumberInterval)
							{
								clearInterval(writeAvailabilityStartTimeNumberInterval)
								writeAvailabilityStartTimeNumberInterval = null
							}
							return kkk(...args)
						}
					})(kkk)*/
				}
				let firstFrameDone = false
				//console.log('eaoeaoae')
				const TC_LOG_PATH = (
					typeof partNumber === 'number'
					? outputRootDir + '/' + partNumber + '/TC_LOG'
					: outputRootDir + '/TC_LOG'
				)
				await safeUnlinkAsync(TC_LOG_PATH)
				tcJobMultiFileLogFile = await fsPromisesOpen(
					TC_LOG_PATH,
					'a'
				)
				//tcJobMultiFileLogFilesByController.set(tcJobMultiFileKillMethods, tcJobMultiFileLogFile)
				//tcJobMultiFileLogFileWriteQueueByController.set(tcJobMultiFileKillMethods, tcJobMultiFileLogFileWriteQueue)
				/*if (typeof onPreprestarted === 'function')
				{
					onPreprestarted!()
				}*/
				/*if (typeof onPrestarted === 'function')
				{
					setTimeout2(0, () => {
						if (!dashFailed && !siginted)
						{
							await onPrestarted!()
						}
					})
				}*/
				if (isSdp && SDP_TEST)
				{
					await engine.wait(5e3)
					if (typeof onPrestarted === 'function' && !dashFailed && !siginted)
					{
						await onPrestarted!()
					}
					await engine.wait(180e3)
				}

				if (inputMediaShouldBeWritten)
				{
					;(async () => {
						tcJobInputInProcess = true
						const code = await spawnAsyncVoid(
							'nice',
							`-n 16 ${FFMPEG_PATH} -i "${inputFile}" -codec copy ${outputInputMediaFilePath}`,
							{
								killMethods: tcJobInputKillMethods,
							},
						)
						tcJobInputInProcess = false
					})()
				}

				tcJobMultiFileInProcess = true
				const code = await spawnAsyncVoid(
					//FFMPEG_PATH, opts,
					ffmpegMainProcessBin,
					ffmpegMainProcessOpts,
					{
						detached: true,// otherwise all .m3u8 and .mpd become empty (0 bytes) when this script receives SIGINT
						stdouterrOrder: 3,
						logCmd: (cmd) => {
							tcJobMultiFileLogFileWriteQueue.add(async () => {
								if (tcJobMultiFileLogFile && tcJobMultiFileLogFile.fd !== -1) try{await tcJobMultiFileLogFile.appendFile(
									cmd
									+ '\n'
									, {encoding: 'utf8'}
								)}catch(e){console.error(e)}
							})
						},
						killMethods: tcJobMultiFileKillMethods,
						//dontRun: isSdp,
						stdout: async (data: Buffer) => {
							if (!tcJobMultiFileLogFile) return
							if (!found0000003m4s)console.log('found0000003m4s=%O', found0000003m4s)
							if (!firstFrameDone)console.log('firstFrameDone=%O', firstFrameDone)
							if (!coverDone)console.log('coverDone=%O', coverDone)
							const dataUtf8 = data.toString()
							tcJobMultiFileLogFileWriteQueue.add(async () => {
								if (tcJobMultiFileLogFile && tcJobMultiFileLogFile.fd !== -1) try{await tcJobMultiFileLogFile.appendFile(
									'['
									+ (new Date()).toISOString()
									+ '][o]'
									+ dataUtf8
									+ '\n'
									, {encoding: 'utf8'}
								)}catch(e){console.error(e)}
							})
							if (
								dataUtf8.includes('No longer receiving stream_index')
							)
							{
								/*await */jsonFail('No longer receiving stream_index')
								return
							}
							//console.log(dataUtf8)
							//fsAsync.writeFile('/tmp/fff'+Date.now()+'out', dataUtf8, {encoding: 'utf-8'})
							//if (howMuchLive)
							if (dataUtf8.includes('0s0000003.m4s'))
							{
								found0000003m4s = true
							}
							else if (dataUtf8.includes('0s0000012.m4s'))
							{
								found0000012m4s = true
							}
							else if (dataUtf8.includes('0s0000015.m4s'))
							{
								found0000015m4s = true
							}
							/*if (dataUtf8.includes('frame='))
							{
								firstFrameDone = true
							}*/
							//console.log('qjaojao')
							if (!dashStarted && !dashFailed && found0000003m4s && firstFrameDone && (!alsoCover || coverDone))
							{
								if (!isSdp)await onPreprestarted!()
								if (!isSdp)await onPrestarted!()
								await onStarted!()
							}
							//console.log('eeee')

							if (typeof partNumber === 'number')
							{
								//console.log('partnumber')
								return
							}
							//console.log('\noo\n' + data.toString().blue + 'oo\n')
							
							/*console.log(
								'duration%Oms-curMs%Oms=left%O, percent done=%O',
								durationInMs,
								curMs,
								durationInMs - curMs,
								curMs / durationInMs * 100
							)*/
						},
						stderr: async (data: Buffer) => {
							if (!tcJobMultiFileLogFile) return
							/*if (typeof onPrestarted === 'function' && !dashFailed && !siginted)
							{
								await onPrestarted!()
							}*/
							let dataUtf8 = data.toString().replaceAll('size=N/A ', '').replaceAll('bitrate=N/A', '')
							/*if (!dashStarted && Math.random() <= 0.95)
							{
								dataUtf8 = dataUtf8
								.replaceAll('[debug] cur_dts is invalid st:0 (0) [init:0 i_done:0 finish:0] (this is harmless if it occurs once at the start per stream)\n', '')
								.replace(/\[vp9 @ 0x[^\]]+\] \[error\] Not all references are available\n/m, '')
								.replace('[error] Error while decoding stream #0:0: Invalid data found when processing input\n', '')
							}*/
							if (!dataUtf8)
							{
								return
							}

							{
								const mmm = dataUtf8
								.match(/(?:^out_|\s)time=\s*(\d+):(\d\d):(\d\d)\.(\d+)/)
								if (mmm)
								{
									//console.log('%s'.red, dataUtf8)
									const curMs = Math.min(
										durationInMs,
										ttToMs(mmm)
									)
									//if (dataUtf8.includes('[dash @ '))
									//console.log(curMs)
									onTranscodeProgress(
										'v',// 'h264', doAbsH, //outputMediaManifestFilePath, 0,
										Math.min(curMs / durationInMs * 100, 99), doAllAbsGivesPercent
									)
								}
							}
							//if (!SDP_TEST && dataUtf8.includes('Opening an input file:') && typeof onPrestarted === 'function' && !dashFailed && !siginted)
							if (
								isSdp && !SDP_TEST && (
									dataUtf8.includes('video codec set to:')
									||
									dataUtf8.includes('yuv420p')
								) && typeof onPrestarted === 'function' && !dashFailed && !siginted
							)
							{
								console.log('onPrestarted because  video codec set to OR yuv420p')
								await onPrestarted!()
							}
							else if (!dashStarted && (
								dataUtf8.includes('Not all references are available')
								||
								dataUtf8.includes('prior keyframe')
								||
								dataUtf8.includes('Missed part of a keyframe')
								||
								(
									!origVideoCodec.startsWith('vp')
									&&
									//'[debug] cur_dts is invalid st:0 (0) [init:0 i_done:0 finish:0] (this is harmless if it occurs once at the start per stream)\n'
									//'[h264 @ 0x55d44809d0c0] [debug] nal_unit_type: 1(Coded slice of a non-IDR picture), nal_ref_idc: 2\n'
									dataUtf8.includes('cur_dts is invalid')
								)
							))
							{
								console.log('onPrestarted because Not all ref OR prior keyframe OR Missed part of a keyframe')
								await onPrestarted!()
							}
							//dataUtf8 = dataUtf8.replace(/^\[dash @ .+$\n*/gm, '')
							tcJobMultiFileLogFileWriteQueue.add(async () => {
								const ll = dataUtf8//.replace(/^\[dash @ .+$\n*/gm, '')
								if (!ll) return
								if (tcJobMultiFileLogFile && tcJobMultiFileLogFile.fd !== -1) try{await tcJobMultiFileLogFile.appendFile(
									'['
									+ (new Date()).toISOString()
									+ '][e]'
									+ ll
									+ '\n'
									, {encoding: 'utf8'}
								)}catch(e){console.error(e)}
							})

							console.error('[err]{%O}', dataUtf8.replaceAll('    \r', ''))

							if (
								false
								&&
								isSdp
								&&
								dataUtf8.includes('Stream #0:0, ')
							)
							{
								await jsonFail('invalid input (frame)')
								return
							}
							if (
								false
								&&
								isSdp
								&&
								dataUtf8.includes('Setting \'video_size\' to value')
								
							)
							{
								const a = dataUtf8.match(/Setting \'video_size\' to value \'(\d+)x(\d+)/)
								if (!a)
								{
									console.error('wtf 3016'.bgRed)
									await jsonFail('invalid input')
									return
								}
								else if (origActualWidth !== +a[1] || origActualHeight !== +a[2])
								{
									await jsonFail('bad input')
									return
								}
							}
							if (
								false
								&&
								isSdp
								&&
								/  Stream #0:0[^:\n]*: Video: vp/.test(dataUtf8)//[^:]* если -loglevel debug
							)
							{
								//Stream #0:0: Video: vp9 (Profile 0), yuv420p(tv), 960x540, 90k tbr, 90k tbn
								//Stream #0:0: Video: vp9 (Profile 0), none, 90k tbr, 90k tbn
								//Stream #0:0, 609, 1/90000: Video: vp9 (Profile 0), 1 reference frame, none, 90k tbr, 90k tbn
								//Stream #0:0, 606, 1/90000: Video: vp9 (Profile 0), 1 reference frame, yuv420p(tv), 960x540, 0/1, 90k tbr, 90k tbn
								//Stream #0:0, 570, 1/90000: Video: vp9 (Profile 0), 1 reference frame, yuv420p(tv), 1440x810, 0/1, 90k tbr, 90k tbn
								// 111088:  Stream #0:0: Video: vp9 (Profile 0), yuv420p(tv), 640x360, 23.98 tbr, 90k tbn
								// tbr can also be: 15.50, 23.92, 24.08, 48, 59.94, 60    all without k
								// output could be:
								// Stream #2:0: Video: vp9 (Profile 0) (VP90 / 0x30395056), yuv420p(tv), 810x1440, q=2-31, 90k tbr, 1k tbn
								const a = dataUtf8.match(/tream #0:0[^:\n]*: Video: vp(.+?reference)?.+?(none|\d+x\d+).+?(90k|\d+(?:\.\d+)? tbr, 90k tbn)/m)
								console.log(a)
								if (!a)
								{
									await jsonFail('invalid input')
									return
								}
								if (a[1])
								{
									await jsonFail('invalid input (frame)')
									return
								}
								if (a[2] === 'none')// || !(a[3] === '90k' || +a[3]/*remove*/ || +a[3] === origFps))
								{
									await jsonFail('invalid input')
									return
								}
								const [guessedW, guessedH] = a[2].split('x').map(x => +x)
								if (origActualWidth !== guessedW || origActualHeight !== guessedH)
								{
									await jsonFail('bad input')
									return
								}
							}
							if (isSdp)
							{
								if (!dashStarted && dataUtf8.includes('max delay reached. need to consume packet') && found0000012m4s)
								{
									await jsonFail('sdp died')
									return
								}
								if (!dashStarted && dataUtf8.includes('Connection timed out'))
								{
									await jsonFail('sdp died')
									return
								}
								if (dataUtf8.includes('no frame!'))
								{
									await jsonFail('sdp died')
									return
								}
								/*if (dataUtf8.includes('Not all references are available'))
								{
									await jsonFail('sdp died')
									return
								}*/
								if (dataUtf8.includes('bind failed'))
								{
									await jsonFail('bind failed')
									return
								}
							}
							if (!firstFrameDone)
							{
								if (dataUtf8.includes('frame duplication too large, skipping'))
								{
									await jsonFail('bad input')
									return
								}
							}
							if (dataUtf8.includes('0s0000003.m4s'))
							{
								found0000003m4s = true
							}
							else if (dataUtf8.includes('0s0000012.m4s'))
							{
								found0000012m4s = true
							}
							else if (dataUtf8.includes('0s0000015.m4s'))
							{
								found0000015m4s = true
							}
							if (!dashStarted && !dashFailed && found0000003m4s && firstFrameDone && (!alsoCover || coverDone || found0000015m4s))
							{
								if (!isSdp)await onPreprestarted!()
								if (!isSdp)await onPrestarted!()
								await onStarted!()
							}
							//console.error('[err]' + dataUtf8)
							//fsAsync.writeFile('/tmp/fff'+Date.now()+'err', dataUtf8, {encoding: 'utf-8'})
							if (0&&dataUtf8.includes('non-existing PPS 0 referenced'))
							{
								await jsonFail('transcode failed (bad phone)')
								return
							}
							else if (
								//dashStarted&&
								dataUtf8.includes('Error initializing the MFX video decoder')
							)
							{
								if (dashStarted)
								{
									await jsonFail('transcode failed (MFX)')
									return
								}
								else
								{
									console.log('onPrestarted because Error initializing the MFX video decoder')
									await onPrestarted!()
								}
							}
							else if (
								dataUtf8.includes('./_studio/mfx_lib/')
							)
							{
								await jsonFail('transcode failed (MFX, assertion)')
								return
							}
							else if (dashStarted && dataUtf8.includes('Error while decoding stream'))
							{
								await jsonFail('transcode failed (bad phone, err2)')
								return
							}
							else if (dataUtf8.includes('Output file is empty'))
							{
								await jsonFail('transcode failed (bad phone, err3)')
								return
							}
							else if (dataUtf8.includes('Nothing was written into output file'))
							{
								await jsonFail('transcode failed (empty output file)')
								return
							}
							else if (
								dataUtf8.includes(': Assertion `')
							)
							{
								await jsonFail('transcode failed (assertion)')
								return
							}
							else if (
								//corrupted double-linked list
								//dataUtf8.includes('corrupted')
								/corrupted(?!\smacroblock)/.test(dataUtf8)
							)
							{
								await jsonFail('transcode failed (something is corrupted)')
								return
							}
							if (typeof requestKeyFrame === 'function' && (
								dataUtf8.includes('corrupted macroblock')
								||
								dataUtf8.includes('bad cseq')
							))
							{
								requestKeyFrame('v')
							}
							//if (dataUtf8.includes('[dash @ '))
							if (dataUtf8.includes('frame='))
							{
								firstFrameDone = true
							}
							if (!dashStarted && !dashFailed && found0000003m4s && firstFrameDone && (!alsoCover || coverDone || found0000015m4s))
							{
								if (!isSdp)await onPreprestarted!()
								if (!isSdp)await onPrestarted!()
								await onStarted!()
							}
							if (/\[FAIL\]/.test(dataUtf8))
							{
								//resolveWhat = false
							}
						},
						qqq: tcJobMultiFilePidObject,
					}
				)
				if (!dashStarted)
				{
					dashFailed = true// на всякий случай
				}
				if (writeAvailabilityStartTimeNumberInterval)
				{
					clearInterval(writeAvailabilityStartTimeNumberInterval)
					writeAvailabilityStartTimeNumberInterval = null
				}
				await tcJobMultiFileLogFileWriteQueue.onIdle()
				//tcJobMultiFileLogFileWriteQueueByController.delete(tcJobMultiFileKillMethods)
				console.log('logFile.close()')
				await tcJobMultiFileLogFile?.close()
				tcJobMultiFileLogFile = null
				//tcJobMultiFileLogFilesByController.delete(tcJobMultiFileKillMethods)
				tcJobMultiFileInProcess = false

				if (tcJobInputInProcess) try {
					tcJobInputKillMethods.smoothKill?.()
				} catch (e) {}
				tcJobMultiFileInProcess = false
				//tcJobDone = true
				console.log(`${code ? '[FAIL]' : '[SUCCESS]'} main ffmpeg child process exited with code ${code}`)
				if (!code && !siginted)
				{
					if (isSdp)
					{
						await jsonFail('sdp has unexpectedly ended with success')
						return 
					}
					else if (!hasInputClearEnd)
					{
						// actually this is not a fail
						await jsonFail('ffmpeg: live successfully ended')
						return 
					}
				}
				else if (code && code !== 255/*SIGINT*/)
				{
					console.error('BAD CODE, line 2059'.red)
					await jsonFail('transcode failed')
					return// false
				}
				/*if (!supports$hls_master_name)
				{
					await fsAsync.rename(playlistM3U8Path, manifestM3U8Path)
				}*/
				onTranscodeProgress('v', /*'h264', doH264LandscapeHeights,*/ /*outputMediaManifestFilePath, 0, */100, doAllAbsGivesPercent)
			}

			console.log('aououeoue')
			//onTranscode(tcPercent, whichFormatsAlreadyDone)
			//const everyFormatGivePercent = (100-KAJ) / doH.length


			
			//await fsAsync.unlink(inputFile)
		})

		console.log('waiting tcToUpQueue, its size=%O', tcToUpQueue.size)
		//await tcToUpQueue.onIdle()
		return {
			//promises: [tcJob, doImageJob],
			promise: new Promise<void>(async (resolve) => {
				await tcJob
				await doImageJob
				endedSuccessfully = true
				resolve()
			}),
			premeta,
			sigint,
		}
	})().catch(async (e) => {
		console.error(e)
		for (const f of ifFailThenUnlink)
		{
			try {
				await safeUnlinkAsync(f)
				// или без await?
			} catch (e) {}
		}
		await jsonFail('%upv__remote__unknown%')
		return
	})
}
/*
ffmpeg -loglevel debug -analyzeduration 20M -probesize 20M -init_hw_device qsv=qsv:MFX_IMPL_hw_any -r 60 -protocol_whitelist file,rtp,udp  -use_wallclock_as_timestamps 1 -fflags '+genpts+igndts' -max_delay 500000 -thread_queue_size 1024 -progress - -y -vsync cfr -i /tmp/oo.sdp -flags '+global_header' -g 60 -keyint_min 60 -force_key_frames 'expr:gte(t,n_forced*1)' -mpv_flags '+strict_gop' -sc_threshold 0 -ac 2 -movflags '+faststart+isml+frag_keyframe+empty_moov+separate_moof+default_base_moof+cmaf' -map 0:v:0 -map 0:a:0 -c:v h264_qsv -threads 4  -rdo 1 -pic_timing_sei 1 -recovery_point_sei 1 -aud 1 -c:a libfdk_aac -cutoff:a 17500 -b:a 160k -filter:a aformat=sample_fmts=flt,volume=-1dB,aresample=44100:resampler=soxr:precision=33 -f flv  rtmps://a.rtmp.youtube.com/live2/8bxb-k9y5-x5ks-8bh0-5z2b
*/
