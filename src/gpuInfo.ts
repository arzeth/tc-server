import systeminformation from 'systeminformation'
//import { spawnAsyncUTF8 } from '../common-server-code/index.js'

const rawList = (await systeminformation.graphics()).controllers.filter(x=>
	(
		x.bus || x.pciBus
	)
	//&&
	// /nvidia|amd|intel/i.test(x.vendor)
)

//type PossibleAccelVideoCodec = 'h264'|'hevc'|'vp8'|'vp9_0'

export interface GraphicsControllerDataExtended extends systeminformation.Systeminformation.GraphicsControllerData {
	vendorShort: string
	devicePath: string
	enc: {
		qsv: Array<'h264'|'hevc'|'vp8'|'vp9_0'|'vp9_2'>
		nv: Array<'h264'|'hevc'|'vp8'|'vp9_0'|'vp9_2'>
		vaapi: Array<'h264'|'hevc'|'vp8'|'vp9_0'>
	}
	dec: {
		qsv: Array<'h264'|'hevc'|'vp8'|'vp9_0'|'vp9_2'>
		nv: Array<'h264'|'hevc'|'vp8'|'vp9_0'|'vp9_2'>
		vaapi: Array<'h264'|'hevc'|'vp8'|'vp9_0'>
	}
}
const outputList: Array<GraphicsControllerDataExtended> = rawList.map(
	// @ts-ignore
	(x: GraphicsControllerDataExtended) =>
{
	x.vendorShort = (() => {
		if (/nvidia/i.test(x.vendor)) return 'nv'
		if (/intel/i.test(x.vendor)) return 'intel'
		if (/amd/i.test(x.vendor)) return 'amd'
		return x.vendor.trim().replace(/([a-zA-Z]+).+/, '$1').toLocaleLowerCase()
	})()

	const modelFirstWord = x.model
	.trim()
	.replace(/(Sandy Bridge|Ivy Bridge|Bay Trail|[a-zA-Z0-9]+(?: Lake)?).+/, '$1').replace(' ', '').toLocaleLowerCase()

	if (x.vendorShort === 'intel')
	{
		let vaGen: number// video accelaration generation (this term exists only in this file)
		switch (modelFirstWord)
		{
			case 'ironlake': vaGen = 1; break; // MPEG-2, H.264 decode.
			case 'sandybridge': vaGen = 2; break; // VC-1 decode; H.264 encode.
			case 'ivybridge':
			case 'baytrail':
			case 'haswell': vaGen = 3; break; // JPEG decode; MPEG-2 encode.
			case 'broadwell': vaGen = 4; break; // VP8 decode.
			case 'braswell': vaGen = 5; break; // H.265 decode; JPEG encode; VP8 encode.
			case 'skylake': vaGen = 6; break; // H.265 encode.
			case 'apollolake': vaGen = 7; break; // VP9 (8-bit, profile 0, 422) decode, H.265 Main10 decode.
			case 'kabylake':
			case 'coffeelake':
			case 'geminilake':
			case 'cannonlake': vaGen = 8; break; // VP9 profile 2 decode; VP9, H.265 Main10 encode.
			case 'icelake': vaGen = 9; break; // H.265 8 bits 422/444 decode,encode; VP9 8/10 bits 444 decode,encode.
			case 'tigerlake': vaGen = 10; break; // AV1 8/10 bits; H.265 12 bits decode; VP9 12 bits decode.
			case 'rocketlake':
			case 'alderlake': vaGen = 11; break; // VP8 encode removed.
			case 'dg1': // Iris XE DG1
			case 'sg1': // Iris XE SG1
				vaGen = 12; // VP8 encode removed.
				break;
			default: vaGen = 12;
		}
		if (vaGen === 8)
		{
			x.enc = {
				qsv: ['h264', 'hevc', 'vp8', 'vp9_0'],
				nv: [],
				vaapi: ['h264', 'hevc', 'vp8', 'vp9_0'],
			}
			x.dec = {
				qsv: ['h264', 'hevc', 'vp8', 'vp9_0'],
				nv: [],
				vaapi: ['h264', 'hevc', 'vp8', 'vp9_0'],
			}
		}
	}
	else if (x.vendorShort === 'nv')
	{
		/*
		// https://github.com/jp7677/dxvk-nvapi/blob/master/src/sysinfo/nvapi_adapter.cpp
		  if (isVkDeviceExtensionSupported(VK_KHR_FRAGMENT_SHADING_RATE_EXTENSION_NAME)
            && m_deviceFragmentShadingRateProperties.primitiveFragmentShadingRateWithMultipleViewports)
            return NV_GPU_ARCHITECTURE_GA100;

        // Variable rate shading is supported on Turing and newer
        if (isVkDeviceExtensionSupported(VK_NV_SHADING_RATE_IMAGE_EXTENSION_NAME))
            return NV_GPU_ARCHITECTURE_TU100;

        // VK_NVX_image_view_handle is supported on Volta and newer
        if (isVkDeviceExtensionSupported(VK_NVX_IMAGE_VIEW_HANDLE_EXTENSION_NAME))
            return NV_GPU_ARCHITECTURE_GV100;

        // VK_NV_clip_space_w_scaling is supported on Pascal and newer
        if (isVkDeviceExtensionSupported(VK_NV_CLIP_SPACE_W_SCALING_EXTENSION_NAME))
            return NV_GPU_ARCHITECTURE_GP100;

        // VK_NV_viewport_array2 is supported on Maxwell and newer
        if (isVkDeviceExtensionSupported(VK_NV_VIEWPORT_ARRAY2_EXTENSION_NAME))
            return NV_GPU_ARCHITECTURE_GM200;

        // Fall back to Kepler
        return NV_GPU_ARCHITECTURE_GK100;
	 */
		/*
		Compared to prior generation Pascal and Volta GPU architectures, Turing supports additional video decode formats such as HEVC 4:4:4 (8/10/12 bit), and VP9 (10/12 bit).
		*/
		x.enc = {
			qsv: [],
			nv: ['h264', 'hevc'],
			vaapi: ['h264'],
		}
		x.dec = {
			qsv: [],
			nv: ['h264', 'hevc', 'vp8', 'vp9_0', 'vp9_2'],
			vaapi: ['h264'],
		}
	}
	
	x.devicePath = '/dev/dri/by-path/pci-0000:' + x.busAddress + '-render'

	return x
})
export default outputList


